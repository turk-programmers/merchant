<?php
include('/home/funeralnet/public_html/mfscommon/php/encryption.php');

function assetsPath( $path ){
    if (empty($path)){
        return $path;
    }
    //www.funeralnet.com/mfscommon/programs/merchant/assets/js/
    $fileName = basename( $path );
    $ext = strtolower(end(explode('.',$fileName)));
    $folder = str_replace( $fileName , "" , $path );
    $clientPath = FCPATH . 'assets/' . $folder . $fileName;
    if( file_exists( $clientPath ) ){
        return str_replace( FCPATH , "" , $clientPath);
    }
    return '//www.funeralnet.com/mfscommon/programs/merchant/assets/' . $ext ."/" . $fileName;
}

/**
 * 2digit number
 */
function number2digit( $number ){
    return ( $number <= 9 ) ? "0" . $number : $number;
}

function cleanCharacter( $str = "" )
{
   // $str = addslashes($str);
    $str = str_replace("'" , "&#39;" , $str);
    return $str;
}

function createPath($path){
    if (is_dir($path)) return true;
    $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
    $return = createPath($prev_path);
    return ($return && is_writable($prev_path)) ? mkdir($path , 0775) : false;
}

function json_readable_encode($in, $indent = 0, Closure $_escape = null)
{
    if (__CLASS__ && isset($this))
    {
        $_myself = array($this, __FUNCTION__);
    }
    elseif (__CLASS__)
    {
        $_myself = array('self', __FUNCTION__);
    }
    else
    {
        $_myself = __FUNCTION__;
    }

    if (is_null($_escape))
    {
        $_escape = function ($str)
        {
            return str_replace(
                array('\\', '"', "\n", "\r", "\b", "\f", "\t", '/', '\\\\u'),
                array('\\\\', '\\"', "\\n", "\\r", "\\b", "\\f", "\\t", '\\/', '\\u'),
                $str);
        };
    }
    $out = '';
    foreach ($in as $key=>$value)
    {
        $out .= str_repeat("\t", $indent + 1);
        $out .= "\"".$_escape((string)$key)."\": ";

        if (is_object($value) || is_array($value))
        {
            $out .= "\n";
            $out .= call_user_func($_myself, $value, $indent + 1, $_escape);
        }
        elseif (is_bool($value))
        {
            $out .= $value ? 'true' : 'false';
        }
        elseif (is_null($value))
        {
            $out .= 'null';
        }
        elseif (is_string($value))
        {
            $out .= "\"" . $_escape($value) ."\"";
        }
        else
        {
            $out .= $value;
        }

        $out .= ",\n";
    }

    if (!empty($out))
    {
        $out = substr($out, 0, -2);
    }

    $out = str_repeat("\t", $indent) . "{\n" . $out;
    $out .= "\n" . str_repeat("\t", $indent) . "}";

    return $out;
}

if(!function_exists('splitName')) {

    /**
     * Split Full Name
     * @param  [string] $name Full name (name midname lastname)
     * @return [array] [first_name , middle_name , last_name]
     */
    function splitName($name = null) {
        $name = preg_replace('/[\s]/i', ' ', $name);
        $parts = explode(' ', $name);
        $name = array();
        $name['first_name'] = array_shift($parts);
        $name['middle_name'] = "";
        $name['last_name'] = array_pop($parts);
        $name['middle_name'] = implode(' ', $parts);

        return $name;
    }
}


