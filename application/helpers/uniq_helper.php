<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// 
// Get unique ID
// 
function get_uniq_id(){
    $CI = & get_instance();

    $uniq = $CI->session->get_user('uniq');
    if(!$uniq) {
        $uniq = gen_uniq_id();
        $CI->session->set_user('uniq', $uniq);
    }
    return $uniq;
}

function gen_uniq_id() {
    $CI = & get_instance();

    $digit = 6; # Number of digit for order id (Include year number for 2 first digit)

    $space = "";
    for ($i = 1; $i <= $digit; $i++) {
        $space.="0";
    }

    $uniqid = date("y") . substr($space . "1", (($digit - 2) * -1));

    $last_uniq = $CI->merchant_model->get_last_uniq_id();
    //echo $last_uniq;
    //echo "<br>";
    if ($last_uniq) {
        $uniqid = substr($last_uniq, (($digit - 2) * -1)) + 1;
        $uniqid = date("y") . substr($space . $uniqid, (($digit - 2) * -1));
    }

    // $prefix = 'E' . substr($CI->session->get_user('billing=>state'), 0, 1) . 'SC';
    $prefix = '';

    //echo  $prefix.$uniqid;
    return $prefix . $uniqid;
}