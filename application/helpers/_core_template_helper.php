<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('template')) {
	function template($view, $data = array()){
		$CI = & get_instance();

		$customView =  $CI->load->view( "custom" , "" , true );

		$CI->load->vars(array(
		    'user_session' => $CI->session->get_user(),
		    'category'     => $CI->shop_name,
		    'shopid'       => $CI->shop_id,
		    'customView'   => $customView
		));

		$CI->load->view('_top', $data);
		$CI->load->view('_global_js_variable');
		$CI->load->view('includes');
		$CI->load->view($view, $data);
		$CI->load->view('_bottom');
		$data['url'] = '/'.$CI->shop['name'];
		// if ($CI->settings['testmode'] == '1' or DEVSTATE) {
		if (DEVSTATE) {
		    $CI->load->view('test_badge',$data);
		}
	}
}