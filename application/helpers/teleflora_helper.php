<?php
if ( ! function_exists('requestInformationTeleFlora') )
{
    function requestInformationTeleFlora()
    {
        $CI = & get_instance();
        $user_session = $CI->session->get_user();
        $cart_session = $CI->session->userdata;
        $shop =  $CI->load->get_var('shops');

        $telefloraInfo = json_decode( Vars::get($shop['teleflora_info'] , "{}") , true);
        $products = array();

        foreach( $CI->cart->contents() as $key => $row  ) {
            $productTotal = Vars::get($row['subtotal'] , 0) + Vars::get($user_session['cart']['delivery_location_rate'] , 0);
            if ( Vars::get($user_session['cart']['taxrate'] , 0) > 0 )
            {
                $totalTax = Vars::get($row['subtotal']) * Vars::get($user_session['cart']['taxratePercent'] , 0);
                if ( $shop['delivery_taxable'] )
                {
                    $totalTax = $productTotal * Vars::get($user_session['cart']['taxratePercent'] , 0);
                }
                $productTotal += $totalTax;
            }

            $nameItems = splitName( Vars::get($user_session['delivery']['deliver_decedent']) );

            $products[ Vars::get($row['id']) ] = array(
                //Config Information
                'telefloraID'        => Vars::get($telefloraInfo['token']) ,
                'Orderdate'          => date("m/d/Y" , Vars::get($user_session['purchasedateint'] , strtotime('now')) ) ,

                'Partner'            => Vars::get($telefloraInfo['partner']) ,

                #https://funeralnet.teamwork.com/#tasks/10193949
                'Recipientfirstname' => $nameItems['first_name']  ,
                'Recipientlastname'  => $nameItems['last_name'] ,
                'Recipientaddress1'  => Vars::get( $user_session['billing']['streetaddress'] ) ,
                'Recipientaddress2'  => '' ,
                'Recipientcity'      => Vars::get( $user_session['billing']['city'] ) ,
                'Recipientstate'     => Vars::get( $user_session['billing']['state'] ) ,
                'Recipientzip'       => Vars::get( $user_session['billing']['zip'] ) ,
                'Recipientphone1'    => Vars::get( $user_session['billing']['daytimephone'] )  ,
                'Recipientphone2'    => '' ,

                // 'Recipientfirstname' => 'FuneralNet' ,
                // 'Recipientlastname'  => '' ,
                // 'Recipientaddress1'  => '1215 SE Ivon, Suite 200' ,
                // 'Recipientaddress2'  => '' ,
                // 'Recipientcity'      => 'Portland' ,
                // 'Recipientstate'     => 'OR' ,
                // 'Recipientzip'       => '97202' ,
                // 'Recipientphone1'    => '800-721-8166' ,
                // 'Recipientphone2'    => '800-721-8166' ,


                // 'Recipientfirstname' => $nameItems['first_name'] ,
                // 'Recipientlastname'  => $nameItems['last_name']  ,
                // 'Recipientaddress1'  => Vars::get( $user_session['delivery']['deliver_streetaddress'] )  ,
                // 'Recipientaddress2'  => Vars::get( $user_session['delivery']['deliver_streetaddress'] )  ,
                // 'Recipientcity'      => Vars::get( $user_session['delivery']['deliver_city'] )  ,
                // 'Recipientstate'     => Vars::get( $user_session['delivery']['deliver_state'] )  ,
                // 'Recipientzip'       => Vars::get( $user_session['delivery']['deliver_zip'] )  ,
                // 'Recipientphone1'    => Vars::get( $user_session['delivery']['deliver_daytimephone'] )  ,
                // 'Recipientphone2'    => Vars::get( $user_session['delivery']['deliver_daytimephone'] )  ,

                'Storeaccountnum'    => Vars::get($telefloraInfo['store_number']) ,


                //Product Information
                'Firstchoiceline1' => Vars::get($row['name']) ,
                'Totalprice'       => number_format( $productTotal , 2 , '.' , "") ,
                'Price'            => number_format( Vars::get($row['subtotal'] , 0) , 2 , '.' , "")  ,
                'Tax'              => Vars::get($user_session['cart']['taxrate']) ,
                'ServiceFee'       => $user_session['cart']['delivery_location_rate'] ,
                'Deliverydate'     => Vars::get($user_session['delivery']['datedelivery']) ,
                'Confirmationid'   => Vars::get($user_session['uniq']) ,
                'Cardmsg1'         => Vars::get($user_session['delivery']['deliver_message']) ,

               // 'Instruction1'     => Vars::get($user_session['delivery']['deliver_instructions']) ,
                'Instruction1'     => Vars::get($user_session['delivery']['deliver_streetaddress']) ,
                'Instruction2'     => Vars::get( $user_session['delivery']['deliver_city'] ) ,
                'Instruction3'     => Vars::get( $user_session['delivery']['deliver_state'] ) . " " .
                                      Vars::get( $user_session['delivery']['deliver_zip'] )   ,
                'Instruction4'     => Vars::get( $user_session['delivery']['deliver_daytimephone'] ) ,

                //User Information
                'Username'          => 'FuneralNet' ,
                'Userphone1'        => '800-721-8166',
                'Useremail'         => 'info@funeralnet.com' ,

                'Cardholderaddress' => '1215 SE Ivon, Suite 200' ,
                'Cardholdercity'    => 'Portland' ,
                'Cardholderstate'   => 'OR' ,
                'Cardholderzip'     => '97202'  ,


                // 'Username'          => Vars::get( $user_session['billing']['firstname'] ) . " " . Vars::get( $user_session['billing']['lastname'] ) ,
                // 'Userphone1'        => Vars::get( $user_session['billing']['daytimephone'] ) ,
                // 'Useremail'         => Vars::get( $user_session['billing']['email'] ) ,
                //

                //'CCNumber'          => $CI->input->post('ccnumber') ,
                //'Expirationdate'    => $CI->input->post('ccmonth') .  $CI->input->post('ccmonth') ,

                'Cardholdername'    =>  $CI->input->post('ccname')

                // 'Cardholderaddress' => Vars::get( $user_session['billing']['streetaddress'] ) ,
                // 'Cardholdercity'    => Vars::get( $user_session['billing']['city'] ) ,
                // 'Cardholderstate'   => Vars::get( $user_session['billing']['state'] ) ,
                // 'Cardholderzip'     => Vars::get( $user_session['billing']['zip'] )
                //'Creditauthcode'    => $CI->input->post('cccode')
            );
        }
        return $products;
    }
}

if ( ! function_exists('send_teleflora') )
{
    function send_teleflora(  )
    {

        $CI = & get_instance();

        require_once("/home/funeralnet/public_html/mfscommon/php/teleflora/Teleflora.php");

        $rti = new Teleflora();

        $products = requestInformationTeleFlora();

        if ( sizeof($products) > 0 )
        {
            // if (is_dev()) {
            //    $rti->setupTestMode(1);
            // }

            $status = $rti->send($products);
            $CI->session->set_user("teleflora_request" , array( "request" => $rti->getDebugLogs() ) );

            if ( ! $status  )
            {
               $message = json_readable_encode( $rti->getErrorLogs()  );
               createErrorTeleflora( $message );
               return false;
            }

            return true;
        }
    }
}


if ( ! function_exists('countTelefloraItem') )
{
    function countTelefloraItem()
    {
        $CI = & get_instance();
        $quan = 0;
        foreach( (array)$CI->cart->contents() as $key => $row )
        {
            $quan += $row['qty'];
        }
        return $quan;
    }
}

if ( ! function_exists('calulateTelefloraDelivery') )
{
    function calulateTelefloraDelivery( $delivery_fee = 0 )
    {
        return $delivery_fee * countTelefloraItem();
    }
}

if ( ! function_exists('createErrorTeleflora') )
{
    function createErrorTeleflora($message)
    {
        $CI = & get_instance();
        $user_session = $CI->session->get_user();
        $shop =  $CI->merchant_model->get_shops();

        $file = Vars::get($user_session['uniq'] , 'teleflora' ) . ".txt";
        $dirLogs = 'application/cache/log/teleflora/'.$shop['client_id'].'/' . date("Y/m/d/");
        createPath( $dirLogs );
        if( ! file_exists( $dirLogs. $file )){
            $fp = fopen($dirLogs. $file , 'wb');
            fwrite($fp, $message );
            fclose($fp);
        }
        else
        {
            if ( $logContent = fopen( $dirLogs. $file  , 'a' ) )
            {
                flock($logContent, LOCK_EX);
                fwrite($logContent, $message);
                flock($logContent, LOCK_UN);  # Release the write lock.
                fclose($logContent);
            }
        }
    }
}

 // $config = array(
    //     'TelefloraID'   => $shop['teleflora_token'] , //48541700
    //     'OrderDate'     => date("Y-m-d H:i:s" , Vars::get($user_session['purchasedateint'] , strtotime('now')) ) ,
    //     'UserFirstName' => Vars::get( $user_session['billing']['firstname'] ) ,
    //     'UserLastName'  => Vars::get( $user_session['billing']['lastname'] ) ,
    //     'UserAddress1'  => Vars::get( $user_session['billing']['streetaddress'] ) ,
    //     'UserCity'      => Vars::get( $user_session['billing']['city'] ) ,
    //     'UserState'     => Vars::get( $user_session['billing']['state'] ) ,
    //     'UserZip'       => Vars::get( $user_session['billing']['zip'] ) ,
    //     'UserCountry'   => 'United States' ,
    //     'UserPhone1'    => Vars::get( $user_session['billing']['daytimephone'] ) ,
    //     'UserEmail'     => Vars::get( $user_session['billing']['email'] )
    // );

 // $products[ Vars::get($row['id']) ] = array(
        //     'TotalAmount'        => number_format( $productTotal , 2 , '.' , "") ,
        //     'ProductID'          => Vars::get($row['id']) ,
        //     'DeliveryFee'        => $user_session['cart']['delivery_location_rate'] ,
        //     'DeliveryDate'       => Vars::get($user_session['delivery']['datedelivery']) . " " .
        //     Vars::get($user_session['delivery']['hoursdelivery']) . ":" .
        //     Vars::get($user_session['delivery']['minutesdelivery']) . ":00" ,
        //     'ProductName'        => Vars::get($row['name']) ,
        //     'ItemNumber'         => (Vars::get($row['options']['teleflora_sku'] ) != "") ? $row['options']['teleflora_sku'] : $row['name'] ,
        //     'Tax'                => Vars::get($user_session['cart']['taxrate']) ,
        //     'Price'              => number_format( Vars::get($row['subtotal'] , 0) , 2 , '.' , "")  ,
        //     'CardMsg1'           => Vars::get($user_session['delivery']['deliver_message']) ,
        //     'CardTypeID'         => 2 ,
        //     'Instruction'        => Vars::get($user_session['delivery']['deliver_instructions']) ,
        //     'RecipientCity'      => Vars::get( $user_session['billing']['city'] ) ,
        //     'RecipientState'     => Vars::get( $user_session['billing']['state'] ) ,
        //     'RecipientZip'       => Vars::get( $user_session['billing']['zip'] ) ,
        //     'RecipientCountry'   => 'United States' ,
        //     'RecipientPhone1'    => Vars::get( $user_session['billing']['daytimephone'] ) ,
        //     'RecipientEmail'     => Vars::get( $user_session['billing']['email'] ) ,
        //     'RecipientFirstName' => Vars::get( $user_session['billing']['firstname'] ) ,
        //     'RecipientLastName'  => Vars::get( $user_session['billing']['lastname'] ) ,
        //     'RecipientAddress1'  => Vars::get( $user_session['billing']['streetaddress'] ) ,
        //     'LocationType'       => Vars::get( $user_session['delivery']['deliver_firstname'] ) ,
        //     'CategoryName'       => Vars::get($row['options']['category_label']) ,
        //     'LuxuryFlag'         => 0 ,
        //     'PromotionPrice'     => 0 ,
        //     'PromotionPercent'   => 0 ,
        //     'Signature'          => Vars::get( $user_session['billing']['firstname'] ) . " " . Vars::get( $user_session['billing']['lastname'] ) ,
        //     'WireFee'            => 0 ,
        //     'AdditionalFee'      => 0 ,
        //     'ServiceFeeDiscount' => 0 ,
        //     'PromotionProductAmount' => 0 ,
        //     'PromotionUpgradeAmoun' => 0 ,
        //     'PromotionAddonAmount' => 0 ,
        //     'UpsellTotal' => number_format( $productTotal , 2 , '.' , "")
        // );

        // switch ($row['grade']) {
        //     case 'Good':
        //        $products[ Vars::get($row['id']) ]['PricePoint'] = 1;
        //        $products[ Vars::get($row['id']) ]['RegularPrice'] = $row['price'];
        //     break;
        //     case 'Better':
        //        $products[ Vars::get($row['id']) ]['PricePoint'] = 2;
        //        $products[ Vars::get($row['id']) ]['DeluxePrice'] = $row['price'];
        //     break;
        //     default:
        //        $products[ Vars::get($row['id']) ]['PricePoint'] = 3;
        //        $products[ Vars::get($row['id']) ]['PremiumPrice'] = $row['price'];
        //     break;
        // }