<?php
require_once('/home/funeralnet/public_html/mfscommon/php/mail.php');
require_once('/home/funeralnet/public_html/mfscommon/php/sms.php');

function email_template($file, $arr = array()) {
    $CI = & get_instance();
    $html = '';

    $template = fopen($file, 'r') or die("Template : Couldn't open for reading \"$file\"\n");
    while (!feof($template)) {
        $html .= fgets($template, 1024);
    }
    fclose($template);

    $html = @preg_replace('/\$(\w+)\$/e', '$arr["$1"]', $html);

    return $html;
}