<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// 
// Prepare content of notification emails
// 
if(!function_exists("prepare_email_content")) {
    function prepare_email_content(){
        $CI = & get_instance();
        $user_session = $CI->session->get_user();

        $settings = $CI->settings;

        // 
        // Purchase Information
        // 
        $CI->tablemessage->putMesTbl('[clear]');
        foreach ($CI->cart->contents() as $pid => $product) {
            /* insert image */
            $img = "<img class='img-responsive ' height='70' src='https://fnetclients.s3.amazonaws.com/merchant/clients/".$CI->shop['client_id']."/pictures/".$product['options']['thumb']."' alt='".$product['name']."'>";
            $CI->tablemessage->putMesTbl($img);
            $choice = "";
            if(@$product['options']['advance_price']['use-advance-pricing']) {
                $choice = ' (' . @$product['options']['advance_price']['prices'][$product['options']['selected_price']]['label'] . ')';
            } elseif(@$product['options']['price2'] > 0 or @$product['options']['price3'] > 0) {
                $choice = " (".$settings['shop_config']['priceLabel'][$product['options']['selected_price']].")";
            }
            $CI->tablemessage->putMesTbl($product['qty'] . 'x ' . $product['name'] . $choice, '$' . number_format($product['price'] * $product['qty'], 2));
            if(count($product['selected_options'])) {
                foreach((array)$product['selected_options'] as $option) {
                    $CI->tablemessage->putMesTbl('[indent]', @$option['label'].": ".@$option['value']);
                }
            }
        }
        $CI->tablemessage->putMesTbl('<b>Sub Total</b>', "$".number_format($user_session['summary']['totalWithTax'], 2));
        $CI->tablemessage->putMesTbl('<b>Sales Tax ('.@$user_session['salestax_rate']['tax_rate'].'%)</b>', "$".number_format($user_session['summary']['taxAmount'], 2));
        $CI->tablemessage->putMesTbl('<b>Delivery Fee</b>', "$".number_format($user_session['summary']['deliveryFee'], 2));
        $purchaseinfo = $CI->tablemessage->putMesTbl('<b>Total</b>', "$".number_format($user_session['summary']['total'], 2));
        $CI->session->set_user('purchaseinfo', $purchaseinfo);
        // 
        // Purchase Information - no price version
        // 
        $CI->tablemessage->putMesTbl('[clear]');
        foreach ($CI->cart->contents() as $pid => $product) {
            /* insert image */
            $img = "<img class='img-responsive ' height='70' src='https://fnetclients.s3.amazonaws.com/merchant/clients/".$CI->shop['client_id']."/pictures/".$product['options']['thumb']."' alt='".$product['name']."'>";
            $choice = "";
            if(@$product['options']['advance_price']['use-advance-pricing']) {
                $choice = ' (' . @$product['options']['advance_price']['prices'][$product['options']['selected_price']]['label'] . ')';
            } elseif(@$product['options']['price2'] > 0 or @$product['options']['price3'] > 0) {
                $choice = " (".$settings['shop_config']['priceLabel'][$product['options']['selected_price']].")";
            }
            $CI->tablemessage->putMesTbl($img, $product['qty'] . ' x ' . $product['name'] . $choice);
            if(count($product['selected_options'])) {
                foreach((array)$product['selected_options'] as $option) {
                    $CI->tablemessage->putMesTbl('[indent]', @$option['label'].": ".@$option['value']);
                }
            }
        }
        $CI->tablemessage->putMesTbl("<b>Message</b>",@$user_session['delivery']['deliver_message']);
        $purchaseinfonoprice = $CI->tablemessage->putMesTbl('<b>Decedent</b>',@$user_session['delivery']['deliver_decedent']);
        $CI->session->set_user('purchaseinfonoprice', $purchaseinfonoprice);

        // 
        // Purchaser Name
        // 
        $user_session = $CI->session->set_user('purchaser', $user_session['billing']['firstname'] ." ". $user_session['billing']['lastname']);

        // 
        // Contact
        // 
        $CI->tablemessage->putMesTbl('[clear]');
        $CI->tablemessage->putMesTbl("<b>Name</b>", $user_session['purchaser']);
        $CI->tablemessage->putMesTbl("<b>Email</b>", $user_session['billing']['email']);
        $CI->tablemessage->putMesTbl("<b>Address</b>", $user_session['billing']['streetaddress'].' '.$user_session['billing']['city'].', '.$user_session['billing']['state'] ." ". $user_session['billing']['zip'] );
        $contact = $CI->tablemessage->putMesTbl("<b>Daytime Phone</b>", $user_session['billing']['daytimephone']);
        $CI->session->set_user('contact', $contact);

        // 
        // Purchased By
        // 
        $CI->session->set_user('purchasedby', $contact);

        // 
        // Deliver to
        // 
        $CI->tablemessage->putMesTbl('[clear]');
        $CI->tablemessage->putMesTbl('<b>To</b>', $user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname']);
        $deliveryto = $CI->tablemessage->putMesTbl('<b>Address</b>', $user_session['delivery']['deliver_streetaddress'].' '.$user_session['delivery']['deliver_city'].', '.$user_session['delivery']['deliver_state'].' '.$user_session['delivery']['deliver_zip']);
        $CI->session->set_user('deliveryto', $deliveryto);

        // 
        // Delivery
        // 
        $deliverymsg = $CI->tablemessage->putMesTbl('<b>Date</b>', date("F d, Y", @strtotime(@$user_session['delivery']['datedelivery'])));
        $CI->session->set_user('deliverymsg', $deliverymsg);

        // 
        // Description
        // 
        $CI->tablemessage->putMesTbl('[clear]');
        // $CI->tablemessage->putMesTbl('Arrangement:');
        foreach ($CI->cart->contents() as $pid => $product) {
            $choice = "";
            if(@$product['options']['advance_price']['use-advance-pricing']) {
                $choice = ' (' . @$product['options']['advance_price']['prices'][$product['options']['selected_price']]['label'] . ')';
            } elseif(@$product['options']['price2'] > 0 or @$product['options']['price3'] > 0) {
                $choice = " (".$settings['shop_config']['priceLabel'][$product['options']['selected_price']].")";
            }
            $CI->tablemessage->putMesTbl($product['qty'] . 'x ' . $product['name'] . $choice, '$' . number_format($product['price'] * $product['qty'], 2));
        }
        $CI->tablemessage->putMesTbl('<b>Decedent</b>',$user_session['delivery']['deliver_decedent']);
        $CI->tablemessage->putMesTbl('<b>Ordered By</b>',$user_session['purchaser']);
        $CI->tablemessage->putMesTbl("<b>Message To Appear On Card</b>",$user_session['delivery']['deliver_message']);
        $CI->tablemessage->putMesTbl("<b>Special Instructions</b>",$user_session['delivery']['deliver_instructions']);
        $description = $CI->tablemessage->putMesTbl("<b>Delivery Date</b>", date("F d, Y", @strtotime(@$user_session['delivery']['datedelivery'])));
        // $CI->tablemessage->putMesTbl("Grave location",$user_session['delivery']['deliver_firstname']);
        $CI->session->set_user('description', $description);


        $CI->session->set_user('shopname', $CI->shop['label']);
        // $CI->session->set_user('date', date('l jS \of F Y h:i:s A'));
        $CI->session->set_user('date', date('F d, Y'));
        $CI->session->set_user('url', base_url());
        $user_session = $CI->session->get_user();

        // 
        // Credit Card Information
        // 
        if($CI->shop['payment_gateway'] == 'Static-Email') {
            $CI->tablemessage->putMesTbl('[clear]');
            $CI->tablemessage->putMesTbl('<b>Name on card</b>', $user_session['insurance']['ccname']);
            $CI->tablemessage->putMesTbl('<b>Card Number</b>', $user_session['insurance']['ccnumber']);
            $CI->tablemessage->putMesTbl('<b>Expiry date MM/YY</b>', $user_session['insurance']['ccmonth']."/".$user_session['insurance']['ccyear']);
            $ccinfo = $CI->tablemessage->putMesTbl('<b>CVV</b>', $user_session['insurance']['cccode']);
            $CI->session->set_user('ccinfotbl', $ccinfo);
        }
    }
}

// 
// Perform sending out notification emails (for accepted payment);
// 
if(!function_exists("perform_sending_email")) {
    function perform_sending_email() {
        $CI = & get_instance();

        // Prepare email content
        prepare_email_content();


        // Get user session
        $user_session = $CI->session->get_user();
        
        // Get provider information
        $CI->load->library('provider');
        $emailpovider = $CI->provider->getDetailProvider($user_session['delivery']['deliverto']);
        $CI->session->set_user('provider', $emailpovider);

        $sent_emails = @$user_session['sent_emails'];

        //Send to customer
        $path = 'assets/templates/ecommerce_customer.tmpl.html';
        if(!file_exists($path)) {
            $path = '/home/funeralnet/public_html/mfscommon/programs/merchant/'.$path;
        }
        if(file_exists($path)) {
            $message_customer = email_template($path, $user_session);
      
            $to = @$user_session['billing']['email'];
            $subject = 'Thank You for Your '.$CI->shop['label'].' Order';
            if (is_debugger(@$user_session['billing']['email'])) {
                $subject = $subject.' - [for customer]';
            }

            send_mail($CI->shop['email_from'], $to, $subject, $message_customer);
            $sent_emails[$path] = 'Email was sent.';
        } else {
            $sent_emails[$path] = array(
                'Email was not sent.',
                'Cannot open an email template.',
                );
        }

        //Send to delivery location
        $path = 'assets/templates/ecommerce_deliver.tmpl.html';
        if(!file_exists($path)) {
            $path = '/home/funeralnet/public_html/mfscommon/programs/merchant/'.$path;
        }
        if(file_exists($path)) {
            if(@$user_session['delivery']['deliver_email']){
                $message_deliver = email_template($path, $user_session);
                $to = @$user_session['delivery']['deliver_email'];
                $subject = 'New '. $CI->shop['label'].' Order for '.$user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname'];
                if (is_debugger(@$user_session['billing']['email'])) {
                    $subject = $subject." - [for delivery][$to]";
                    $to = @$user_session['billing']['email'];
                }
                send_mail($CI->shop['email_from'], $to, $subject, $message_deliver);
                $sent_emails[$path] = 'Email was sent.';
            } else {
                $sent_emails[$path] = array(
                    'Email was not sent.',
                    'No deliver email.',
                    );
            }
        } else {
            $sent_emails[$path] = array(
                'Email was not sent.',
                'Cannot open an email template.',
                );
        }

        //Send to shop
        $path = 'assets/templates/ecommerce_shop.tmpl.html';
        if(!file_exists($path)) {
            $path = '/home/funeralnet/public_html/mfscommon/programs/merchant/'.$path;
        }
        if(file_exists($path)) {
            $message_shop = email_template($path, $user_session);
            eval('$email_to = ' . $CI->shop['email_to'] . ';');
            $subject = 'New '.$CI->shop['label'].' Order for '.$user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname'].' - [CC Charged]';
            if(is_array($email_to)) {
                foreach($email_to as $i=>$mail) {
                    if (is_debugger(@$user_session['billing']['email'])) {
                        $subject = $subject." - [for shop][$mail]";
                        $mail = @$user_session['billing']['email'];
                    }
                    send_mail($CI->shop['email_from'], $mail, $subject, $message_shop);
                }
            } elseif(@$email_to) {
                if (is_debugger(@$user_session['billing']['email'])) {
                    $subject = $subject." - [for shop][$email_to]";
                    $email_to = @$user_session['billing']['email'];
                }
                send_mail($CI->shop['email_from'], $email_to, $subject, $message_shop);
            }
            $sent_emails[$path] = 'Email was sent.';
        } else {
            $sent_emails[$path] = array(
                'Email was not sent.',
                'Cannot open an email template.',
                );
        }

        //Send to provider
        $path = 'assets/templates/ecommerce_provider.tmpl.html';
        if(!file_exists($path)) {
            $path = '/home/funeralnet/public_html/mfscommon/programs/merchant/'.$path;
        }
        if(file_exists($path)) {
            $message_shop = email_template($path, $user_session);
            $to = @$emailpovider['email'];
            // $subject = 'New '.$CI->shop['label'].' Order for '.@$emailpovider['name'];
            $subject = 'New '.$CI->shop['label'].' Order for '.$user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname'];
            if (is_debugger(@$user_session['billing']['email'])) {
                $subject = $subject." - [for provider][$to]";
                $to = @$user_session['billing']['email'];
            }
            send_mail($CI->shop['email_from'], $to, $subject, $message_shop);
            $sent_emails[$path] = 'Email was sent.';
        } else {
            $sent_emails[$path] = array(
                'Email was not sent.',
                'Cannot open an email template.',
                );
        }

        $CI->session->set_user('sent_emails', $sent_emails);
    }
}

// 
// Perform sending out notification emails (for declined payment);
// 
if(!function_exists("perform_sending_declined_email")) {
    function perform_sending_declined_email() {
        $CI = & get_instance();

        // Prepare email content
        prepare_email_content();

        // Get user session
        $user_session = $CI->session->get_user();
        $sent_emails = @$user_session['sent_emails'];

        //Send to shop
        $path = 'assets/templates/ecommerce_shop_declined.tmpl.html';
        if(!file_exists($path)) {
            $path = '/home/funeralnet/public_html/mfscommon/programs/merchant/'.$path;
        }
        if(@file_exists($path)) {
            $message_shop = email_template($path, $user_session);
            eval('$email_to = ' . $CI->shop['email_to'] . ';');
            $subject = 'New '.$CI->shop['label'].' Order Payment Was Declined. - [CC Denied]';
            if (is_debugger(@$user_session['billing']['email'])) {
                $email_to = @$user_session['billing']['email'];
                $subject = $subject.' - [for shop]';
            }
            if(is_array($email_to)) {
                foreach($email_to as $i=>$mail) {
                    send_mail($CI->shop['email_from'], $mail, $subject, $message_shop);
                }
            } elseif(@$email_to) {
                send_mail($CI->shop['email_from'], $email_to, $subject, $message_shop);
            }
            $sent_emails[$path] = 'Email was sent.';
        } else {
            $sent_emails[$path] = array(
                'Email was not sent.',
                'Cannot open an email template.',
                );
        }
        $CI->session->set_user('sent_emails', $sent_emails);
    }
}