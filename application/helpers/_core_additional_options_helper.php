<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists("split_choices")) {
	function split_choices($choices) {
		$pattern = '/([^,]*[\(]*[\$]*[0-9\,\.]*[\)]*)(,)/i';
		$replace = '$1[:split:]';
		// $choices = trim(preg_replace($pattern, $replace, trim($choices,",").","),"[:split:]");
		$choices = preg_replace($pattern, $replace, trim($choices,","));
		$choices = explode('[:split:]', $choices);
		return $choices;
	}
}
