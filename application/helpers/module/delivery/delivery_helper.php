<?php 

if ( ! function_exists('loadCustomDataDelivery')) {
	function loadCustomDataDelivery() {
		$CI = & get_instance();
		$shops = $CI->load->get_var('shops');

		$off_dates_records = get_off_dates_records();
		extract($off_dates_records);

		$now = date('m/d/Y');
		$next_service_date = $now;

		// If no same day service, then plus 1 day
		if($shops['allow_same_day'] == 0) {
			$next_service_date = date('m/d/Y', strtotime("+1 days", strtotime($next_service_date)));
		}
		$next_service_date = next_service_date(strtotime($next_service_date), $off_records);
		if($next_service_date) {
			$next_service_date = date("m/d/Y", $next_service_date);
		}

		$vars = array(
			'now' => $now,
			'next_service_date' => $next_service_date,
			'off_state' => $off_state,
			'temporary_off_records' => $temporary_off_records,
			'temporary_off_dates' => $temporary_off_dates,
			'permanent_off_records' => $permanent_off_records,
			'permanent_off_dates' => $permanent_off_dates,
			'off_records' => $off_records,
		);
		$CI->load->vars($vars);
	}
}

if ( ! function_exists('next_service_date')) {
	function next_service_date($available_date = false, $off_records = array()) {
		static $round = 1;
		if(++$round > 100) {
			devprint("Getting infinit loop!");
			return false;
		}

		// $CI = & get_instance();
		// $shops = $CI->load->get_var('shops');

		if(!$available_date) {
			$available_date = time();
		}

		$off_record = array_shift($off_records);
		if($off_record) {
			if(strtotime($off_record['start']) <= $available_date) {
				if($off_record['end']) {
					return next_service_date(strtotime("+1 days", strtotime($off_record['end'])), $off_records);
				} else {
					return false;
				}
			} elseif(count($off_records)) {
				return next_service_date($available_date, $off_records);
			}
		}
		return $available_date;
	}
}

if ( ! function_exists('is_available_date')) {
	function is_available_date($date = false) {

		// $CI = & get_instance();
		// $shops = $CI->load->get_var('shops');
		$date = strtotime($date);
		if($date === false) {
			$date = time();
		}

		$off_dates_records = get_off_dates_records();
		extract($off_dates_records);

		$available = true;
		foreach((array)$off_records as $off_record) {
			if(strtotime($off_record['start']) <= $date) {
				if($off_record['end'] and $date <= strtotime($off_record['end'])) {
					$available = false;
				} elseif(!$off_record['end']) {
					$available = false;
				}
			}
		}

		return $available;
	}
}

if ( ! function_exists('get_off_dates_records')) {
	function get_off_dates_records() {
		$CI = & get_instance();
		$shops = $CI->load->get_var('shops');
		$off_state = false;
		// $off_records
		$off_records = $CI->merchant_model->get_shop_off_records();
		// $temporary_off_records
		$temporary_off_records = array_filter($off_records, function($rec) {
			return strtotime($rec['start']) !== false and strtotime($rec['end']) !== false;
		});
		// $temporary_off_dates
		$temporary_off_dates = array();
		foreach((array)$temporary_off_records as $rec) {
			$sDate = strtotime(date("Y-m-d 00:00:00", strtotime($rec['start'])));
			$eDate = strtotime(date("Y-m-d 23:59:59", strtotime($rec['end'])));
			for($d=$sDate;$d<=$eDate;$d+=86400) {
				$temporary_off_dates[] = date('Y-m-d', $d);
			}
			if($sDate < time() and time() < $eDate) {
				$off_state = true;
			}
		}
		// $permanent_off_records
		$permanent_off_records = array_filter($off_records, function($rec) {
			return strtotime($rec['start']) !== false and strtotime($rec['end']) === false;
		});
		// $permanent_off_dates
		$permanent_off_dates = array();
		foreach((array)$permanent_off_records as $rec) {
			$sDate = strtotime(date("Y-m-d 00:00:00", strtotime($rec['start'])));
			$permanent_off_dates[] = date('Y-m-d', $sDate);
			if($sDate < time()) {
				$off_state = true;
			}
		}

		$rt = array(
			'off_records' => $off_records,
			'off_state' => $off_state,
			'temporary_off_records' => $temporary_off_records,
			'temporary_off_dates' => $temporary_off_dates,
			'permanent_off_records' => $permanent_off_records,
			'permanent_off_dates' => $permanent_off_dates,
		);

		// Stop the OFF feature
		// if(!is_dev()) {
		// if(!is_dev() and $shops['id'] == 57) {
		if(false) {
			$rt = array(
				'off_records' => array(),
				'off_state' => false,
				'temporary_off_records' => array(),
				'temporary_off_dates' => array(),
				'permanent_off_records' => array(),
				'permanent_off_dates' => array(),
			);
		}

		return $rt;
	}
}