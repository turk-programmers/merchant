<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Cart extends CI_Cart {

    public $taxrate = 1; // tex rate for calulate summary ex. Client must user 7% of tax it just provide 7
    public $taxratePercent = 0; // tex rate for calulate summary ex. Client must user 7% of tax it just provide 7
    public $deliveryFee = 0;
    public $delieryTaxable = false;
    public $CI;
    public $product_name_rules = "[:print:]";
    //public $shop = array();

    public function __construct() {
        parent::__construct();
        $this->CI = & get_instance();

    }

    public function setTax($taxrate = 1) {
        $this->taxrate =  $taxrate;
        $this->taxratePercent = $taxrate/100;


        $this->CI->session->set_userdata('taxrate', $this->taxrate);
        $this->CI->session->set_userdata('taxratePercent', $this->taxratePercent);

        $user_session = $this->CI->session->get_user();
        $session = Vars::get($user_session['cart'] , array());
        $session['taxrate'] = $this->taxrate;
        $session['taxratePercent'] = $this->taxratePercent;
        $this->CI->session->set_user('cart' , $session);
    }

    public function setDeliveryFee($deliveryFee = 0) {
        $shop = $this->CI->load->get_var('shops');
        $user_session = $this->CI->session->get_user();

        $session = Vars::get($user_session['cart'] , array());
        $session['delivery_location_rate'] = $deliveryFee;

        if (  Vars::get($shop['is_teleflora'] , false)  )
        {
            $deliveryFee =  calulateTelefloraDelivery($deliveryFee) ;
        }

        $this->deliveryFee = $deliveryFee;
        $this->CI->session->set_userdata('deliveryRate', $this->deliveryFee );

        $session['deliveryRate'] = $this->deliveryFee;
        $this->CI->session->set_user('cart' , $session);
    }

    public function getDeliveryFee() {
        return $this->deliveryFee;
    }

    public function setDeliveryTaxable($taxable = false) {
        $this->delieryTaxable = $taxable;
    }


    public function getTax() {
        $user_session = $this->CI->session->get_user();
        return  Vars::get($user_session['cart']['taxrate'] , 0);
        //return number_format( $this->CI->session->userdata('taxrate') ,2 );
    }

    public function calulateTotal()
    {
        // devprint($this->_cart_contents);
        return  $this->_cart_contents['cart_total'] + $this->calTax() + $this->deliveryFee;
    }



    //return Summary of total and minus with tax
    public function totalWithTax() {
        //+ $this->deliveryFee
        return number_format($this->calulateTotal(), 2);
    }

    #don't have decimal

    public function totalWithTax2() {
        return $this->calulateTotal();
    }


    public function calTax() {
        $user_session = $this->CI->session->get_user();
        $cart_total = $this->_cart_contents['cart_total'];
        if ($this->delieryTaxable) {
            $cart_total += $this->deliveryFee;
        }
        return number_format( $cart_total * Vars::get($user_session['cart']['taxratePercent']) , 2 );
        // } else {
        //     return number_format($this->_cart_contents['cart_total']*$this->CI->session->userdata('taxratePercent'),2);
        // }


        // if ($this->delieryTaxable) {
        //     return number_format(($this->_cart_contents['cart_total'] + $this->deliveryFee)*$this->CI->session->userdata('taxratePercent'),2);
        // } else {
        //     return number_format($this->_cart_contents['cart_total']*$this->CI->session->userdata('taxratePercent'),2);
        // }
    }

    //Return summary of tax
    public function getTaxValue() {

        $user_session = $this->CI->session->get_user();

        return number_format( ($this->_cart_contents['cart_total'] * Vars::get($user_session['cart']['taxratePercent']) ), 2);
    }

    var $error = "";

    public function getError() {
        return $this->error;
    }

    function contents()
    {
        $user_session = $this->CI->session->get_user();
        $cart = Vars::get($user_session['cart']['cart_contents'] , array());
        $this->_cart_contents = $cart;
        // Remove these so they don't create a problem when showing the cart table
        unset($cart['total_items']);
        unset($cart['cart_total']);
        // devprint($this->_cart_contents);

        return $cart;
    }

    public function _save_cart()
    {
        // devprint($this->_cart_contents);
        parent::_save_cart();
        $session['cart_contents'] = $this->_cart_contents;
        $this->CI->session->set_user('cart' , $session);
    }

    public function _insert($items = array()) {

        // Was any cart data passed? No? Bah...
        if (!is_array($items) OR count($items) == 0) {
            log_message('error', 'The insert method must be passed an array containing data.');
            $this->error = "The insert method must be passed an array containing data.";
            return FALSE;
        }

        // --------------------------------------------------------------------
        // Does the $items array contain an id, quantity, price, and name?  These are required
        if (!isset($items['id']) OR ! isset($items['qty']) OR ! isset($items['price']) OR ! isset($items['name'])) {
            log_message('error', 'The cart array must contain a product ID, quantity, price, and name.');
            $this->error = 'The cart array must contain a product ID, quantity, price, and name.';
            return FALSE;
        }

        // --------------------------------------------------------------------
        // Prep the quantity. It can only be a number.  Duh...
        $items['qty'] = trim(preg_replace('/([^0-9])/i', '', $items['qty']));
        // Trim any leading zeros
        $items['qty'] = trim(preg_replace('/(^[0]+)/i', '', $items['qty']));

        // If the quantity is zero or blank there's nothing for us to do
        if (!is_numeric($items['qty']) OR $items['qty'] == 0) {
            $this->error = "";
            return FALSE;
        }

        // --------------------------------------------------------------------
        // Validate the product ID. It can only be alpha-numeric, dashes, underscores or periods
        // Not totally sure we should impose this rule, but it seems prudent to standardize IDs.
        // Note: These can be user-specified by setting the $this->product_id_rules variable.
        if (!preg_match("/^[" . $this->product_id_rules . "]+$/i", $items['id'])) {
            log_message('error', 'Invalid product ID.  The product ID can only contain alpha-numeric characters, dashes, and underscores');
            $this->error = 'Invalid product ID.  The product ID can only contain alpha-numeric characters, dashes, and underscores';
            return FALSE;
        }

        // --------------------------------------------------------------------
        // Validate the product name. It can only be alpha-numeric, dashes, underscores, colons or periods.
        // Note: These can be user-specified by setting the $this->product_name_rules variable.
        if (!preg_match("/^[" . $this->product_name_rules . "]+$/i", $items['name'])) {
            log_message('error', 'An invalid name was submitted as the product name: ' . $items['name'] . ' The name can only contain alpha-numeric characters, dashes, underscores, colons, and spaces');
            $this->error = 'An invalid name was submitted as the product name: ' . $items['name'] . ' The name can only contain alpha-numeric characters, dashes, underscores, colons, and spaces';
            return FALSE;
        }

        // --------------------------------------------------------------------
        // Prep the price.  Remove anything that isn't a number or decimal point.
        $items['price'] = trim(preg_replace('/([^0-9\.])/i', '', $items['price']));
        // Trim any leading zeros
        $items['price'] = trim(preg_replace('/(^[0]+)/i', '', $items['price']));

        // Is the price a valid number?
        if (!is_numeric($items['price'])) {
            log_message('error', 'An invalid price was submitted for product ID: ' . $items['id']);
            $this->error = 'An invalid price was submitted for product ID: ' . $items['id'];
            return FALSE;
        }

        // --------------------------------------------------------------------
        // We now need to create a unique identifier for the item being inserted into the cart.
        // Every time something is added to the cart it is stored in the master cart array.
        // Each row in the cart array, however, must have a unique index that identifies not only
        // a particular product, but makes it possible to store identical products with different options.
        // For example, what if someone buys two identical t-shirts (same product ID), but in
        // different sizes?  The product ID (and other attributes, like the name) will be identical for
        // both sizes because it's the same shirt. The only difference will be the size.
        // Internally, we need to treat identical submissions, but with different options, as a unique product.
        // Our solution is to convert the options array to a string and MD5 it along with the product ID.
        // This becomes the unique "row ID"
        if (isset($items['options']) AND count($items['options']) > 0) {
            $rowid = @md5($items['id'] . implode('', $items['options']));
        } else {
            // No options were submitted so we simply MD5 the product ID.
            // Technically, we don't need to MD5 the ID in this case, but it makes
            // sense to standardize the format of array indexes for both conditions
            $rowid = md5($items['id']);
        }

        // --------------------------------------------------------------------
        // Now that we have our unique "row ID", we'll add our cart items to the master array
        // let's unset this first, just to make sure our index contains only the data from this submission
        unset($this->_cart_contents[$rowid]);

        // Create a new index with our new row ID
        $this->_cart_contents[$rowid]['rowid'] = $rowid;

        // And add the new items to the cart array
        foreach ($items as $key => $val) {
            $this->_cart_contents[$rowid][$key] = $val;
        }



        // Woot!
        return $rowid;
    }

    function total_items()
    {
        $user_session = $this->CI->session->get_user();
        return Vars::get( $user_session['cart']['cart_contents']['total_items'] , 0);
    }

    function total()
    {
        $user_session = $this->CI->session->get_user();
        return Vars::get( $user_session['cart']['cart_contents']['cart_total'] , 0);
    }

    public function update($items = array())
    {
        $this->contents();
        parent::update($items);
    }


    // public function update($items = array())
    // {
    //    // extract($items);
    //     // if($qty <= 0) {
    //     //     $user_session = $this->CI->session->get_user();
    //     //     if ( isset($user_session['cart']['cart_contents'][$rowid]) )
    //     //     {
    //     //         parent::update($items);
    //     //         // unset($user_session['cart']['cart_contents'][$rowid]);
    //     //         // devprint($user_session['cart']['cart_contents']);
    //     //         // $this->CI->session->set_user('cart' , $user_session['cart']['cart_contents'] );
    //     //     }

    //     // } else {
    //     //     $this->CI->session->set_user("cart=>cart_contents=>$rowid=>qty", $qty);
    //     // }

    //    // parent::update($items);
    //    // $session['cart_contents'] = $this->_cart_contents;
    //    // $this->CI->session->set_user('cart' , $session);
    //     //$this->CI->session->set_user("cart=>cart_contents=>$rowid=>qty", $qty);


    // }

}
