<?php

/**
 * Description of Payment
 *
 * @author UseR
 */
//include_once('/home/funeralnet/public_html/mfscommon/php/merchant_classes/payment_driver/');
class Payment {

    //put your code here
    var $CI;
    var $approved = false;
    var $declined = false;
    var $duplicated = false;
    var $error = true;
    var $order = array();
    var $config = array();

    public function __construct($config) {
        //parent::__construct();
        $this->config = $config;

        $this->CI = & get_instance();
        $this->CI->load->library('payment/' . strtolower($config['gateway']) . '_driver', '', 'gateway');

        // 
        // In case paypal was called
        // 
        if(strtolower($config['gateway']) == 'paypal')  {
            $this->CI->gateway->init($config['paypal_details']);
        }
    }

    /**
     *
     * @param type $gateway
     * @param type array
     * $data = array (
     *  'email' => '',
     *  'card_name' => '',
     *  'last_name' => '',
     *  'address' => '',
     *  'city' => '',
     *  'state' => '',
     *  'zip' => '',
     *  'country' => 'US',
     *  'description' => '',
     *  'auth_login' => '',
     *  'auth_transkey'=>'',
     *  'ccnumber' => '',
     *  'expiration_of_month' => '',
     *  'expiration_of_year' => '',
     *  'amount' => '',
     *  'cccode' => '',
     *  'wonderpay_acct' => '',
     *  'wonderpay_mpin' => '',
     *
     * );
     */
    function process($data) {
        $this->CI->gateway->process($data);
    }

    public function isApproved() {
        return $this->CI->gateway->is_approved();
    }

    public function getOrder() {
        return $this->CI->gateway->get_order();
    }
    
    public function getFullResult() {
        if(method_exists($this->CI->gateway, 'get_FullResult')) {
            return $this->CI->gateway->get_FullResult();
        } else {
            return "#Gateway object does not provide a full result.";
        }
    }

    /*
     * For paypal
     */
    public function redirect_to_paypal() {
        $order = $this->CI->gateway->get_order();
        $this->CI->gateway->redirect_to_paypal($order['TOKEN']);
    }

    // function init($data) {
    //     $this->CI->gateway->init($data);
    // }

}
