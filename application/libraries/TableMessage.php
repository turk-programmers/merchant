<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TableMessage
 *
 * @author UseR
 */
class TableMessage {
    # return overview table for notification email.
    var $CI ;
    public function __construct(){
        //parent::__construct();
        $this->CI =& get_instance();
    }
    
    public function putMesTbl($label = "", $val = "[default]") {

        /* --- Usage -------------------
          putMesTbl('[clear]'); 					# clear all buffer
          putMesTbl('Title');						# title row
          putMesTbl('Label','Value');				# normal row
          putMesTbl('[colspan]','.....');			# merge row (for too long value)
          putMesTbl('[indent]','.....');			# indent row (description)
          putMesTbl('[indentitalic]','.....');  # indent row (italic description)
          putMesTbl('[fullline]','.....');	# full row with one column
          putMesTbl();							# blank row
          $_SESSION['buffer1'] = putMesTbl();		# get buffer

         */

        static $message = '';

        if (preg_match("/\[clear\]/i", $label)) {
            $message = '';
            return;
        }

        if ($label and $val != "[default]") {
            if (preg_match("/\[colspan\]/i", $label)) {
                $message.='
				<tr align=left valign=top>
					<td colspan=2 align=right>' . $val . '</td>
				</tr>
				';
            } elseif (preg_match("/\[fullline\]/i", $label)) {
                $message.='
        <tr align=left valign=top>
          <td colspan=2 align=left>' . $val . '</td>
        </tr>
        ';
            } elseif (preg_match("/\[indent\]/i", $label)) {
                $message.='
        <tr align=left valign=top>
          <td colspan=2 class="indent">' . $val . '</td>
        </tr>
        ';
            } elseif (preg_match("/\[indentitalic\]/i", $label)) {
                $message.='
				<tr align=left valign=top>
					<td colspan=2 class="indent"><i>' . $val . '</i></td>
				</tr>
				';
            } else {
                $message.='
				<tr align=left valign=top>
					<td>' . $label . '</td>
					<td align=right>' . $val . '</td>
				</tr>
				';
            }
        } elseif ($label) {
            $message.='
			<tr align=left>
				<td colspan=2><b>' . $label . '</b></td>
			</tr>
			';
        } elseif ($val != "[default]") {
            $message.='
			<tr align=left valign=top>
				<td>&nbsp;</td>
				<td align=right>' . $val . '</td>
			</tr>
			';
        } else {
            $message.='<tr><td>&nbsp;</td></tr>';
        }

        return '<style>
				.indent{
					text-indent:15px;
				}
				</style>
				<table width=500>' . $message . '</table>';
    }

    private function putAdminTbl($label = "", $val = "[default]", $param = array()) {

        /* --- Usage -------------------
          putAdminTbl('[clear]'); 					# clear all buffer
          putAdminTbl('Title');						# title row
          putAdminTbl('Label','Value');				# normal row
          putAdminTbl('[colspan]','.....');			# merge row (for too long value)
          putAdminTbl('[indent]','.....');			# indent row (description)
          putAdminTbl('[indentitalic]','.....');		# indent row (italic description)
          putAdminTbl();								# blank row
         *
          /*--- For sending more parameter -------------------------
          putAdminTbl('Title', 'Value', array(param...));			# send parameter as array(must have 'action' as one of members)

          # For action is "showdetail"
          'action' => 'showdetail',
          'detail' => $detail,

          # For action is "showlargeimage"
          'action' => 'showlargeimage',
          'url' => $large_image_url,
          'thumb' => $thumb_image_url,
         */


        $row = array();

        if (preg_match("/\[clear\]/i", $label)) {
            $this->set_user_session('admintable', array());
            return;
        }

        if ($label and $val != "[default]") {
            if (preg_match("/\[colspan\]/i", $label)) {
                $row = array(
                    'type' => 'row-colspan',
                    'value' => $val,
                );
            } elseif (preg_match("/\[indent\]/i", $label)) {
                $row = array(
                    'type' => 'row-indent',
                    'value' => $val,
                );
            } elseif (preg_match("/\[indentitalic\]/i", $label)) {
                $row = array(
                    'type' => 'row-indent-italic',
                    'value' => $val,
                );
            } else {
                $row = array(
                    'type' => 'row-normal',
                    'label' => $label,
                    'value' => $val,
                );
            }
        } elseif ($label) {
            $row = array(
                'type' => 'row-title',
                'value' => $label,
            );
        } elseif ($val != "[default]") {
            $row = array(
                'type' => 'row-value',
                'value' => $val,
            );
        } else {
            $row = array(
                'type' => 'row-empty',
            );
        }

        $row['param'] = $param;

        $this->set_user_session('admintable=>[]', $row);
    }

        public function test(){
            return "TEST";
        }
}
