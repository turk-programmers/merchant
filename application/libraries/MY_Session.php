<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MY_Session extends CI_Session {

    var $CI;

    public function __construct() {
        parent::__construct();
        $this->CI = & get_instance();
    }

    /**
     * Assign an empty array to session and that mean all value stored in session will be cleared.
     */
    function clear() {
        $_SESSION = array();
    }

    /**
     * Get a session.
     *
     * @param string $key Associate key of session you want to get.<br>Can be more than one level.<br>Ex: "legal_name" or "deceased=>legal_name".<br>Do not pass anything and you will get a whole session including a "user" key.
     * @return mixed A value stored in the session.<br>Return <b>False</b> if it's empty or not set.
     */
    function get($key = '') {
        $ses_string = '$_SESSION';

        if ($key != '') {
            $key = explode('=>', $key);
            foreach ($key as $k) {
                if ($k == '[]') {
                    $ses_string .= '[]';
                } else {
                    $ses_string .= '["' . $k . '"]';
                }
            }
        }

        $syntax = '$rt = @' . $ses_string . ' ? ' . $ses_string . ' : false;';
        eval($syntax);
        return $rt;
    }

    /**
     * Get a session underneath the "user" key.
     *
     * @param string $key Associate key of session you want to get.<br>Can be more than one level.<br>Ex: "legal_name" or "deceased=>legal_name".
     * @return mixed A value stored in the session.<br>Return <b>False</b> if it's empty or not set.
     */
    function get_user($key = '') {
        $ses_string = '$_SESSION["user"]';

        if ($key != '') {
            $key = explode('=>', $key);
            foreach ($key as $k) {
                if ($k == '[]') {
                    $ses_string .= '[]';
                } else {
                    $ses_string .= '["' . $k . '"]';
                }
            }
        }

        $syntax = '$rt = @' . $ses_string . ' ? ' . $ses_string . ' : false;';
        eval($syntax);
        return $rt;
    }

    /**
     * Save a value into a session.
     *
     * @param String $key Associate key of session you want to set.<br>Can be more than one level.<br>Ex: "legal_name" or "deceased=>legal_name".
     * @param mixed $val The value you want to save in.
     * @return mixed Whole associate array of a session.<br>The result of a <b>get_session()</b> function.
     */
    function set($key, $val) {
        $key = explode('=>', $key);
        $ses_string = '$_SESSION';
        foreach ($key as $k) {
            if ($k == '[]') {
                $ses_string .= '[]';
            } else {
                $ses_string .= '["' . $k . '"]';
            }
        }
        $ses_string .= ' = $val;';
        eval($ses_string);
        return $this->get();
    }

    /**
     * Save a value into a session underneath the "user" key automatically.
     *
     * @param string $key Associate key of session you want to set.<br>Can be more than one level.<br>Ex: "legal_name" or "deceased=>legal_name".
     * @param mixed $val The value you want to save in.
     * @return mixed Whole associate array of a "user" key in session.<br>The result of a <b>get_user_session()</b> function.
     */
    function set_user($key, $val) {
        $key = explode('=>', $key);
        $ses_string = '$_SESSION["user"]';
        foreach ($key as $k) {
            if ($k == '[]') {
                $ses_string .= '[]';
            } else {
                $ses_string .= '["' . $k . '"]';
            }
        }
        $ses_string .= ' = $val;';
        eval($ses_string);
        return $this->get_user();
    }


}
