<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Encrytp_shop {
                  
    public function encode_session($session){
        $CI =& get_instance();
        $encryption_key = $CI->settings['encryption_key'];
        $session_save = json_encode($session);
        $session_save = fn_encrypt($session_save, $encryption_key);
        return $session_save;
    }
    
    public function decode_session($session){
        $CI =& get_instance();
        $encryption_key = $CI->settings['encryption_key'];
        $session = fn_decrypt($session, $encryption_key);
        $session = json_decode($session, true);
        return $session;
    }
}
