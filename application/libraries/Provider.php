<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Getprovider
 *
 * @author UseR
 */
class Provider {
    var $CI;
    var $other = false;
    var $location = false;
    public function __construct(){
        //parent::__construct();
        $this->CI =& get_instance();          
    }
    
    /**
     * 
     * @param type $idprovider
     * @param type $clientid
     * This function for select provider 
     * The parameter $locationid is a id of location if variable is set with "other" function will be should location from shop with $clientid
     */
    public function getDetailProvider($locationid){
        //User selected other location 
        if(!$locationid){
            //No provider 
            return false;
        }
        if($locationid == "other"){
            // get location from shop            
            $provider = $this->CI->merchant_model->get_provider_shop();
            
            $lastprovider = $provider['last_provider_id'];
            $providerid = explode(",",$provider['providers']);
            
            //result provider detail
            $provider = $this->viewprovider($providerid,$lastprovider);
            
            //update last provider
            $this->CI->merchant_model->updateLastProvider_shop($provider['id']);
            
            return $provider;
        } else {
            // get location  
            $provider = $this->CI->merchant_model->get_location($locationid);
            
            $lastprovider = $provider['last_provider_id'];
            $providerid = explode(",",$provider['providers']);
            
            //result provider detail
            $provider = $this->viewprovider($providerid,$lastprovider);
            
            //update last provider
            $this->CI->merchant_model->updateLastProvider($locationid,$provider['id']);
            
            return $provider;
        }
        
    }
    
    private function viewprovider($providerid,$lastprovider){
        //more provider in this location
        if (count($providerid) >= 2 ){
            $index = 0;
            if($lastprovider != 0 or $lastprovider!='' ){
                foreach ($providerid as $key=>$value){
                    if($value == $lastprovider){
                        $index = $key;
                    }
                }
                // end of provider
                if( count($providerid)-1 == $index ){
                    $index = 0;
                } else {
                    $index = $index+1;
                }
            }            
            return $this->provider($providerid[$index]);            
        } else {
            return $this->provider($providerid[0]);
        }
    }
    
    private function provider($providerid){
        return $this->CI->merchant_model->get_provider($providerid);
    }
}
