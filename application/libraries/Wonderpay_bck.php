<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Wonderpay
{
	// Set these variables prior to use
	var $action			= 'ns_quicksale_cc';
	var $wonderpay_url  = 'https://trans.merchantpartners.com/cgi-bin/process.cgi';
	var $wonderpay_acct = ''; //Client wonderpay account ID
	var $pin = '';//Optionnal client pin phrase (found in wonderpay FRISK options)
	var $test			= 0; //set with testMode, to simulate accepted transaction
	var $debug			= '';

	var $result_fields	= array();

	var $approved		= false;
	var $declined		= false;
	var $duplicated		= false;
	var $error			= true;
	var $order			= array();
	var $log			= '';
	var $rawresult		= '';
	
	# for log message
	var $arr_log		= array();

	public function process($wonderpay_acct, $amount, $ccname, $ccnum, $expmon, $expyear, $cvv = '', $pin = ''){
		$this->arr_log['wonderpay_acct'] 	= "***".substr($wonderpay_acct,-2);
		$this->arr_log['wonderpay_mpin'] 	= "**********************".substr($pin,-10);
		$this->arr_log['ccname'] 			= $ccname;
		$this->arr_log['ccnumber'] 			= "************".substr($ccnum,-4);
		$this->arr_log['charge_amount'] 	= $amount;
		
		$this->debug .= "$wonderpay_acct, $amount, $ccname, $ccnum, $expmon, $expyear, $cvv, $pin";
		
		# Prepare the transaction data for transmission.
		$request_elements = array
		(
			'action='  . urlencode($this->action),
			'ecxid='   . urlencode($wonderpay_acct),
			'acctid='  . urlencode($wonderpay_acct),
			'amount='  . urlencode($amount),
			'ccname='  . urlencode($ccname),
			'ccnum='   . urlencode($ccnum),
			'expmon='  . urlencode($expmon),
			'expyear=' . urlencode($expyear)
		);
		if($cvv) $request_elements[] = 'cvv2=' . urlencode($cvv);
		if($pin) $request_elements[] = 'merchantpin=' . urlencode($pin);
		$data = implode('&', $request_elements);

		$this->process_data($data);
	}

	public function process_echeck($wonderpay_acct, $amount, $ckname, $ckaba, $ckacct){
		# Prepare the transaction data for transmission.
		$this->debug .= "$wonderpay_acct, $amount, $ckname, $ckaba, $ckacct";
		$request_elements = array
		(
			'action='  						. urlencode('ns_quicksale_check'),
			'acctid='  						. urlencode($wonderpay_acct),
			'amount='  						. urlencode($amount),
			'ckname='  						. urlencode($ckname),
			'ckaba='   						. urlencode($ckaba),
			'ckacct='  						. urlencode($ckacct),
		);
		$data = implode('&', $request_elements);

		$this->process_data($data);
	}

	function process_data($data){
		# Post the transaction to the WonderPay server.
		$ch = curl_init($this->wonderpay_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$charge_result = curl_exec($ch);
		curl_close($ch);
		$this->rawresult = $charge_result;
		$this->debug .= $charge_result;
		# Prepare the result of the transaction for analysis.  Put the keys and
		# values returned to us into a hash.
		$curl_result = preg_replace('/\<[^>]+\>/', '', $charge_result);  # Strip out HTML.

		$keys_and_values = preg_split("/\n/", $curl_result);
		foreach ($keys_and_values as $pair)
		{
			if (preg_match('/^\s*$/', $pair)) continue;	   # Skip blank lines.
			list($key, $value)   = preg_split('/=/', $pair);  # Separate key from value.
			$this->result_fields[$key] = $value;
		}
		
		$this->arr_log['ccresultField'] = $this->result_fields;
		
		if ($this->test){
			$this->approved = true;
			$this->declined = false;
			$this->duplicated = false;
			$this->error	= false;
			$this->order['result']	= 'Accepted';
			$this->order['reason']	= 'Test Mode';
			$this->order['message']	= "";
			$this->order['moremessage']	= "";
		}elseif (isset($this->result_fields['DUPLICATE'])){
			$this->approved = false;
			$this->declined = true;
			$this->duplicated = true;
			$this->error	= false;
			$this->order['result']	= 'Duplicated';
			$this->order['reason']	= $this->result_fields['Accepted'];
			$this->order['message']	= "Your transaction has been declined with a duplicate condition.";
			$this->order['moremessage']	= "Please wait for 30 - 60 minutes to use the same credit card with the same charge amount or use a different card.";
		}elseif (isset($this->result_fields['Accepted'])){
			$this->approved = true;
			$this->declined = false;
			$this->duplicated = false;
			$this->error	= false;
			$this->order['result']	= 'Accepted';
			$this->order['reason']	= $this->result_fields['Accepted'];
			$this->order['message']	= "";
			$this->order['moremessage']	= "";
		}elseif (isset($this->result_fields['Declined'])){
			$this->approved = false;
			$this->declined = true;
			$this->duplicated = false;
			$this->error		= false;
			$this->order['result']	= 'Declined';
			$this->order['reason']	= $this->result_fields['Declined'];
			$this->order['message']	= "Your credit card has been declined.";
			$this->order['moremessage']	= "Please make sure you filled the correct credit card information or try another card.";
		}elseif (isset($this->result_fields['Error'])){
			$this->approved = false;
			$this->declined = false;
			$this->duplicated = false;
			$this->error	= true;
			$this->order['result']	= 'Error';
			$this->order['reason']	= $this->result_fields['Error'];
			$this->order['message']	= "An error occurred during the transaction.";
			$this->order['moremessage']	= "Please make sure you filled the correct credit card information or try another card.";
		}else{
			$this->approved = false;
			$this->declined = false;
			$this->duplicated = false;
			$this->error		= true;
			$this->order['result']	= 'Unexpected';
			$this->order['reason']	= 'Unexpected value returned.';
			$this->order['message']	= "An unexpected error occurred during the transaction.";
			$this->order['moremessage']	= "Please make sure you filled the correct credit card information or try another card.";
		}
		$this->order['historyid'] 	= $this->result_fields['historyid'];
		$this->order['orderid'] 	= $this->result_fields['orderid'];
		$this->debug .= print_r($this->result_fields, true);
		$this->debug .= print_r($this->order, true);
	}

	public function isApproved() {
		return $this->approved;
	}

	public function isDeclined(){
		return $this->declined;
	}

	public function isDuplicated(){
		return $this->duplicated;
	}

	public function isError(){
		return $this->error;
	}

	public function getOrder(){
		return $this->order;
	}

	public function getResultFields(){
		return $this->result_fields;
	}

	public function getHistoryId(){
		return $this->result_fields['historyid'];
	}

	public function getOrderId(){
		return $this->result_fields['orderid'];
	}

	public function getDebug(){
		return $this->debug;
	}
	
	public function getRawResult(){
		return $this->rawresult;
	}

	public function testMode(){
		$this->test = 1;
	}

	public function getLog_Old(){
		$this->log = "\n--------------------------------------------------------------------------------\n";
		$this->log .= date('r') . "\n";
		$this->log .= "--------------------------------------------------------------------------------\n\n";
		$this->log .= "Transaction Results\n\n";
		foreach (array_keys($this->result_fields) as $key)
		{
			$value = preg_replace('/
			/', '', $this->result_fields[$key]);
			$this->log .= sprintf("%-20s = \"%s\"\n", '"' . $key . '"', $value);
		}
		$this->log .= "\nGLOBALS\n\n";
		foreach (array_keys($GLOBALS) as $key)
		{
			if ( ! is_scalar($GLOBALS[$key])) continue;
			$this->log .= sprintf("%-20s = \"%s\"\n", '"' . $key . '"', $GLOBALS[$key]);
		}
		return $this->log;
	}
	
	public function getLog(){
		return array_merge($this->arr_log, array(
		));
	}
}
?>