<?php

/**
* <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    include_once('/home/funeralnet/public_html/mfscommon/php/payment_driver/Wonderpay_driver.php');
*/
require '/home/funeralnet/public_html/mfscommon/php/merchant_classes/wonderpay/wonderpay.class.inc.php';
class Wonderpay_driver
{
	var $api		= null;
	var $approved		= false;
        var $declined		= false;
        var $duplicated		= false;
        var $error		= true;
        var $order		= array();
	
	function __construct(){
            $this->api = new wonderpay();
	}

	/**
	 * Process cc payment
	 * data array Credit card and user info
	 * return bool true is the transaction was successful, false if it was declined.
	 */
	function process($data){
            if($data['card_name'] != 'FuneralNet'){
                $charge_amount = sprintf("%.2f", $data['amount']);
                $this->api->process($data['auth_login'],$charge_amount,$data['card_name'],$data['ccnumber'],$data['expiration_of_month'],$data['expiration_of_year'],$data['cccode'],$data['auth_transkey']);
                $this->order = $this->api->getOrder();   
                if($this->api->isApproved()){
                    $this->approved = true;
                    $this->error = false;
                } else {
                    $this->approved = false;
                    $this->error = true;
                }
            } else {
                $this->approved = true;
                $this->error = false;
                $this->order['result'] = 'Accepted';
                $this->order['reason'] = 'Test Transaction';
            }     

            //return $this->api->isApproved();

	}

	/**
        * return bool true is the transaction was successful, false if it was declined.
        */
	function is_approved(){
            return $this->approved;
	}

        /**
        * return message from transection
        * @return array
        */
        function get_order(){
            return $this->order;
	}

}