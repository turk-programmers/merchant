<?php

/**
* <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    include_once('/home/funeralnet/public_html/mfscommon/php/payment_driver/Authorize_driver.php');
*/
require '/home/funeralnet/public_html/mfscommon/php/merchant_classes/authorized.net/authorized_for_ci.class.inc.php';
class Authorize_driver {
    var $api		= null;
    var $approved		= false;
    var $declined		= false;
    var $duplicated		= false;
    var $error		= true;
    var $order		= array();
    
    function __construct(){
        $this->api = new authorize();
    }
        
    public function process($data){
        # Initial config
        $this->api->init(array(
            'auth_login' => $data['auth_login'],
            'auth_transkey' => $data['auth_transkey']
        ));
        
        $returnResult= array();
        //set new transaction
        $this->api->setTransaction($data['ccnumber'], $data['expiration_of_month'].$data['expiration_of_year'], $data['amount'], $data['cccode']);
        //Billing information
        $this->api->setParameter("x_email", $data['email']);
        $this->api->setParameter("x_first_name", $data['card_name']);
        $this->api->setParameter("x_last_name", $data['last_name']);
        $this->api->setParameter("x_address", $data['address']);
        $this->api->setParameter("x_city", $data['city']);
        $this->api->setParameter("x_state", $data['state']);
        $this->api->setParameter("x_zip", $data['zip']);
        $this->api->setParameter("x_country", $data['country']);
        $this->api->setParameter("x_description", $data['description']);

        //Process checkout information
        $this->api->process();
        
        $this->order = $this->api->getOrder();
        
        if(preg_match("/Accepted/i",$this->order['result'])){
            
            $this->approved = true;
            $this->error = false;

        } else {
            // if($data['card_name'] == 'FuneralNet'){
            if(is_debugger(@$data['email'])){

                $this->order['result'] = 'Accepted';
                $this->order['reason'] = 'Test Transaction';

                $this->approved = true;
                $this->error = false;

            } else {

                $this->approved = false;
                $this->error = true;

            }
        }
    }
    
    /**
    * return bool true is the transaction was successful, false if it was declined.
    */
    public function is_approved(){
        return $this->approved;
    }
    
    /**
     * return message from transection
     * @return array
     */
    public function get_order(){
        return $this->order;
    }
}