<?php

/**
* <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    include_once('/home/funeralnet/public_html/mfscommon/php/payment_driver/Authorize_driver.php');
*/
require '/home/funeralnet/public_html/mfscommon/php/merchant_classes/firstdata/Firstdata.php';
class Firstdata_driver {
    
    var $order		= array();
    var $approved		= false;
    var $error		= true;
    
    function __construct(){
        $this->api = new firstdata();
    }
    
    public function process($data){
        $this->api->process($data);
        $this->order = $this->api->getOrder();
        if($this->api->isApproved()){
            $this->approved = true;
            $this->error = false;
        } else {
            // if($data['card_name'] == 'FuneralNet'){
            if(is_debugger(@$data['email'])){
                $this->approved = true;
                $this->error = false;
                $this->order['result'] = 'Accepted';
                $this->order['reason'] = 'Test Transaction';
            } else {
                $this->approved = false;
                $this->error = true;
            }               
        }

    }
    
    /**
    * return bool true is the transaction was successful, false if it was declined.
    */
    public function is_approved(){
        return $this->approved;
    }
    
    /**
     * return message from transection
     * @return array
     */
    public function get_order(){
        return $this->order;
    }
}
