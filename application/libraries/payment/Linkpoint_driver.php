<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Linkpoint_driver
 *
 * @author UseR
 */
class Linkpoint_driver {
    var $api		= null;
    var $approved		= false;
    var $declined		= false;
    var $duplicated		= false;
    var $error		= true;
    var $order    = array();
    var $fullResult		= array();

    /*function __construct(){
        $this->api = new wonderpay();
    }*/

    var $linkpoint = array
    (
      'host'          => 'secure.linkpt.net',
      'port'          => 1129,
      'order_options' => array
      (
        //'result'      => 'GOOD',      # When testing, forces acceptance.
        //'result'      => 'DECLINE',   # When testing, forces failure.
        'result'      => 'LIVE',      # Use this for normal operation.
        'ordertype'   => 'SALE',
      ),
    );
    
    function process($data){
        $xml ="<order>
          <orderoptions>
            <ordertype>~ordertype~</ordertype>
            <result>~result~</result>
          </orderoptions>
          <creditcard>
            <cardnumber>~card_number~</cardnumber>
            <cardexpmonth>~card_expmonth~</cardexpmonth>
            <cardexpyear>~card_expyear~</cardexpyear>
          </creditcard>
          <billing>
          </billing>
          <shipping>
          </shipping>
          <transactiondetails>
          </transactiondetails>
          <merchantinfo>
            <configfile>~store_number~</configfile>
          </merchantinfo>
          <payment>
            <chargetotal>~amount~</chargetotal>
          </payment>
        </order>";
        $angle_brackets = array
            (
              '/\</',
              '/\>/',
            );

        $entities = array
            (
              '&lt;',
              '&gt;',
            );
        
        # Strip white space, including linefeeds, that precedes "<" or follows ">" in the XML data.
	
        $xml = preg_replace(array('/\s+\</', '/\>\s+/'), array('<', '>'), $xml);
        $field_placeholders = array
            (
              '/~ordertype~/',
              '/~result~/',
              '/~card_number~/',
              '/~card_expmonth~/',
              '/~card_expyear~/',
              '/~store_number~/',
              '/~amount~/',
            );
        $charge_amount = sprintf("%.2f", $data['amount']);
        
        $order_info = array(
            'card_number' => $data['ccnumber'],
            'card_expmonth' => $data['expiration_of_month'],
            'card_expyear' => $data['expiration_of_year'],
            'charge_amount' => $charge_amount,
        );
        
        if(isset($data['result'])){
            $this->linkpoint['order_options']['result'] = $data['result'];
        }
        
        $field_data = array
            (
              $this->linkpoint['order_options']['ordertype'],
              $this->linkpoint['order_options']['result'],
              $order_info['card_number'],
              $order_info['card_expmonth'],
              $order_info['card_expyear'],
              $data['auth_login'],
              $order_info['charge_amount'],
            );
        
        # Substitute the field data for the field placeholders in the XML data.
	$xml = preg_replace($field_placeholders, $field_data, $xml);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,            'https://' . $this->linkpoint['host'] . ':' . $this->linkpoint['port'] . '/LSGSXML');
        curl_setopt($ch, CURLOPT_POST,           1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $xml); # the string we built above
        curl_setopt($ch, CURLOPT_TIMEOUT,        120);
        curl_setopt($ch, CURLOPT_SSLCERT,        $data['cert']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER,         0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE,        0); # Optional - verbose debug output, not for production use.
        $charge_result = curl_exec($ch);
        curl_close($ch);
        
        //$this->order = $charge_result;
        if (strlen($charge_result) < 2) {
            # There was no response.
            $result_hash['r_error'] = 'Could not execute curl.'.$charge_result;
        } else {
            # We received a response.  Convert the XML response into an array of keys
            # and values.

            preg_match_all ("/<(.*?)>(.*?)\</", $charge_result, $result, PREG_SET_ORDER);
            $n = 0;
            while (isset($result[$n]))
            {
              $result_hash[$result[$n][1]] = strip_tags($result[$n][0]);
              $n++;
            }
            $this->fullResult = $result_hash;
        }
        $return_hash = array
        (
          'approved' => 0,
          'errmsg'   => $result_hash['r_error'],
        );
        $return_hash['authcode']   = $result_hash['r_code']     ? $result_hash['r_code']     : '';
        $return_hash['authrefnum'] = $result_hash['r_ref']      ? $result_hash['r_ref']      : '';
        $return_hash['avscode']    = $result_hash['r_avs']      ? $result_hash['r_avs']      : '';
        $return_hash['orderid']    = $result_hash['r_ordernum'] ? $result_hash['r_ordernum'] : '';
        $return_hash['respmsg']    = $result_hash['r_message']  ? $result_hash['r_message']  : '';
        if ((! empty($result_hash['r_approved'])) and $result_hash['r_approved']=='APPROVED' )
          $return_hash['approved'] = 1;
        
        if ($return_hash['approved']) {
            $this->approved = true;           
            $this->order['message'] = $return_hash['respmsg'];
        } else {
            $this->declined = true;
            $this->order['moremessage'] = $return_hash['errmsg'];
            $this->order['message'] = $return_hash['respmsg'];
            $this->order['reason'] = 'Order number :'.$result_hash['r_ordernum'];
        }
    }
    
    /**
    * return bool true is the transaction was successful, false if it was declined.
    */
    function is_approved(){
        return $this->approved;
    }
    
    function get_order(){
        return $this->order;
    }
    function get_FullResult(){
        return $this->fullResult;
    }
}
