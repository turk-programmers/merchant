<?php

require '/home/funeralnet/public_html/mfscommon/php/merchant_classes/payeezy/Payeezy.php';

class Payeezy_driver{
    var $api = null;
    var $approved		= false;
    var $declined		= false;
    var $duplicated		= false;
    var $error		= true;
    var $order		= array();

    function __construct(){
            $this->api = new Payeezy();		
    }

    public function process($data){
        $this->api->setApiKey($data['apiKey']);
        $this->api->setApiSecret($data['apiSecret']);

        $this->api->setMerchantToken($data['merchantToken']);

        $this->api->setTokenUrl("https://api-cert.payeezy.com/v1/transactions/tokens");  
        $this->api->setUrl("https://api-cert.payeezy.com/v1/transactions");

        $primaryTxPayload = array(
            "amount"=> $this->makeAmountFormat($this->processInput($data['amount'])),
            "card_number" => $this->processInput($data['ccnumber']),
            "card_type" => $this->cardType($this->processInput($data['ccnumber'])),
            "card_holder_name" => $this->processInput($data['card_name']),
            "card_cvv" => $this->processInput($data['cccode']),
            "card_expiry" => $this->processInput($data['expiration_of_month']).$this->processInput($data['expiration_of_year']),
            //"merchant_ref" => $this->processInput($data['merchant_ref']),
            "currency_code" => 'USD',
        );
        
        
        $authresponse = $this->api->purchase($primaryTxPayload);
        
        $this->order = json_decode($authresponse,true);
     
        if($this->order['transaction_status'] === 'approved'){
                $this->order['reason'] = 'Success :' . $this->order['bank_resp_code'];
                $this->order['message'] = 'transaction_id :'.$this->order['transaction_id']. ' | transaction_tag :'.$this->order['transaction_tag'];
                $this->order['moremessage'] = 'Approved :'.$this->order['gateway_message'];
                $this->approved = true;
        } else {
                $this->order['reason'] = $this->order['validation_status'].$this->order['code'];
                $this->order['message'] = $this->order['Error']['messages'][0]['code'];
                $this->order['moremessage'] = $this->order['Error']['messages'][0]['description'];
                $this->declined = true;
        }
    }

	/**
    * return bool true is the transaction was successful, false if it was declined.
    */
    public function is_approved(){
        return $this->approved;
    }
    
    /**
     * return message from transection
     * @return array
     */
    public function get_order(){
        return $this->order;
    }

    public function get_FullResult(){
        return $this->api->getFullResult();
    }

    private function makeAmountFormat($amount){
    	return str_replace('.','',number_format($amount,2));
    }

    private function processInput($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return strval($data);
	}


	private function cardType($number){
	    $number=preg_replace('/[^\d]/','',$number);
	    if (preg_match('/^3[47][0-9]{13}$/',$number))
	    {
	        return 'American Express';
	    }
	    elseif (preg_match('/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/',$number))
	    {
	        return 'Diners Club';
	    }
	    elseif (preg_match('/^6(?:011|5[0-9][0-9])[0-9]{12}$/',$number))
	    {
	        return 'Discover';
	    }
	    elseif (preg_match('/^(?:2131|1800|35\d{3})\d{11}$/',$number))
	    {
	        return 'JCB';
	    }
	    elseif (preg_match('/^5[1-5][0-9]{14}$/',$number))
	    {
	        return 'MasterCard';
	    }
	    elseif (preg_match('/^4[0-9]{12}(?:[0-9]{3})?$/',$number))
	    {
	        return 'Visa';
	    }
	    else
	    {
	        return 'Unknown';
	    }
	}
}
?>