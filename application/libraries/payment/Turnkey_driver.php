<?php

/**
* <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    include_once('/home/funeralnet/public_html/mfscommon/php/payment_driver/Authorize_driver.php');
*/
require '/home/funeralnet/public_html/mfscommon/php/merchant_classes/firstdata/Firstdata.php';
class Turnkey_driver {
    
    var $order		= array();
    var $approved		= false;
    var $error		= true;
    
    function __construct(){
        $this->api = new firstdata();
    }
    
    public function process($data){
        if($data['card_name'] != 'FuneralNet'){
            $this->api->process($data);
            $this->order = $this->api->getOrder();
            if($this->api->isApproved()){
                $this->approved = true;
                $this->error = false;
            } else {
                $this->approved = false;
                $this->error = true;
            }
        } else {
            $this->approved = true;
            $this->error = false;
            $this->order['result'] = 'Accepted';
            $this->order['reason'] = 'Test Transaction';
        }                
    }
    
    /**
    * return bool true is the transaction was successful, false if it was declined.
    */
    public function is_approved(){
        return $this->approved;
    }
    
    /**
     * return message from transection
     * @return array
     */
    public function get_order(){
        $this->order['reason'] = $this->order['result'];
        return $this->order;
    }
}
