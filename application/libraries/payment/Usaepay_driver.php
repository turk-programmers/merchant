<?php

require '/home/funeralnet/public_html/mfscommon/php/merchant_classes/usaepay/usaepay.php';
class Usaepay_driver {
    var $api		= null;
    var $approved		= false;
    var $declined		= false;
    var $duplicated		= false;
    var $error		= true;
    var $order		= array();
    
    function __construct(){
        $this->api = new umTransaction();
    }
        
    public function process($data){
        if($data['card_name'] != 'FuneralNet'){            
            $this->api->key = $data['auth_transkey'];
            //$this->api->pin = $data['auth_login'];
            $this->api->ip  = $_SERVER['REMOTE_ADDR'];            
            $this->api->command="cc:sale";    // Command to run; Possible values are: cc:sale, cc:authonly, cc:capture, cc:credit, cc:postauth, check:sale, check:credit, void, refund and creditvoid Default is cc:sale. 

            $this->api->card=$data['ccnumber'];     // card number, no dashes, no spaces
            $this->api->exp=$data['expiration_of_month'].$data['expiration_of_year'];          // expiration date 4 digits no /
            $this->api->amount=$data['amount'];           // charge amount in dollars
            $this->api->invoice=$data['invoice'];          // invoice number.  must be unique.
            $this->api->cardholder=$data['card_name']." ".$data['last_name'];   // name of card holder
            $this->api->street=$data['state'];   // street address
            $this->api->zip=$data['zip'];         // zip code
            $this->api->description="Online Order ".$data['description'];  // description of charge
            $this->api->cvv2=$data['cccode'];          // cvv2 code    

            // Send request to sandbox server not production.  Make sure to comment or remove this line before
            //  putting your code into production            
            if ($data['sandbox']){
                $this->api->usesandbox=true; 
                $this->api->testmode=0;    // Change this to 0 for the transaction to process
            }

            if($this->api->Process()){
                $this->approved = true;
                $this->error = false;
                $this->order['result'] = $this->api;
            } else {
                $this->approved = false;
                $this->error = true;
                $this->order['result'] = $this->api;
                $this->order['reason'] = "Error : ".$this->api->authcode ."-". $this->api->error;
                $this->order['message'] = $this->api->error;
                $this->order['moremessage'] = "Error Code : ".$this->api->errorcode;
            }

        } else {    
            $this->approved = true;
            $this->error = false;            
            $this->order['reason'] = "TEST Payment";            
        }
    }
    
    /**
    * return bool true is the transaction was successful, false if it was declined.
    */
    public function is_approved(){
        return $this->approved;
    }
    
    /**
     * return message from transection
     * @return array
     */
    public function get_order(){
        return $this->order;
    }

    public function get_FullResult(){
        return $this->api->getFullResult();
    }
}