<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bluefin_driver
 *
 * @author UseR
 */
require '/home/funeralnet/public_html/mfscommon/php/merchant_classes/bluefin/bluefin.class.inc.php';
class Bluefin_driver {
    var $api		= null;
    var $approved		= false;
    var $declined		= false;
    var $duplicated		= false;
    var $error		= true;
    var $order		= array();
    
    function __construct(){
        $this->api = new bluefin();
    }
    
    function process($data){
        $this->api->setParams(array(
            'account_id'            => $data['auth_login'],
            'api_accesskey'         => $data['auth_transkey'],
            'transaction_amount'        => $data['amount'],
            'card_number'           => $data['ccnumber'],
            'card_expiration'       => $data['expiration_of_month'].$data['expiration_of_year'],
            'first_name'            => $data['card_name'],
            'last_name'             => $data['last_name'],
            'transaction_description'       => $data['description'],
        ));

        $ccapproved = $this->api->process();
        $this->order = $this->api->getResponse();

        if($ccapproved){
            $this->approved = true;
            $this->error = false;
        } else {
            // if($data['card_name'] == 'FuneralNet'){
            if(is_debugger(@$data['email'])){
                $this->approved = false;
                $this->error = true;
                $this->order['result'] = 'Accepted';
                $this->order['reason'] = 'Test Transaction';
            } else {
                $this->approved = false;
                $this->error = true;
                $this->order['reason'] = $this->order['error_code'];
                $this->order['message'] = $this->order['error_message'];
                $this->order['moremessage'] = $this->order['error_msg'];
            }
        }
    }
    
    /**
    * return bool true is the transaction was successful, false if it was declined.
    */
    public function is_approved(){
        return $this->approved;
    }
    
    /**
     * return message from transection
     * @return array
     */
    public function get_order(){
        return $this->order;
    }
}
