<?php 
require('/home/funeralnet/domains/funeralnet.com/public_html/mfscommon/php/merchant_classes/transectionexpress/transex.php');

class Transection_driver{
    var $api = null;
    var $approved       = false;
    var $declined       = false;
    var $duplicated     = false;
    var $error          = true;
    var $order          = array();

    function __construct(){
        $this->api = new Transex();    
    }

    public function process($data){
 
        //Set Parameter
        $year = $data['expiration_of_year'];
        if(strlen($year)>=4){
            $year = substr($year,2);
        } 
        $cardnum = preg_replace ( "/[^0-9,.]/", "", $data['ccnumber'] );
        $expdate =  $year . $data['expiration_of_month'];

        $this->api->setParameter('GatewayID',$data['auth_login']);
        $this->api->setParameter('RegKey',$data['auth_transkey']);
        $this->api->setParameter('IndustryCode','2');
        $this->api->setParameter('AccountNumber',$cardnum);
        $this->api->setParameter('CVV2',$data['cccode']);
        $this->api->setParameter('ExpirationDate',$expdate);
        $this->api->setParameter('Track1Data','');
        $this->api->setParameter('Track2Data','');
        $this->api->setParameter('Amount',str_replace('.','',number_format($data['amount'],2)));
        $this->api->setParameter('TaxIndicator','');
        $this->api->setParameter('TaxAmount','');
        $this->api->setParameter('PONumber','');
        $this->api->setParameter('ShipToZip','');
        $this->api->setParameter('CustRefID','');
        $this->api->setParameter('FullName=',$data['card_name']);
        $this->api->setParameter('Address1=',$data['address']);
        $this->api->setParameter('Address2','');
        $this->api->setParameter('City',$data['city']);
        $this->api->setParameter('State',$data['state']);
        $this->api->setParameter('Zip',$data['zip']);
        $this->api->setParameter('PhoneNumber','');
        $this->api->setParameter('Email',$data['email']);
        $this->api->setParameter('Descriptor',$data['description']);

        $results = $this->api->process();
        return $results;
    }

    public function debug(){
        $this->api->debugParameter();
    }

    public function getParam(){
        return $this->api->parameters;
    }
    public function is_approved(){
        return $this->api->isApproved();
    }

    public function get_order(){
        return $this->api->getOrder();
    } 
}

?>
