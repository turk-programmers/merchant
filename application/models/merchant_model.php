<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Merchant_model extends CI_Model {

    private $client_id;
    private $shop_id;
    private $config_info;
    private $db_merchant;

    public function init($client_id, $shop_id) {
        $this->db_merchant = $this->load->database('default', TRUE);
        $this->client_id = $client_id;
        $this->shop_id = $shop_id;

        $this->config_info = $this->db_merchant->where('client_id', $this->client_id)
                ->get('clients')
                ->result_array();
    }

    // 
    // For debugging only
    // 
    public function orderList_debug() {
        $sql = <<<sql
SELECT
    *
FROM
    orders
WHERE
    shop_id = 57
ORDER BY
    id DESC

sql;
        $res = $this->db_merchant->query($sql)->result_array();
        $updates = array();
        foreach($res as $i=>$rec) {
            $rec['session'] = unserialize($rec['session']);
            if(@$rec['session']['delivery']['deliver_decedent']) {
                $updates[$i]['id'] = $rec['id'];
                $updates[$i]['decedent'] = @$rec['session']['delivery']['deliver_decedent'];
            }
            $res[$i] = $rec;
        }
        devprint($res);
        // devprint($updates);
        // $this->db_merchant->update_batch('orders', $updates, 'id');
    }

    /**
     *
     * @Return the config from database in table settings with each client
     */
    public function get_config() {
        $settings = array();
        $result = $this->db_merchant->select('var_name, var_value, var_type')
                        ->from('settings')
                        ->where('shop_id', $this->shop_id)
                        ->get()->result_array();
        foreach ($result as $row) {
            if ($row['var_type'] == 'ARRAY') {
                eval('$settings[$row["var_name"]] = ' . $row["var_value"] . ';');
            } else {
                $settings[$row['var_name']] = $row['var_value'];
            }

            if ($row['var_type'] == 'BIGSTRING') {
                $settings[$row['var_name']] = @unserialize($row['var_value']);
            }
        }
        return $settings;
    }

    public function get_shop_off_records() {
        $shop_id = $this->shop_id;
        $sql = <<<sql
SELECT
    *
FROM
    shop_off_records
WHERE
    shop_id = $shop_id AND
        (
                (end AND now() <= end) OR
                (start AND end is NULL) OR
                0
        ) AND
    active AND
    1
sql;
        $res = $this->db_merchant->query($sql)->result_array();
        $recs = array();
        foreach ($res as $rec) {
            $recs[$rec['id']] = $rec;
        }
        return $recs;
    }

    /**
     *
     * @param type $locationid
     * @return location of client if parameter hasn't location_id this function will return all locations for this client
     */
    public function get_location($locationid = null) {
        if ($locationid == '') {
            $locations = array();
            $result = $this->db_merchant->select('*')
                            ->from('locations')
                            ->where('client_id', $this->client_id)
                            ->where('status', 1)
                            ->where("(year(deleted) = '0000' or deleted is null)")
                            ->get()->result_array();
            foreach ($result as $row) {
                $locations[$row['name_id']] = $row;
            }
        } else {
            $locations = array();
            $result = $this->db_merchant->select('*')
                            ->from('locations')
                            ->where('client_id', $this->client_id)
                            ->where('id', $locationid)
                            ->where('status', 1)
                            ->get()->result_array();
            $locations = $result[0];
        }
        return $locations;
    }

    public function get_provider_shop() {
        $locations = array();
        $result = $this->db_merchant->select('*')
                        ->from('shop')
                        ->where('client_id', $this->client_id)
                        ->where('id', $this->shop_id)
                        ->get()->result_array();
        $locations = $result[0];
        return $locations;
    }

    public function updateLastProvider_shop($providerid) {
        $data = array(
            'last_provider_id' => $providerid
        );
        $this->db_merchant
                ->where('id', $this->shop_id)
                ->update('shop', $data);
    }

    public function updateLastProvider($locationid, $providerid) {
        $data = array(
            'last_provider_id' => $providerid
        );
        $this->db_merchant
                ->where('id', $locationid)
                ->update('locations', $data);
    }

    public function get_provider($providerid = null) {
        $provider = array();
        $result = $this->db_merchant->select('*')
                        ->from('providers')
                        ->where('client_id', $this->client_id)
                        ->where('id', $providerid)
                        ->get();
        if($result->num_rows() > 0){
            $result = $result->result_array();
            $provider = $result[0];
            return $provider;
        } else {
            return false;
        }

    }

    public function get_all_shop() {
        $shops = array();
        $result = $this->db_merchant->select('name,label,email_from,email_to,default_tax_rate,payment_gateway,payment_account,payment_key')
                        ->from('shop')
                        ->where('client_id', $this->client_id)
                        ->where('deleted = 0 or deleted is null', null, false)
                        ->get()->result_array();
        foreach ($result as $row) {
            $shops[$row['name']] = $row;
        }
        return $shops;
    }

    public function get_shops() {
        $shop = array();
        $result = $this->db_merchant->select()
                        ->from('shop')
                        ->where('client_id', $this->client_id)
                        ->where('id', $this->shop_id)
                        ->get()->result_array();
        foreach ($result as $row) {
            $shop = $row;
        }
        return $shop;
    }

    public function get_default_tax_rate($category) {
        $category_id = $this->get_main_category_id();

        $query = $this->db_merchant->select('default_tax_rate')
                        ->where('id', $category_id)
                        ->where('client_id', $this->client_id)
                        ->get('shop')->row_array();
        if (count($query) <= 0) {
            return 0;
        } else {
            return $query['default_tax_rate'];
        }
    }

    /**
     * get categories name when the database have some products
     * @param type $category
     * @return type
     */
    public function get_categories($category) {
        $result = array();
        $query = $this->db_merchant
                    ->select('c.label')
                    ->select('c.name')
                    ->select('count(p.pid) as num_products')
                    ->from('categories c')
                    ->from('products p')
                    ->where('c.id = p.category_id', NULL, FALSE)
                    ->where('c.shop_id', $this->shop_id)
                    ->where('c.status', 1)
                    ->where('c.parent_id', 0)//Parent
                    ->where('(p.deleted is null or now()< p.deleted)', null, false)
                    ->group_by("p.category_id")
                    ->order_by('order')
                    ->get();
        $c = 0;
        foreach ($query->result_array() as $row) {
            $result[$c] = array(
                'label' => $row['label'],
                'name' => $row['name'],
                'section' => $row['name'],
                'link' => '/' . $category . '/products/' . urlencode($row['label']) . '/#' . $row['name'] . '-view',
                'num_products' => $row['num_products'],
            );
            $c++;
        }
        return $result;
    }

    public function get_subcategories_lv2($category) {
        $result = array();
        $query = $this->db_merchant->from('categories c')
                ->from('products p')
                ->where('c.id = p.category_id', NULL, FALSE)
                ->where('c.shop_id', $this->shop_id)
                ->where('c.status', 1)
                ->where('c.parent_id !=', 0)
                ->where('(p.deleted is null or now()< p.deleted)', null, false)
                ->select('c.label')
                ->select('c.name')
                ->select('c.parent_id')
                ->select('count(p.pid) as num_products')
                ->group_by("p.category_id")
                ->get();
        $c = 0;
        foreach ($query->result_array() as $row) {
            $queryParent = $this->db_merchant->from('categories')->where('id', $row['parent_id'])
                            ->select('name')->get();
            $parent = $queryParent->row();
            $result[$c] = array(
                'label' => $row['label'],
                'name' => $row['name'],
                'section' => $parent->name,
                'num_products' => $row['num_products'],
            );
            $c++;
        }

        return $result;
    }

    public function get_products($category, $sub_category = NULL) {

        $result['subcategory_list'] = $this->get_categories($category);
        $result['subcategory_list_lv2'] = $this->get_subcategories_lv2($category);
        $result['product_list'] = $this->fetch_products($category, $sub_category);

        return $result;
    }

    public function get_product_by_id($product_id, $field = "") {

        # CHECK VALIDATE REQUIREMENT
        if ($product_id == 0)
            return false;
        if (!is_numeric($product_id))
            return false;

        $query = $this->db_merchant->from('categories c')
                ->from('products p')
                ->where('p.pid', $product_id)
                ->where('c.id = p.category_id', NULL, FALSE)
                ->where('c.status', 1)
                ->select('c.name AS category_name')
                ->select('c.label AS category_label')
                ->select('p.price AS price')
                ->select('p.price2 AS price2')
                ->select('p.price3 AS price3')
                ->select('p.advance_price AS advance_price')
                ->select('p.shipping AS shipping')
                ->select('p.name AS product_name')
                ->select('p.second_name AS second_name')
                ->select('p.shortdesc AS shortdesc')
                ->select('p.longdesc AS longdesc')
                ->select('p.additional_options AS additional_options')
                ->select('p.sorder AS sorder')
                ->select('p.images AS images')
                ->select('p.thumb AS thumb')
                ->select('p.teleflora_sku AS teleflora_sku')

                //->limit(1)
                ->get();


        foreach ($query->result_array() as $row) {
            $modified_additional_options = array();
            if(@$row['additional_options']) {
                $additional_options = json_decode($row['additional_options'], true);
                foreach(($additional_options) as $option) {
                    // $option['choices'] = explode(',', $option['choices']);
                    $option['choices'] = split_choices($option['choices']);
                    $modified_additional_options[$option['key']] = $option;
                }
            }
            $advance_price = json_decode($row['advance_price'], true);
            if(json_last_error() == JSON_ERROR_NONE) {
                $row['advance_price'] = $advance_price;
            }
            $result = array(
                'price' => $row['price'],
                'price2' => $row['price2'],
                'price3' => $row['price3'],
                'advance_price' => $row['advance_price'],
                'product_name' => $row['product_name'],
                'second_name' => $row['second_name'] ,
                'category_label' => $row['category_label'] ,
                'category_name' => $row['category_name'] ,
                'shortdesc' => $row['shortdesc'],
                'additional_options' => $modified_additional_options,
                'image' => $row['images'],
                'thumb' => $row['thumb'],
                'teleflora_sku' => $row['teleflora_sku'],
            );
        }



        # RETURN RESULT
        if (is_array($result)) {




            if ($field == "")
                return $result;
            else
                return isset($result[$field]) ? $result[$field] : false;
        }
        else {
            return false;
        }
    }

    ####################################################################################################
    #											FUNCTIONS											   #
    ####################################################################################################

    private function fetch_products($category = NULL, $sub_category = NULL) {

        if ($category == NULL)
            return false;

        $result = false;
        $category_id = $this->get_main_category_id();

        $query = $this->db_merchant->from('categories c')
                ->from('products p')

                # Table: Category
                ->select('c.name AS category_name')
                ->select('c.label AS label')
                ->select('c.order AS catorder')

                # Table: Products
                ->select('p.pid AS product_id')
                ->select('p.price AS price')
                ->select('p.price2 AS price2')
                ->select('p.price3 AS price3')
                ->select('p.advance_price AS advance_price')
                ->select('p.shipping AS shipping')
                ->select('p.name AS product_name')
                ->select('p.shortdesc AS shortdesc')
                ->select('p.longdesc AS longdesc')
                ->select('p.additional_options AS additional_options')
                ->select('p.images AS image')
                ->select('p.thumb AS thumb')
                ->select('c.parent_id')
                ->where('(p.deleted is null or now()< p.deleted)', null, false)
                ->where('c.id = p.category_id', NULL, FALSE)
                ->where('c.shop_id', $this->shop_id)
                ->where('c.status', 1)
                ->order_by('p.sorder', 'asc')
                ->order_by('p.price', 'asc')
                ->order_by('p.name', 'asc');
        $query = $this->db_merchant->get();

        $c = 0;
        foreach ($query->result_array() as $row) {
            $parent = $row['parent_id'] == '0' ? $row['category_name'] : $this->getParent($row['parent_id']);
            $additional_options = array();
            if(@$row['additional_options']) {
                $additional_options = json_decode($row['additional_options'], true);
                foreach(($additional_options) as $i=>$option) {
                    // $choices = explode(',', $option['choices']);
                    $choices = split_choices($option['choices']);
                    $additional_options[$i]['choices'] = $choices;
                }
            }
            $advance_price = json_decode($row['advance_price'], true);
            if(json_last_error() == JSON_ERROR_NONE) {
                $row['advance_price'] = $advance_price;
            }

            $result[$c] = array(
                'category_name' => $row['category_name'],
                'category_label' => $row['label'],
                'category_order' => $row['catorder'],
                'parent' => $parent,
                'product_id' => $row['product_id'],
                'price' => $row['price'],
                'price2' => $row['price2'],
                'price3' => $row['price3'],
                'advance_price' => $row['advance_price'],
                'shipping' => $row['shipping'],
                'product_name' => $row['product_name'],
                'shortdesc' => $row['shortdesc'],
                'additional_options' => $additional_options,
                'longdesc' => $row['longdesc'],
                'image' => $row['image'],
                'thumb' => $row['thumb'],
            );
            $c++;
        }
        return $result = $result ? $result : 'No Products';
    }

    private function getParent($id) {
        $query = $this->db_merchant->from('categories')
                        ->where('id', $id)->get();
        $data = $query->result_array();
        return @$data[0]['name'];
    }

    private function get_main_category_id() {

        return $this->shop_id;
    }

    /**
     * -----------------------------------------------------------------------------------
     * Archive Function
     * -----------------------------------------------------------------------------------
     */
    function get_order_by_id($id) {
        $items = array();
        /* $apccfg = array(
          'key' => $this->catchprefix.'-order-'.$id,
          //'ttl' => (60*60*3),
          ); */
        /* if(apc_exists($apccfg['key'])){
          $items = $this->get_catch($apccfg['key']);
          }else{ */
        $query = $this->db_merchant
                ->where('id', $id)
                ->limit(1)
                ->get('orders');
        $items = $query->result_array();
        //$this->set_catch($apccfg['key'], $items);
        //}
        return @$items[0];
    }

    function get_order_by_hash($hash) {
        $items = array();
        $query = $this->db_merchant
                ->where('hash', $hash)
                ->limit(1)
                ->get('orders');
        $items = $query->result_array();
        //$this->set_catch($apccfg['key'], $items);
        //}
        return isset($items[0]) ? $items[0] : array() ;
    }

    function add_order_archive($data) {
        $this->db_merchant->insert('orders', $data);
        $id = $this->db_merchant->insert_id();
        return $id;
    }

    function update_order_archive($data, $id) {
        $this->db_merchant
                ->where('id', $id)
                ->update('orders', $data);
    }

    function get_last_uniq_id() {
        $query = $this->db_merchant
                ->where('uniq like "%' . date('y') . '____"')
                ->where('shop_id', $this->shop_id)
                ->order_by('id', 'desc')
                ->limit(1)
                ->get('orders');
        $item = $query->result_array();
        // if(is_dev()) {
        //     echo $this->db_merchant->last_query();
        //     echo "<br>";
        // }
        if (@$item[0]['uniq']) {
            return $item[0]['uniq'];
        } else {
            return false;
        }
    }

    public function getNewUniq(){

        $query = $this->db_merchant
                    ->where('shop_id', $this->shop_id)
                    ->order_by('id', 'desc')
                    ->get('orders');
        if ($query->num_rows() > 0){
            $row = $query->row_array();
            return (int)$row['uniq']+1;
        } return false;

    }


    function save_token_paypal( $uniq, $token, $status) {
        $query = $this->db_merchant
                ->where('uniq',$uniq)
                ->where('shop_id', $this->shop_id)
                ->where('client_id',$this->client_id)
                ->where('status','0')->get('token_paypal');
        if($query->num_rows() > 0){
            //update token
            $data = $query->result_array();
            $update = array(
                'token' => $token,
                'status' => $status
            );

            $this->db_merchant->where('id', $data[0]['id']);
            $this->db_merchant->update('token_paypal', $update);
        } else {
            $data = array(
                'token'=>$token,
                'uniq'=>$uniq,
                'status'=>'0',
                'client_id'=>$this->client_id,
                'shop_id'=>$this->shop_id,
            );
            $this->db_merchant->insert('token_paypal', $data);
        }
    }

    function checktoken($token){
        $query = $this->db_merchant
                ->where('token',$token)
                ->where('status','0')->get('token_paypal');
        $rt = array();
        if($query->num_rows() > 0){
            $rt['status'] = true;
            $data = $query->result_array();
            $rt['uniq'] = $data[0]['uniq'];
        }
        return $rt;
    }

    function get_order_by_uniq($uniq){
        $query = $this->db_merchant
                ->where('shop_id', $this->shop_id)
                ->where('uniq',$uniq)->get('orders')->result_array();
        return $query[0]['session'];
    }

}
