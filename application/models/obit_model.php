<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of obit_model
 *
 * @author UseR
 */
class obit_model extends CI_Model {

    private $client_id;
    private $db_obit;

    public function init($client_id) {
        $this->db_obit = $this->load->database('obit', TRUE);
        $this->client_id = $client_id;
    }
    
    public function ObitDecedent($obitid){
        $data = $this->db_obit->select('*')
                //->where('clientid',$this->client_id)
                ->where('id',$obitid)
                ->get('main')->result_array();
        
        return $data[0];
    }
    
    public function ObitFuneralHome($obitid) {               
        $data = $this->db_obit->from('locations')
                ->from('defaults')
                ->where('defaults.defid = locations.defid', NULL, FALSE)
                ->where('locations.id',$obitid)
                ->where("locations.type = 'Funeral Home'", NULL, FALSE)
                ->select('defaults.*')
                ->get()->result_array();
        return $data[0];
    }
    
    public function ObitService($obitid) {               
        $data = $this->db_obit->from('locations')
                ->from('defaults')
                ->where('defaults.defid = locations.defid', NULL, FALSE)
                ->where('locations.id',$obitid)
                ->where("locations.type = 'Service'", NULL, FALSE)
                ->select('defaults.*')
                ->get()->result_array();
        return $data[0];
    }
    
    public function ObitServiceDate($obitid) {            
        $data = $this->db_obit->select('sdate')->where('id',$obitid)
                ->where('sdate  > now()',NULL,FALSE)->get('main')->result_array();   
        if(count($data)>0){
            return $data[0];
        } else {
            return false;
        }        
    }
}
