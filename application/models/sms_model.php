<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sms_model
 *
 * @author UseR
 */
class sms_model extends CI_Model {
    private $client_id;
    private $db_sms;
    
    public function init($client_id) {
        $this->db_sms = $this->load->database('sms', TRUE);
        $this->client_id = $client_id;
    }
    
    public function send_sms($from, $to, $message, $program = "", $client = "default"){
       
        $AccountSid	= "AC61b889873afca3f822517bcf3d7df75f";
	$AuthToken	= "9d9c543f5b3f2bea1f9bc4b7e90acb8f";
        
        $rt = array();

	$post = array(
		'From'		=> $from,
		'To'		=> $to,
		'Body'		=> $message,
	);
	$rt['post'] = $post;
        
        $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://api.twilio.com/2010-04-01/Accounts/'.$AccountSid.'/SMS/Messages.xml');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, $AccountSid.':'.$AuthToken);
	$resp = curl_exec($ch);
        
        $data = array(
                'tonumber' => $to,
                'fromnumber' => $from,
                'message' => $message,
                'resp' => str_replace('\'','`',$resp),
                'ip' => $_SERVER['REMOTE_ADDR'],
                'client' => $client,
                'program' => $program,
        );
        
        if ($this->db_sms->insert('smslog',$data)){
            $rt['mysql_query_success'] = 1;
        } else {
            $rt['mysql_query_success'] = 0;
        }
        
        $this->db_sms->close();
        
        return $rt;
    }
    
    public function getBLShortenUrl($longUrl, $field="url"){
        $result = array();
        /**
        * The URI of the standard bitly v3 API.
        */
        $bitly_api = "http://api.bit.ly/v3/";
        /**
        * The bitlyLogin assigned to your bit.ly account. (http://bit.ly/a/your_api_key)
        */
        $bitlyLogin = "o_1tg8ajp3t0";
        /**
        * The bitlyKey assigned to your bit.ly account. (http://bitly.com/a/your_api_key)
        */
        $bitlyKey = "R_df60820a2f64f1e1b4b12acf5a37e05b";
	$url = $bitly_api . "shorten?login=" . $bitlyLogin . "&apiKey=" . $bitlyKey . "&format=json&longUrl=" . urlencode($longUrl);

	$result = array();
	//try{
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 4);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$output = curl_exec($ch);
		//echo $output.'<br>';

		$output = json_decode($output);
		if (isset($output->{'data'}->{'hash'})) {
			$result['url'] = $output->{'data'}->{'url'};
			$result['hash'] = $output->{'data'}->{'hash'};
			$result['global_hash'] = $output->{'data'}->{'global_hash'};
			$result['long_url'] = $output->{'data'}->{'long_url'};
			$result['new_hash'] = $output->{'data'}->{'new_hash'};
		}
	/*} catch (Exception $e) {
		//echo 'fail<br>';
	}*/
	if($field == 'array'){
		return $result;
	}else{
		return $result[$field];
	}
    }
}
