<!--link rel="stylesheet" href="<?=ASSETPATH?>/css/bootstrap.min.css" /-->
<link rel="stylesheet" href="<?=ASSETPATH?>/css/validationEngine.jquery.css" />

<?php
if($_SERVER['SERVER_NAME'] == "sandbox.funeralnet.com"){
    echo '<link rel="stylesheet" href="<?=ASSETPATH?>/css/bootstrap.min.css" />';
    echo '<script src="<?=ASSETPATH?>/js/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="<?=ASSETPATH?>/css/style.css'.TAILSTRING.'" />';
} else {
    echo $customView;
   // include_once ('custom.php');
}
?>


<link rel="stylesheet" href="<?=ASSETPATH?>/css/merchant.css<?=TAILSTRING?>" />
<link rel="stylesheet" href="<?=ASSETPATH?>/css/datepicker3.css<?=TAILSTRING?>" />
<link rel="stylesheet" href="<?=ASSETPATH?>/css/magnific-popup.css<?=TAILSTRING?>" />
<link rel="stylesheet" href="<?=ASSETPATH?>/css/prism.css<?=TAILSTRING?>" />
<link rel="stylesheet" href="<?=ASSETPATH?>/css/shuffle-styles.css<?=TAILSTRING?>" />

<script src="<?=ASSETPATH?>/js/bootstrap-datepicker.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/jquery.magnific-popup.min.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/jquery.validationEngine.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/jquery.validationEngine-en.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/fnetmerchant.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/jsonElement.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/jquery.creditCardValidator.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/modernizr.custom.min.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/bootstrap-tooltip.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/bootstrap-popover.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/jquery.shuffle.min.js<?=TAILSTRING?>"></script>
<script src="<?=ASSETPATH?>/js/jquery.maskedinput.min.js<?=TAILSTRING?>"></script>
<script>
   var shops = {
        id : <?php echo $shops['id'] ;?>,
        sameDay : <?php echo $shops['allow_same_day'] ;?> ,
        sameDay_con : <?php echo !empty($shops['condition_same_day']) ? $shops['condition_same_day'] : '0';?> ,
        allow_same_day : <?php echo $shops['allow_same_day'];?> ,
        today : '<?php echo date('m/d/Y');?>'
   }
</script>

