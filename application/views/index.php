<center id="merchandise_wrapper">
	<?php $this->view('static-views/temporary_off_message'); ?>
    <div id="content-flowers">

        <p align="left" >Our mortuary has developed an exclusive arrangement with a local, well established florist to simplify ordering floral tributes for services at our funeral homes. Immediate family members, relatives and friends can order floral tributes for a private viewing, wake, funeral, visitation or memorial service here online.</p>

		<p align="left" >By placing the order here, we offer you convenience and timely delivery. The florist knows our service schedule and prepares and delivers orders accordingly.</p>
        
        <h3 style="text-align: left;">Please make a selection from the following categories:</h3>
        <?php $this->view('static-views/catagories') ?>

    </div>
</center>