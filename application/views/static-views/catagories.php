<style>
.merchant-btn-edit {
	display: inline-block;
	font-size: 1.2em;
	line-height: 20px;
	margin: 8px 0px;
	outline: none;
	padding: 0 10px;
	text-align: left;
	text-decoration: none !important;
	background-image: url('<?=base_url()?>/assets/images/btn-radio.png');
	background-repeat: no-repeat;
	padding-left: 26px;
	height: 25px;
	padding-top: 1px;
	transition-duration: 0ms !important;
}
.merchant-btn-edit:hover{
	background-position: 0px -25px;
}
</style>
<div class="row">
    <div class="col-md-6" style="text-align: center;">
        <img src="<?=base_url()?>/assets/images/hero-flower.jpg" alt="" width="300" border="0">
    </div>
    <div class="col-md-6" style="padding-top: 20px;">
        <?php foreach ($categories as $category_name) 
            { ?>
            <p class='type-flowers'><a class="merchant-btn-edit" style="width:100%;" href="<?=$category_name['link'] ?>"><?= $category_name['label'] ?></a></p>
        <?php } ?>
    </div>                
</div>