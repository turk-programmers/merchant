<?php
if ($shops['show_price'] == '1') { 
    ?>
    <div class="label-price">
        <?php 
        if(@$product['advance_price']['use-advance-pricing']) {
        	$prices = array_filter($product['advance_price']['prices'], function($price) {
        		return !@$price['disable'];
        	});
        	if(count($prices) > 1) {
        		?>
	            <select name='product_price'>
	                <optgroup>
	                    <?php 
			            foreach((array)$prices as $i=>$price) {
				            ?>
	                        <option value='<?=$i?>'><?php echo $price['label'] . (is_numeric($price['price']) ? ' $' . number_format($price['price'], (fmod($price['price'], 1) ? 2 : 0)) : " " . $price['price']); ?></option>
				            <?php 
			            }
	                    ?>
	                </optgroup>
	            </select>
        		<?php
        	} else {
	            foreach((array)$prices as $i=>$price) {
		            ?>
		            <input type='hidden' name='product_price' value='<?=$i?>' >
		            <?php echo $price['label'] . (is_numeric($price['price']) ? ' $' . number_format($price['price'], (fmod($price['price'], 1) ? 2 : 0)) : " " . $price['price']); ?>
		            <?php 
	            }
        	}
        } else {
	        if ($product['price2'] > 0 or $product['price3'] > 0) {
	            ?>
	            <select name='product_price'>
	                <optgroup>
	                    <?php 
	                    if ($product['price'] > 0) { 
	                        ?>
	                        <option value='price'><?php echo $settings['shop_config']['priceLabel']['price'] . ' $' . number_format($product['price'], (fmod($product['price'], 1) ? 2 : 0)); ?></option>
	                        <?php 
	                    }
	                    if ($product['price2'] > 0) {
	                        ?>
	                        <option value='price2'><?php echo $settings['shop_config']['priceLabel']['price2'] . ' $' . number_format($product['price2'], (fmod($product['price2'], 1) ? 2 : 0)); ?></option>
	                        <?php
	                    }
	                    if ($product['price3'] > 0) {
	                        ?>
	                        <option value='price3'><?php echo $settings['shop_config']['priceLabel']['price3'] . ' $' . number_format($product['price3'], (fmod($product['price3'], 1) ? 2 : 0)); ?></option>
	                        <?php
	                    }
	                    ?>
	                </optgroup>
	            </select>
	            <?php
	        } else {
	            ?>
	            <input type='hidden' name='product_price' value='price' >
	            <?php echo 'Price $' . number_format($product['price'], (fmod($product['price'], 1) ? 2 : 0)); ?>
	            <?php 
	        } 
        }
        ?>
    </div>
    <?php 
} 