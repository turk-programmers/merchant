<?php
$index = 1;
foreach ($this->cart->contents() as $pid => $product) {

    $prod['name'] = $product['name'];
    $prod['price'] = $product['price'];
    $prod['quantity'] = $product['qty'];
    $prod['totalprice'] = number_format($product['price'] * $product['qty'], 2);

    $choice = "";
    if(@$product['options']['advance_price']['use-advance-pricing']) {
        $choice = ' (' . @$product['options']['advance_price']['prices'][$product['options']['selected_price']]['label'] . ')';
    } elseif(@$product['options']['price2'] > 0 or @$product['options']['price3'] > 0) {
        $choice = ' (' . $settings['shop_config']['priceLabel'][$product['options']['selected_price']] . ')';
    }

    $product_image = "https://fnetclients.s3.amazonaws.com/merchant/clients/".$shops['client_id']."/pictures/".$product['options']['image'];
    $product_thumb = "https://fnetclients.s3.amazonaws.com/merchant/clients/".$shops['client_id']."/pictures/".$product['options']['thumb'];
    ?>

    <tbody>
        <tr>
            <td class='hidden-xs'><?= $index ?></td>
            <td>
                <a class="mfp-image image-link" href="<?=$product_image?>" data-mfp-src="<?=$product_image?>">
                    <img class='img-responsive' src="<?=$product_thumb?>" alt="<?php echo $product['options']['product_name']; ?>">
                </a>
                <div class="product_name"><?=$prod['name'].= $choice?></div>
                <?php
                if(count($product['options']['additional_options'])) {
                    ?>
                    <div class="product_options">
                        <?php
                        foreach((array)$product['options']['additional_options'] as $option) {
                            ?>
                            <div class="product_option">
                                <?=$option['label']?>:
                                <form action="" method="post">
                                    <input type=hidden name="rowid" value="<?= $product['rowid'] ?>"/>
                                    <input type=hidden name="action" value="update-option"/>
                                    <input type=hidden name="option_key" value="<?=$option['key']?>"/>
                                    <select name="option_value" onchange="this.form.submit()">
                                        <?php
                                        $sel = array(@$product['selected_options'][@$option['key']]['value'] => 'selected');
                                        foreach((array)$option['choices'] as $choice) {
                                            ?>
                                            <option value="<?=$choice?>" <?=@$sel[$choice]?>><?=$choice?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </form>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    // devprint($product);
                }
                ?>
            </td>

            <td>
                <div style='width:43px; float: left'>
                    <form action='' method='post'>
                        <select name='qty' onchange="this.form.submit()">
                            <?php
                            for ($i = 1; $i <= 10; $i++) {
                                ?>
                                <option value='<?php echo $i ?>' <?php echo $prod['quantity'] == $i ? 'selected' : '' ?>><?php echo $i ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input type='hidden' name='rowid' value='<?= $product['rowid'] ?>'/>
                        <input type='hidden' name='action' value='update'/>
                    </form>
                </div>
                <span class='delete_button' data-rowid='<?= $product['rowid'] ?>'>&nbsp;</span>
            </td>
            <td align='right' class='hidden-xs'><?php echo $prod['price']; ?></td>
            <td align='right'><?php echo $prod['totalprice']; ?></td>

        </tr>
    </tbody>

    <?php
    $index++;
}
?>