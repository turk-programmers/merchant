<?php
if(count($product['additional_options']) > 0) {
    ?>
    <div class="additional_options">
        <?php
        foreach((array)$product['additional_options'] as $option) {
            ?>
            <div class="additional_option">
                <?=$option['label']?>:<br>
                <select name="selected_options[<?=@$option['key']?>]">
                    <?php
                    foreach((array)$option['choices'] as $choice) {
                        ?>
                        <option value="<?=$choice?>"><?=$choice?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <?php
            ?>
            <input type="hidden" name="<?=@$option['label']?>" value="<?=@$option['choices'][0]?>" />
            <?php
        }
        ?>
    </div>
    <?php
    
    /*
    foreach((array)$product['additional_options'] as $option) {
        ?>
        <input type="hidden" name="selected_options[<?=@$option['key']?>]" value="<?=@$option['choices'][0]?>" />
        <?php
    }
    */
} else {
    ?>
    <div class="additional_options"></div>
    <?php
}