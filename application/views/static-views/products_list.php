<link rel="stylesheet" href="<?=ASSETPATH?>/css/product_list.css<?=TAILSTRING?>" />
<?php
$section = array();
?>
<div class='b-bread'>
    <div class="b-bread__btn">
        <?php
        foreach ($subcategories['subcategory_list'] as $subcategory) {
            if (!in_array($subcategory['section'], $section)) {
                array_push($section, $subcategory['section']);
            }
            $subcatbttclass = ($subcategory['name'] == $req_sub_category) ? 'b-bread__btn_active' : 'b-bread__btn_inactive';
            ?>
            <div class="b-bread__sub_level_1" data-link="<?php echo $subcategory['link']; ?>" data-tab='<?php echo $subcategory['section']; ?>' data-level='1' data-section='<?php echo $subcategory['section']; ?>'>
                <div class="b-bread__btn <?php echo $subcatbttclass; ?>" id='<?php echo $subcategory['section']; ?>'>
                    <div class="b-bread__label <?php echo $subcatbttclass; ?>">
                        <span class='label_name'><?php echo $subcategory['label'] . " (" . $subcategory['num_products'] . ")"; ?></span>
                    </div>
                </div>
            </div>
        <?php 
    } ?>

    </div>
</div>
<div class='pull-right'>
    <button style='margin-right: 5px; font-size: 25px' class='merchant-btn-revert btn-grid'>
        <i class="glyphicon glyphicon-th fa fa-th"></i>
    </button>
    <button style='margin-right: 5px; font-size: 25px' class='merchant-btn btn-list'>
        <i class="glyphicon glyphicon-list fa fa-th-list"></i>
    </button>
</div>
<div class='clearfix'></div>
<?php foreach ($section as $item) { ?>
    <div class='b-bread'>
        <div class="b-bread__btn">
            <div class='b-bread__sub b-bread__sub_level_2 <?php echo $item ?>' data-section='<?php echo $item ?>'>
                <?php
                foreach ($subcategories['subcategory_list_lv2'] as $subcategory) {
                    if ($item == $subcategory['section']) {
                        ?>
                        <div class="b-bread__btn" data-section='<?php echo $subcategory['name'] ?>' >
                            <div class="b-bread__label"><?php echo $subcategory['label'] ?></div>
                        </div>
                    <?php 
                } ?>

                <?php 
            } ?>

            </div>
        </div>
    </div>
<?php 
} ?>
<div class='clearfix'></div>
<div id='listproduct'>
    <div id="grid">

        <?php 
        foreach ($subcategories['product_list'] as $index => $product) { 
            $grid_width = 4;
            if(@$shops['product_column_number'] == 4) {
                $grid_width = 3;
            }
            ?>
            <div class='item product grid col-lg-<?=$grid_width?> col-md-<?=$grid_width?> col-sm-<?=$grid_width?> col-xs-12' data-groups='["<?php echo $product['category_name'] ?>" , "<?php echo $product['parent'] ?>"]' data-value="<?=@$product['product_id']?>">
                <div class='row'>
                    <div class='col-lg-12 col-xs-12 height-img'>

                        <img class='img-responsive ' src="https://fnetclients.s3.amazonaws.com/merchant/clients/<?= $shops['client_id'] ?>/pictures/<?= $product['thumb'] ?>" alt="<?php echo $product['product_name']; ?>">
                    </div>


                    <div class='col-lg-12 col-xs-12 form-group'>
                        <?php if (isset($product['product_name'])) echo "<p class='name'>{$product['product_name']}</p>" ?>
                        <?php //if (isset($product['shortdesc'])) echo "<h5>{$product['shortdesc']}</h5>" ?>
                        <?php //if (isset($product['longdesc'])) echo "<p class='description'>{$product['longdesc']}</p>" ?>
                        <?php //eval('$default_flower_package_name = ' . $settings['shop_config']['priceLabel'] . ';');   ?>
						<div class="view-detial-flowers"><a class="view-details-popup" href=".detail-<?=$product['product_id']?>">View Details</a></div>
                        <div id=" " class="white-popup mfp-hide flowers-popup detail-<?=$product['product_id']?>">
                            <div class="thumb-product">
                               <img src="https://fnetclients.s3.amazonaws.com/merchant/clients/<?= $shops['client_id'] ?>/pictures/<?= $product['image'] ?>" />
                            </div>
                            <div class="title-product">
                                <div class="name"><?php echo $product['product_name']; ?></div>
                                <?php $this->view("static-views/products_list_price_popup_detail", array(
                                    'product' => $product,
                                ));?>
                            </div>
                            <?php
                            if(strip_tags($product['shortdesc'].$product['longdesc']) != "") {
                                ?>
                                <div class="clear"></div>
                                <div class="container-data-desc">
                                    <p class="title-desc">Description</p>
                                    <p class="shortdesc"><?php echo nl2br($product['shortdesc']); ?></p>
                                    <p class="longdesc"><?php echo nl2br($product['longdesc']); ?></p>
                                </div>
                                <?php
                            }
                            ?>
                        </div>


                        <form action="" method="post">

                            <div class='product_price_box'>
                                <?php 
                                $this->view("static-views/products_list_price_field", array(
                                    'product' => $product,
                                ));
                                $this->view("static-views/products_list_additional_options", array(
                                    'product' => $product,
                                ));
                                ?>
                                <br>
                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>">
                                <?php } ?>

                                <?php if ($shops['ecommerce'] == 1) { ?>

                                    <button type="submit" value='Add to Cart' name="product_submit" id="cart_btt<?php echo $product['product_id']; ?>" class="merchant-btn btn-add" data-name='<?=$product['product_name']?>' data-img='https://fnetclients.s3.amazonaws.com/merchant/clients/<?= $shops['client_id'] ?>/pictures/<?= $product['thumb'] ?>'>
                                    <!-- <i class="glyphicon glyphicon-shopping-cart"></i>--> Add to cart
                                    </button>

                                <?php } ?>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
            <div class='clearfix'></div>
        <?php } ?>
    </div>
</div>
<?php if ($shops['ecommerce'] == 1) { ?>
    <div class='contain-step'>
        <div class='pull-right'>
            <!--<a class='btn btn-default previous' href="/<?php echo $shops['name'] ?>/products">Continue Shopping</a>-->
            <a class='merchant-btn-revert next <?php echo $this->cart->total_items() == 0? 'noitems' : '' ?>' href="/<?php echo $shops['name'] ?>/cart">Go to Cart</a>
        </div>
    </div>
<?php } ?>
<div id="divtmpflw"></div>
<?php
if(file_exists(getcwd()."/assets/core/core-less-css.php")) {
    ?>
    <script src="<?=ASSETPATH?>/js/item_adjusting.js<?=TAILSTRING?>"></script>
    <script src="<?=ASSETPATH?>/js/product_list.js<?=TAILSTRING?>"></script>
    <?php
}
?>