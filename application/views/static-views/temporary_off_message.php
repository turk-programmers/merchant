<?php
if(@$off_state) {
	// devprint($off_records);
	?>
	<div class="temporary-off-message">
		<p>
			<?php
			if(@$next_service_date) {
				?>
				Please be notified: 
				The flower store is temporary closed, and will be available for delivery again on <span style="white-space: nowrap;"><?=date("F d, Y", strtotime($next_service_date))?></span>.<br>
				<?php
			} else {
				?>
				Please be notified: The flower store is temporary closed, and will be available again soon.<br>
				<?php
			}
			?>
		</p>
	</div>
	<?php
}