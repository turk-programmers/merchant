<div class="price">
	<?php 
    if ($shops['show_price'] == '1') { ?>
        <div class="price-popup">
            <?php 
            if(@$product['advance_price']['use-advance-pricing']) {
                $prices = array_filter((array)$product['advance_price']['prices'], function($price) {
                    return !@$price['disable'];
                });
                foreach((array)$prices as $price) {
                    echo '<span class="price-popup">'.$price['label'] . ' :</span> <strong> ' . (is_numeric($price['price']) ? "$".number_format($price['price'], fmod($price['price'], 1) ? 2 : 0) : $price['price']) . '</strong><br>';
                }
            } else {
                if($product['price2'] > 0 or $product['price3'] > 0) {
                    echo "<strong>";
                    if(@$product['price'] > 0) {
                        echo '<span class="price-popup">'.$settings['shop_config']['priceLabel']['price'] . ' :</span> <strong> $' . number_format($product['price'],fmod($product['price'], 1) ? 2 : 0).'</strong><br>';
                    }
                    if (@$product['price2'] > 0) {
                        echo '<span class="price-popup">'.$settings['shop_config']['priceLabel']['price2'] . ' :</span> <strong>$' . number_format($product['price2'],fmod($product['price2'], 1) ? 2 : 0).'</strong><br>';
                    } 
                    if (@$product['price3'] > 0) {
                        echo '<span class="price-popup">'.$settings['shop_config']['priceLabel']['price3'] . ' :</span><strong> $' . number_format($product['price3'],fmod($product['price3'], 1) ? 2 : 0).'</strong><br>';
                    } 
                    echo "</strong>";
                } elseif($product['price'] > 0) {
                    echo "<strong>";
                    echo '<span class="price-popup">Price :</span> <strong> $' . number_format($product['price'],fmod($product['price'], 1) ? 2 : 0).'</strong><br>';
                    echo "</strong>";
                }
            }
            ?>
        </div>
	   <?php 
    }
    ?>
</div>