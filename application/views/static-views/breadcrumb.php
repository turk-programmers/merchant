<div id="breadcrumb">
    <?php if ($this->uri->segment(1) == 'products') { ?>  
        <?php if ($shops['ecommerce'] == 1) { ?>
            <?php $show_quantity = ($this->cart->total_items() == 0) ? 'Empty' : $this->cart->total_items(); ?>            
                <a href="" title="" class='showcart'> 
                    <div id="cart" style="float:right; cursor:pointer;margin-top: -7px;"  data-toggle="popover" title="Item(s) in cart" data-container="body" data-placement="left" data-content="loading...."><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;Cart 
                        <div class="cart-qty-container">
                            <span class="left"></span><span class="cart_qty"><?php echo $show_quantity; ?></span><span class="right"></span><br class="clear">
                        </div>
                    </div>
                </a>                        
        <?php } ?>
        
        <a href="/<?php echo $shops['name'] ?>/products/"><?php echo $shops['label'] ?></a> / 
        <?php echo urldecode($this->uri->segment(2)) ?>  
    <?php } ?>
        
    <?php if ($this->uri->segment(1) == 'cart') { ?>          
        
        <a href="/<?php echo $shops['name'] ?>/products/"><?php echo $shops['label'] ?></a> / 
        <?php echo ucfirst($this->uri->segment(1)) ?>  
    <?php } ?>    
        
    <?php if ($this->uri->segment(1) == 'billing') { ?>          
        
        <a href="/<?php echo $shops['name'] ?>/products/"><?php echo $shops['label'] ?></a> / 
        <a href="/<?php echo $shops['name'] ?>/cart/">Cart</a> / 
        Billing Information
    <?php } ?>     
        
    <?php if ($this->uri->segment(1) == 'delivery') { ?>          
        
        <a href="/<?php echo $shops['name'] ?>/products/"><?php echo $shops['label'] ?></a> / 
        <a href="/<?php echo $shops['name'] ?>/cart/">Cart</a> / 
        <a href="/<?php echo $shops['name'] ?>/billing/">Billing Information</a> / 
        Delivery Address
    <?php } ?>   
        
    <?php if ($this->uri->segment(1) == 'payment') { ?>          
        
        <a href="/<?php echo $shops['name'] ?>/products/"><?php echo $shops['label'] ?></a> / 
        <a href="/<?php echo $shops['name'] ?>/cart/">Cart</a> / 
        <a href="/<?php echo $shops['name'] ?>/billing/">Billing Information</a> / 
        <a href="/<?php echo $shops['name'] ?>/delivery/">Delivery Address</a> / Credit Card Information
    <?php } ?>   
</div>
<div id='areaajax' style="display: none">
    Loading...
</div>
