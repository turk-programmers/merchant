
<style>
    .flyover {
        left: 150%;
        overflow: hidden;
        position: fixed;
        width: 50%;
        opacity: 0.9;
        z-index: 1050;
        transition: left 0.6s ease-out 0s;
    }
    .flyover-bottom {
        bottom: 10px;
    }
    .flyover-top {
        top: 10px;
    }
    .flyover.in {
        left: 50%;
    }
    .alert {
        background-color: #FCF8E3;
        border: 1px solid #FBEED5;
        border-radius: 4px 4px 4px 4px;
        color: #C09853;
        margin-bottom: 20px;
        padding: 10px 35px 10px 15px;
    }
    .alert h4 {
        color: inherit;
        margin-top: 0;
    }
    .alert-danger {
        background-color: #F2DEDE;
        border-color: #EED3D7;
        color: #B94A48;
    }
    .alert-success {
        background-color: #E0F3E6;
        border-color: #EED3D7;
        color: #49BB56;
    }
    .product {    
        padding: 8px;      
        border:1px solid gray;
        margin: 1px 0px 12px 13px;
    }
    .btn-add{
        margin: 8px 0px 8px 0px;
    }
    .contain-step {
        margin: 0px 0px 0px 0px;
        padding: 10px 0px 36px 0px;
    }

    #merchandise_wrapper .noproductrow {
        padding: 120px 0 140px 0;
        text-align: center;
        font-weight: bold;
    }
    #merchandise_wrapper .subcategory_btt {
        text-transform: capitalize;
    }
    #merchandise_wrapper .product_qty {
        height: 26px;
    }
    #merchandise_wrapper .pic {
        border: 1px solid #AC8A5A;
        max-width: 180px;
        max-height: 210px;
    }

    #merchandise_wrapper .category_btt, .active_category_btt {
        color: white;
        padding: 5px 20px;
        text-transform: capitalize;
    }
    #merchandise_wrapper .category_btt {
        border: solid 1px #C4C4C4;
        background-color: #C4C4C4;
    }
    #merchandise_wrapper .category_btt:hover {
        border: solid 1px #B8B8B8;
        background-color: #B8B8B8;
    }
    #merchandise_wrapper .active_category_btt {
        border: solid 1px #35709C;
        background-color: #35709C;
        cursor: default;
    }
    #merchandise_wrapper #breadcrumb {
        background-color: #FFF;
        padding: 13px 30px;
        margin: 0 0 25px 0;
        font-size: 13px;
        color: #999;
    }
    #merchandise_wrapper #breadcrumb a {
        color: #999;
    }
    #merchandise_wrapper #breadcrumb #cart {
        /*position: absolute;
        right: 3px;
        top: 3px;*/

        border: #e7e7e7 1px solid;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        height: 34px;
        overflow: hidden;
        line-height: 34px;
        font-family: 'Arial',Verdana,Tahoma;
        color: #767676;
        padding: 0 4px 0 13px;        
        background-repeat: no-repeat;
        background-position: 10px 50%;
        cursor: pointer;
    }
    #merchandise_wrapper #breadcrumb #cart span.cart_qty {
        display: inline-block;
        float: left;
        color: #FFF;
        background-color: #d2d2d2;
        margin: 7px 0px 0px 0px;
        padding: 0px 3px;
        height: 20px;
        line-height: 20px;
        font-family: 'Arial',Verdana,Tahoma;
        font-size: 14px;
    }

    #merchandise_wrapper #breadcrumb #cart div.cart-qty-container {
        position: relative;
        display: inline-block;
        margin: 0px 5px 0px 5px;
    }
    #merchandise_wrapper #breadcrumb #cart span.left {
        display: inline-block;
        float: left;
        width: 9px;
        height: 34px;
        overflow: hidden;
        background-image: url(<?php echo DEFAULT_FRONT_IMAGE; ?>qty-left.jpg);
        background-position: 0px 50%;
        background-repeat: no-repeat;
    }
    #merchandise_wrapper #breadcrumb #cart span.right {
        display: inline-block;
        float: left;
        width: 4px;
        height: 34px;
        overflow: hidden;
        background-image: url(<?php echo DEFAULT_FRONT_IMAGE; ?>qty-right.jpg);
        background-position: 0px 50%;
        background-repeat: no-repeat;
    }

    #merchandise_wrapper .add_cart_btt {
        border: solid 1px gray;
        background-color: gray;
        color: white;
        padding: 2px 12px;
    }
    #merchandise_wrapper .add_cart_btt:hover {
        border: solid 1px #777;
        background-color: #777;
    }
    #merchandise_wrapper .add_cart_btt {}

    #merchandise_wrapper .back_btt, #merchandise_wrapper .submit_btt {
        display: inline-block;
        padding: 8px 20px;
        color: white;
        -moz-border-radius: 7px;
        -webkit-border-radius: 7px;
        border-radius: 7px;
        text-decoration: none;
    }
    #merchandise_wrapper .back_btt {
        background: #B1B1B1;
    }
    #merchandise_wrapper .back_btt:hover {
        background: #A8A8A8;
    }
    #merchandise_wrapper .back_btt:active {
        background: #999999;
    }
    #merchandise_wrapper .submit_btt {
        background: #579E27;
    }
    #merchandise_wrapper .submit_btt:hover {
        background: #519225;
    }
    #merchandise_wrapper .submit_btt:active {
        background: #45831B;
    }
    #merchandise_wrapper p {
        margin-left:10px;   
    }
</style>
<div id="alertmsg1" class="alert alert-success flyover flyover-top"><h4>Success</h4><p> You have added product to your shopping cart!</p></div>
<div id="merchandise_wrapper">

    <?php $this->load->view('breadcrumb', array('shops' => $shops)); ?> 

    <p>Substitutions of an arrangement and container of similar value may be made in the unlikely event that your selection is unavailable. <br><br>Please allow 24 hours for delivery. Orders placed after 4pm on Fri. will not be available for delivery until the following Monday after 11 am.</p>

    <h3 style="margin-bottom:25px; text-transform:capitalize;">&nbsp;<?php echo $req_sub_category; ?></h3>

    <p align="center">
        <?php
        $productid = array(); // 1002,1003
        $productqty = array(); // 5
        $productrow = array(); // 1002,1003_1
        $rowids = array();
        foreach ($this->cart->contents() as $rowid => $item) {
            array_push($productid, $item['id']);
            array_push($productqty, $item['qty']);
            array_push($productrow, $item['product_row']);
            array_push($rowids, $rowid);
        }

        foreach ($subcategories['subcategory_list'] as $arr) {
            $subcatbttclass = ($arr['name'] == $req_sub_category) ? 'active_category_btt' : 'category_btt';
            ?>
            <button class="<?php echo $subcatbttclass; ?>" value="<?php echo $arr['link']; ?>" onclick="window.open(this.value, '_top')"><?php echo $arr['name'] . " (" . $arr['num_products'] . " items)"; ?></button>
        <?php } ?>
    </p>


    <?php foreach ($subcategories['product_list'] as $index => $arr) { ?>       

        <div class='product'>
            <div class='row'>
                <!--
                --
                -- Image
                --
                -->
                <div class='col-lg-3 col-md-3 col-sm-4 col-xs-5 form-group'>                                   

                    <a class="mfp-image image-link" href="https://www.maxsass.com/flowers/graphics/CL13.jpg" data-mfp-src="https://www.maxsass.com/flowers/graphics/CL13.jpg">
                        <img class='img-responsive' src="https://www.maxsass.com/flowers/graphics/CL13.jpg" alt="<? echo $arr['product_name']; ?>">   
                    </a>
                    <br clear="all" />
                </div>                          


                <div class='col-lg-9 col-md-9 col-sm-8 col-xs-7 form-group'>
                    <?php if (isset($arr['shortdesc'])) echo "<h5>{$arr['shortdesc']}</h5>" ?>
                    <?php if (isset($arr['shortdesc'])) echo "<p>({$arr['product_name']})</p>" ?>
                    <?php if (isset($arr['longdesc'])) echo "<p>{$arr['longdesc']}</p>" ?>	
                    <?php eval('$default_flower_package_name = ' . $shops['package_name'] . ';'); ?>

                    <?php if (($arr['price'] != '0.00') && ($arr['price2'] != '0.00') && ($arr['price3'] != '0.00')) { ?>
                        <form action="" method="post">
                            <div class='col-lg-6 col-md-10 col-sm-8 col-xs-12 product_name'>

                                <?php
                                if ($shops['show_price'] == '1')
                                    echo $default_flower_package_name['price'] . ' $' . $arr['price'];
                                ?>

                                <?php
                                $has = false;
                                $qty = 0;
                                if (in_array($arr['product_id'] . '_1', $productrow, true)) {
                                    $has = true;
                                    $id = array_search($arr['product_id'] . '_1', $productrow, true);
                                    $qty = $productqty[$id];
                                    $row = $productrow[$id];
                                    $rowid = $rowids[$id];
                                    ?>

                                    <?php
                                }
                                ?>
                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <select name='qty'>
                                        <?php
                                        for ($i = 1; $i <= 10; $i++) {
                                            $select = $qty == $i ? "selected" : "";
                                            echo "<option value='$i' $select >$i</option>";
                                        }
                                        ?>
                                    </select>

                                    <input type="hidden" name="product_price" value="price">
                                    <input type="hidden" name="productrow" value="<?php echo $arr['product_id']; ?>_1">
                                    <input type="hidden" name="product_id" value="<?php echo $arr['product_id']; ?>">


                                <?php } ?>

                            </div> 
                            <br class='clear'>
                            <div class='col-lg-2 col-md-2 col-sm-3 col-xs-6'>
                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <?php if ($has) { ?>
                                        <button type="submit" value='Update' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-refresh"></i> Update
                                        </button>
                                    <?php } else { ?>

                                        <button type="submit" value='Add to Cart' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-shopping-cart"></i> Add
                                        </button>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </form>
                        <br class='clear'>                                
                        <form action="" method="post">
                            <div class='col-lg-6 col-md-10 col-sm-8 col-xs-12 product_name'>
                                <?php
                                if ($shops['show_price'] == '1')
                                    echo $default_flower_package_name['price2'] . ' $' . $arr['price2'];
                                ?>

                                <?php
                                $has = false;
                                $qty = 0;
                                if (in_array($arr['product_id'] . '_2', $productrow, true)) {
                                    $has = true;
                                    $id = array_search($arr['product_id'] . '_2', $productrow, true);

                                    $qty = $productqty[$id];
                                    $row = $productrow[$id];
                                    $rowid = $rowids[$id];
                                    ?>

                                    <?php
                                }
                                ?>
                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <select name='qty'>
                                        <?php
                                        for ($i = 1; $i <= 10; $i++) {
                                            $select = $qty == $i ? "selected" : "";
                                            echo "<option value='$i' $select >$i</option>";
                                        }
                                        ?>
                                    </select>

                                    <input type="hidden" name="product_price" value="price2">
                                    <input type="hidden" name="productrow" value="<?php echo $arr['product_id']; ?>_2">
                                    <input type="hidden" name="product_id" value="<?php echo $arr['product_id']; ?>">                                         
                                <?php } ?>
                            </div>
                            <br class='clear'>
                            <div class='col-lg-2 col-md-2 col-sm-3 col-xs-6'>
                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <?php if ($has) { ?>
                                        <button type="submit" value='Update' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-refresh"></i> Update
                                        </button>
                                    <?php } else { ?>

                                        <button type="submit" value='Add to Cart' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-shopping-cart"></i> Add
                                        </button>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </form>

                        <br class='clear'>
                        <form action="" method="post">
                            <div class='col-lg-6 col-md-10 col-sm-8 col-xs-12 product_name'>
                                <?php
                                if ($shops['show_price'] == '1')
                                    echo $default_flower_package_name['price3'] . ' $' . $arr['price3'];
                                ?>

                                <?php
                                $has = false;
                                $qty = 0;
                                if (in_array($arr['product_id'] . '_3', $productrow, true)) {
                                    $has = true;
                                    $id = array_search($arr['product_id'] . '_3', $productrow, true);

                                    $qty = $productqty[$id];
                                    $row = $productrow[$id];
                                    $rowid = $rowids[$id];
                                    ?>

                                    <?php
                                }
                                ?>
                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <select name='qty'>
                                        <?php
                                        for ($i = 1; $i <= 10; $i++) {
                                            $select = $qty == $i ? "selected" : "";
                                            echo "<option value='$i' $select >$i</option>";
                                        }
                                        ?>
                                    </select>

                                    <input type="hidden" name="product_price" value="price3">
                                    <input type="hidden" name="productrow" value="<?php echo $arr['product_id']; ?>_3">
                                    <input type="hidden" name="product_id" value="<?php echo $arr['product_id']; ?>">                                         
                                <?php } ?>
                            </div>
                            <br class='clear'>
                            <div class='col-lg-2 col-md-2 col-sm-3 col-xs-6'>

                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <?php if ($has) { ?>
                                        <button type="submit" value='Update' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-refresh"></i> Update
                                        </button>
                                    <?php } else { ?>

                                        <button type="submit" value='Add to Cart' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-shopping-cart"></i> Add
                                        </button>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </form>

                    <?php } elseif (($arr['price'] != '0.00') && ($arr['price2'] != '0.00')) { ?>
                        <form action="" method="post">
                            <div class='col-lg-6 col-md-10 col-sm-8 col-xs-12 product_name'>

                                <?php
                                if ($shops['show_price'] == '1')
                                    echo $default_flower_package_name['price'] . ' $' . $arr['price'];
                                ?>

                                <?php
                                $has = false;
                                $qty = 0;
                                if (in_array($arr['product_id'] . '_1', $productrow, true)) {
                                    $has = true;
                                    $id = array_search($arr['product_id'] . '_1', $productrow, true);
                                    $qty = $productqty[$id];
                                    $row = $productrow[$id];
                                    $rowid = $rowids[$id];
                                    ?>

                                    <?php
                                }
                                ?>
                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <select name='qty'>
                                        <?php
                                        for ($i = 1; $i <= 10; $i++) {
                                            $select = $qty == $i ? "selected" : "";
                                            echo "<option value='$i' $select >$i</option>";
                                        }
                                        ?>
                                    </select>

                                    <input type="hidden" name="product_price" value="price">
                                    <input type="hidden" name="productrow" value="<?php echo $arr['product_id']; ?>_1">
                                    <input type="hidden" name="product_id" value="<?php echo $arr['product_id']; ?>">


                                <?php } ?>

                            </div> 
                            <br class='clear'>
                            <div class='col-lg-2 col-md-2 col-sm-3 col-xs-6'>
                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <?php if ($has) { ?>
                                        <button type="submit" value='Update' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-refresh"></i> Update
                                        </button>
                                    <?php } else { ?>

                                        <button type="submit" value='Add to Cart' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-shopping-cart"></i> Add
                                        </button>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </form>

                        <br class='clear'>
                        <form action="" method="post">
                            <div class='col-lg-6 col-md-10 col-sm-8 col-xs-12 product_name'>
                                <?php
                                if ($shops['show_price'] == '1')
                                    echo $default_flower_package_name['price2'] . ' $' . $arr['price2'];
                                ?>

                                <?php
                                $has = false;
                                $qty = 0;
                                if (in_array($arr['product_id'] . '_2', $productrow, true)) {
                                    $has = true;
                                    $id = array_search($arr['product_id'] . '_2', $productrow, true);

                                    $qty = $productqty[$id];
                                    $row = $productrow[$id];
                                    $rowid = $rowids[$id];
                                    ?>

                                    <?php
                                }
                                ?>
                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <select name='qty'>
                                        <?php
                                        for ($i = 1; $i <= 10; $i++) {
                                            $select = $qty == $i ? "selected" : "";
                                            echo "<option value='$i' $select >$i</option>";
                                        }
                                        ?>
                                    </select>

                                    <input type="hidden" name="product_price" value="price2">
                                    <input type="hidden" name="productrow" value="<?php echo $arr['product_id']; ?>_2">
                                    <input type="hidden" name="product_id" value="<?php echo $arr['product_id']; ?>">                                         
                                <?php } ?>
                            </div>
                            <br class='clear'>
                            <div class='col-lg-2 col-md-2 col-sm-3 col-xs-6'>
                                <?php if ($shops['ecommerce'] == 1) { ?>
                                    <?php if ($has) { ?>
                                        <button type="submit" value='Update' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-refresh"></i> Update
                                        </button>
                                    <?php } else { ?>

                                        <button type="submit" value='Add to Cart' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-shopping-cart"></i> Add
                                        </button>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </form>

                    <?php } elseif (($arr['price'] != '0.00') && ($arr['price2'] = '0.00') && ($arr['price3'] = '0.00')) { ?>
                        <form action="" method="post">
                            <div class='col-lg-6 col-md-10 col-sm-8 col-xs-12 product_name'>
                                <?php
                                if ($shops['show_price'] == '1')
                                    echo 'Price : $' . $arr['price'];
                                ?>
                                <?php
                                $has = false;
                                $qty = 0;
                                if (in_array($arr['product_id'], $productid, true)) {
                                    $has = true;
                                    $id = array_search($arr['product_id'], $productid, true);
                                    $qty = $productqty[$id];
                                    $row = $productrow[$id];
                                    $rowid = $rowids[$id];
                                    ?>

                                    <?php
                                }
                                ?>

                                <?php if ($shops['ecommerce'] == 1) { ?>

                                    <select name='qty'>
                                        <?php
                                        for ($i = 1; $i <= 10; $i++) {
                                            $select = $qty == $i ? "selected" : "";
                                            echo "<option value='$i' $select >$i</option>";
                                        }
                                        ?>
                                    </select>

                                    <input type="hidden" name="product_price" value="price">
                                    <input type="hidden" name="productrow" value="<?php echo $arr['product_id']; ?>">
                                    <input type="hidden" name="product_id" value="<?php echo $arr['product_id']; ?>">                                        

                                <?php } ?>
                            </div> 
                            <br class='clear'>
                            <div class='col-lg-2 col-md-2 col-sm-3 col-xs-6'>
                                <?php if ($shops['ecommerce'] == 1) { ?>

                                    <?php if ($has) { ?>
                                        <button type="submit" value='Update' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-refresh"></i> Update
                                        </button>
                                    <?php } else { ?>

                                        <button type="submit" value='Add to Cart' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-shopping-cart"></i> Add
                                        </button>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </form>

                    <?php } ?>                                                                                                                        
                </div>
            </div>
        </div>

    <?php } ?>

    <?php if ($shops['ecommerce'] == 1) { ?>
        <div class='contain-step'>
            <div class='pull-right'>            
                <a class='btn btn-default previous' href="/<?php echo $shops['name'] ?>/products">Continue Shopping</a>                        
                <a class='btn btn-success next' href="/<?php echo $shops['name'] ?>/cart">Go to Cart</a>            
            </div> 
        </div>
    <?php } ?>
    <div id="divtmpflw"></div>
</div>
<script>

    //http://dimsemenov.com/plugins/magnific-popup/
    //http://dimsemenov.com/plugins/magnific-popup/documentation.html
    $(document).ready(function() {
        $('.image-link').magnificPopup({type: 'image'});

        var ajaxAddToCart;
        $('button[name=product_submit]').click(function(e) {
            e.preventDefault();
            var form = $(this).parent().parent();
            var btn = $(this);
            $('.next , .previous').button('loading');
            //$('button[name=product_submit]').button('loading');
            ajaxAddToCart = $.ajax({
                type: 'POST',
                data: form.serialize(),
                url: '/<?php echo $shops['name'] ?>/ajax_add_to_cart/',
                beforeSend: function() {

                    try {
                        ajaxAddToCart.abort();
                    } catch (Exception) {
                        //$('button[name=product_submit]').button('reset');
                        $('.next , .previous').button('reset');
                    }
                },
                success: function(xhr) {
                    //$('button[name=product_submit]').button('reset');
                    $('.next , .previous').button('reset');
                    btn.html('<i class="glyphicon glyphicon-refresh"></i> Update');
                    var jsonData = $.parseJSON(xhr);
                    if (!$('#alertmsg1').is('.in')) {
                        $('#alertmsg1').addClass('in');

                        setTimeout(function() {
                            $('#alertmsg1').removeClass('in');
                        }, 3200);
                    }
                    $('.cart_qty').html(jsonData['total_items']);
                }
            });

            return false;
        });


    });

</script>