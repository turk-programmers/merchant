<link rel="stylesheet" href="<?=ASSETPATH?>/css/products.css<?=TAILSTRING?>" />
<style>
    #merchandise_wrapper #breadcrumb #cart span.left {
        background-image: url(<?php echo DEFAULT_FRONT_IMAGE; ?>qty-left.jpg);
    }
    #merchandise_wrapper #breadcrumb #cart span.right {
        background-image: url(<?php echo DEFAULT_FRONT_IMAGE; ?>qty-right.jpg);
    }
</style>
<div id="alertmsg1" class="alert alert-success flyover flyover-top"><h4>Success</h4>
    <p> You have added product to your shopping cart!</p></div>
<div id="merchandise_wrapper">
    <?php $this->view('static-views/breadcrumb', array('shops' => $shops)); ?>
    <?php $this->view('static-views/temporary_off_message'); ?>
    <?php $this->view('products/page-paragraph'); ?>
    <?php $this->view('products/page-title'); ?>
    <?php $this->view('static-views/products_list'); ?>
</div>
<script>
    var shopname = "<?php echo $shops['name'] ?>";
</script>
<script src="<?=ASSETPATH?>/js/product.js<?=TAILSTRING?>"></script>