<div class='form-group'>
    <label for='firstname'>First Name</label>
    <input type='text' name='firstname' id='firstname' class='form-control validate[required]' placeholder='First Name'/>
</div>
<div class='form-group'>
    <label for='lastname'>Last Name</label>
    <input type='text' name='lastname' id='lastname' class='form-control validate[required]' placeholder='Last Name'/>
</div>
<div class='form-group'>
    <label for='email'>E-mail</label>
    <input type='text' class='form-control validate[required,custom[email]]' name='email' id='email' placeholder='E-mail' data-value='kapom@frazerconsultants.com' />
</div>
<div class='form-group'>
    <label for='streetaddress'>Street Address</label>
    <input type='text' class='form-control validate[required]' name='streetaddress' id='streetaddress' placeholder='Street Address'/>
</div>
<div class='form-group'>
    <label for='city'>City</label>
    <input type='text' class='form-control validate[required]' name='city' id='city' placeholder='City'/>
</div>
<div class='form-group'>
    <label for='state'>State</label>
    <select class='form-control validate[required]' name='state'>
        <?php foreach($cfg['states'] as $key=>$group ): ?>
        <option value='<?php echo $group['code'] ?>'><?php echo $group['name'] ?></option>
        <?php endforeach ?>
    </select>
</div>
<div class='form-group'>
    <label for='zip'>Zip</label>
    <input type='text' class='form-control validate[required,custom[onlyLetterNumber]]' name='zip' id='zip' placeholder='Zip'/>
</div>
<div class='form-group'>
    <label for='daytimephone'>Daytime Phone</label>
    <input type='text' class='form-control validate[required]' name='daytimephone' id='daytimephone' placeholder='Daytime Phone'/>
</div>