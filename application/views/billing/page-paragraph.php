<p>
	Substitutions of an arrangement and container of similar value may be made in the unlikely event that your selection is unavailable.<br>
	<?php
	if (@$shops['allow_same_day'] == 0) {
		?>
		<br>
		Please allow 24 hours for delivery. Orders placed after 4pm on Fri. will not be available for delivery until the following Monday after 11 am.
		<?php
	}
	?>
</p>