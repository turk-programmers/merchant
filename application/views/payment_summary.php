<div class="col-lg-6 col-md-6 col-sm-6 col-xs-10 merchant-summary">                

    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>Sale tax (<?php echo $this->cart->getTax() ?>%).</div>
    </div>

    <div class='row'>
        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>Delivery fee.</div>
        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'></div>
        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>$<?php echo $this->cart->getDeliveryFee() ?></div>
    </div>
    <div class='row'>
        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>tax amount.</div>
        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'></div>
        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>$<?php echo $this->cart->calTax() ?></div>
    </div>
    <div class='row'>
        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>Total</div>
        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'></div>
        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'><b><i><u>$<?php echo $this->cart->totalWithTax() ?></u></i></b></div>
    </div>
</div>