<?php
    include_once('/home/funeralnet/public_html/mfscommon/php/stripe/init.php');
    \Stripe\Stripe::setApiKey($secret_key);
    // $total_charge = $this->cart->total() ;
    // if( gettype( $total_charge ) == 'double' ){
    //     $tmp = explode(".",$total_charge) ; 
    //     $total_charge = $tmp[0].str_pad((@$tmp[1]), 2 , 0,STR_PAD_RIGHT) ;
    // }
    // else{
    //     $total_charge .= "00";
    // }
?>
<style>

</style>
<script>
    $(function() {
        <?php
        if (@$prev_form) {
            ?>
            $.mc.prev_form = '<?= $prev_form ?>';
            <?php
        }
        ?>
        $.mc.next_form = '<?= $next_form ?>';
    });
</script>

<div id="merchandise_wrapper">
    <?php $this->view('static-views/breadcrumb', array('shops' => $shops)); ?>
    <?php $this->view('static-views/temporary_off_message'); ?>
    <?php
    if (@$error_message) {
        ?>
        <div class="errorMessage" style='color:red;'>
            <?php echo $error_message; ?>
        </div>
        <?php
    }
    ?>
    <form role="form" action="" method="post" id='mainform'>
        <input type="hidden" name="scriptaction"    id="scriptaction"   value="validate" />
        <input type="hidden" name="next_form"       id="next_form"      value="<?= $next_form ?>" />
        <div class='container-payment'>
            <div class='row'>
                <?php $this->load->view('summary_cart'); ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-10 merchant-summary">
                    <div class='row'>
                        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>Sales tax</div>
                        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'></div>
                        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'><?php echo $this->cart->getTax() ?>%</div>
                    </div>
                    <div class='row'>
                        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>Delivery fee</div>
                        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'></div>
                        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>$<?php echo $this->cart->getDeliveryFee() ?></div>
                    </div>
                    <div class='row'>
                        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>Tax amount</div>
                        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'></div>
                        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>$<?php echo $this->cart->calTax() ?></div>
                    </div>
                    <div class='row'>
                        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>Total</div>
                        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'></div>
                        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'><b><i><u>$<?php echo $this->cart->totalWithTax() ?></u></i></b></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='row col-lg-7 pull-right'>
            <a class='merchant-btn btn-back' href="#">Return to Delivery</a>
            <?php
            if($shops['payment_gateway']!='Paypal' and $next_service_date !== false) {
                ?>
                <a class='merchant-btn-revert btn-next' <?=is_debugger(@$user_session['billing']['email']) ? "data-dev=1" : ""?> href="">Make Payment</a>
                <?php
            }
            ?>
        </div>
        <div class='form-group stripe-block' style='display:none;'>
            <div class='col-sm-offset-2 col-sm-10'>
                <script src="https://checkout.stripe.com/checkout.js"
                    class="stripe-button"
                    data-key="<?php echo $publishable_key; ?>"
                    data-amount="<?=number_format($this->cart->totalWithTax(), 2, '', '')?>"
                    data-description="Secure Credit Card Payment">
                </script>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $('.btn-back').click(function(e){
        e.preventDefault();
        $.mc.goBack();
    });
    $('.btn-next').on('click',function(){
        if($(this).data('dev')) {
            $('#mainform').submit();
            return false;
        }
        $('.stripe-button-el').trigger('click') ;
        return false ;
    })
</script>
