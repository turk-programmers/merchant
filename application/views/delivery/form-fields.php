<?php if ($shops['show_locations'] == '1') { ?>
    <div class='form-group'>
        <label for='state'>Deliver To</label>
        <select class='form-control deliverto validate[required]' name='deliverto'>
            <option value='' data-street='' data-city='' data-state='' data-zip='' data-salestax='' data-default-salestax='' data-name='' >Please select the location</option>
            <?php foreach ($locations as $key => $group): ?>
                <!--
                <?php print_r($group) ?>
                -->
                <option value='<?php echo $group['id'] ?>' data-street='<?php echo $group['address_street'] ?>' data-city='<?php echo $group['address_city'] ?>' data-state='<?php echo $group['address_state'] ?>' data-zip='<?php echo $group['address_zip'] ?>' data-salestax='<?php echo $group['tax_rate'] ?>' data-default-salestax='' data-name='<?php echo $group['name_id'] ?>' ><?php echo $group['name_id'] ?></option>
            <?php endforeach ?>
            <option value='other' data-street='' data-city='' data-state='' data-zip='' data-salestax='' data-default-salestax='' data-name='' >Other</option>
        </select>
    </div>
<?php } ?>

<div class='form-group hidden'>
    <button class='btn btn-default btn-sm btn-block billaddress'>Deliver to my billing address</button>
</div>

<div class='form-group'>
    <label for='datedelivery'>Date of Service/Delivery</label>
    <input type="text" type="text" name='datedelivery' id='datedelivery' class="form-control datedelivery validate[required,funcCall[checkLEGAL]]" data-value="<?=date("m-d-Y", strtotime("+3 day"))?>">
    
    <p class='bg-warning messageShow'></p>

</div>

<div class='form-group'>
    <label for='deliver_firstname'>First Name</label>
    <input type='text' name='deliver_firstname' id='deliver_firstname' class='tmpdata form-control validate[required]' placeholder='First Name'/>
</div>

<div class='form-group'>
    <label for='deliver_lastname'>Last Name</label>
    <input type='text' name='deliver_lastname' id='deliver_lastname' class='tmpdata form-control validate[required]' placeholder='Last Name'/>
</div>

<div class='form-group'>
    <label for='deliver_email'>E-mail</label>
    <input type='text' class='tmpdata form-control validate[required,custom[email]]' name='deliver_email' id='deliver_email' placeholder='E-mail' />
</div>

<div class='form-group'>
    <label for='deliver_streetaddress'>Street Address</label>
    <input type='text' class='tmpdata form-control validate[required]' name='deliver_streetaddress' id='deliver_streetaddress' placeholder='Street Address'/>
</div>


<div class='form-group'>
    <label for='deliver_city'>City</label>
    <input type='text' class='tmpdata form-control validate[required]' name='deliver_city' id='deliver_city' placeholder='City'/>
</div>

<div class='form-group'>
    <label for='deliver_state'>State</label>
    <select class='tmpdata form-control validate[required]' name='deliver_state' id='deliver_state'>
        <?php foreach ($cfg['states'] as $key => $group): ?>
            <option value='<?php echo $group['code'] ?>'><?php echo $group['name'] ?></option>
        <?php endforeach ?>
    </select>
</div>

<div class='form-group'>
    <label for='deliver_zip'>Zip</label>
    <input type='text' class='tmpdata form-control validate[required,custom[onlyLetterNumber]]' name='deliver_zip' id='deliver_zip' placeholder='Zip'/>
</div>

<div class='form-group'>
    <label for='deliver_daytimephone'>Daytime Phone</label>
    <input type='text' class='tmpdata form-control validate[required]' name='deliver_daytimephone' id='deliver_daytimephone' placeholder='Daytime Phone'/>
</div>

<div class='form-group'>
    <label for='deliver_decedent'>Decedent's Name</label>
    <input type='text' class='form-control validate' name='deliver_decedent' id='deliver_decedent' placeholder='Decedent'/>
</div>

<div class='form-group'>
    <label for='deliver_message'>Message To Appear On Card</label>
    <textarea id='deliver_message' class='form-control' name='deliver_message' placeholder='Enter Message To Appear On Card'></textarea>
</div>

<div class='form-group'>
    <label for='deliver_instructions'>Special Instructions</label>
    <textarea id='deliver_instructions' class='form-control' name='deliver_instructions' placeholder='Enter Special Instructions'></textarea>
</div>
