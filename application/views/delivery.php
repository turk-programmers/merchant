
<script>
$(function() {
    <?php
    if (@$prev_form) {
        ?>
        $.mc.prev_form = '<?= $prev_form ?>';
        <?php
    }
    ?>
    $.mc.next_form = '<?= $next_form ?>';
});

var dateFormat = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.]((?:19|20)\d\d)$/
//var dateFormat = /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.]((?:19|20)\d\d)$/;

function checkLEGAL(field, rules, i, options){

    var match = field.val().match(dateFormat)
    
    if (!match) return "* Invalid date";
    var bd=new Date(match[3], match[2]-1, match[1]);
    
    var diff = Math.floor((new Date).getTime() - bd.getTime());
    var day = 1000* 60 * 60 * 24;

    var days = Math.floor(diff/day);
    var months = Math.floor(days/31);
    var years = Math.floor(months/12);
}
</script>

<div id="merchandise_wrapper">

    <?php $this->view('static-views/breadcrumb', array('shops' => $shops)); ?>
    <?php $this->view('static-views/temporary_off_message'); ?>
    <?php $this->view('delivery/page-paragraph'); ?>
    <?php $this->view('delivery/page-title'); ?>
    
    <div class='container-delivery'>
        <div class='row'>
            <form role="form" action="" method="post" id='mainform'>
                <input type="hidden" name="scriptaction"    id="scriptaction"   value="validate" />
                <input type="hidden" name="next_form"       id="next_form"      value="<?= $next_form ?>" />
                <?php $this->load->view('delivery/form-wrapper'); ?>
                <?php $this->load->view('cart/summary_wrapper'); ?>
            </form>
        </div>
    </div>
    <div class='row col-lg-6 pull-right'>
        <a class='merchant-btn btn-back' href="#">Return to Billing</a>
        <?php
        if(@$next_service_date !== false) {
            ?>
            <a class='merchant-btn-revert btn-next' style='display: none' href="#" >Continue</a>
            <?php
        }
        ?>
    </div>
</div>
<?php
$message = $shops['same_day_message'];
?>
<script>var tmpdata = {};</script>
<script src="<?=ASSETPATH?>/js/delivery.js<?=TAILSTRING?>"></script>
<script>
$(function() {
    $('.billaddress').click(function() {
        //return false;
        //$('#mainform').clearForm({callback:function(){ $('.messageShow').html('').parent().removeClass('has-warning'); }});
        $("#mainform").jsonToElement({
            json: <?= json_encode($user_session['billing']['autopopulate']) ?>,
            show: true
        });
        return false;
    });
    <?php 
    if (!empty($user_session['delivery'])) {
        $deliverydate = date('m-d-Y',strtotime(str_replace("-","/",$user_session['delivery']['datedelivery'])));
        $user_session['delivery']['datedelivery'] = $deliverydate;
        ?>
        tmpdata = <?= json_encode($user_session['delivery']) ?>;
        <?php
        if($user_session['delivery']['deliverto'] != 'other'){ 
            ?>
            $("#mainform").jsonToElement({
                json: <?= json_encode($user_session['delivery']) ?>,
            });
            $('.tmpdata').each(function(){
                $(this).parent().hide();
            });
            <?php 
        } else {
            ?>
            $("#mainform").jsonToElement({
                json: <?= json_encode($user_session['delivery']) ?>,
                show: true
            });
            <?php 
        }
        ?>
        $('.btn-next').show();
        <?php 
    } 
    ?>

    var temporary_off_dates = <?=json_encode(@$temporary_off_dates)?>;
    var permanent_off_dates = <?=json_encode(@$permanent_off_dates)?>;
    $('#datedelivery').datepicker({
        format: 'mm-dd-yyyy',
        startDate: "<?= @$next_service_date ? $next_service_date : $now?>",
        beforeShowDay: function(date){
            date.setHours(8);
            date.setMinutes(0);
            date.setSeconds(0);
            var d = [];
            d.push(date.getFullYear());
            d.push(("0"+(date.getMonth()+1)).substr(-2));
            d.push(("0"+date.getDate()).substr(-2));
            var dString = d.join('-');
            // For temporary OFF date
            // if(temporary_off_dates.indexOf(dString) >= 0) {
            //     return false;
            // }
            for(var closeDate in temporary_off_dates) {
                closeDate = new Date(temporary_off_dates[closeDate]);
                if(
                    date.getFullYear() == closeDate.getFullYear() &&
                    date.getMonth() == closeDate.getMonth() &&
                    date.getDate() == closeDate.getDate()
                    ) {
                    return false;
                }
            }
            // For permanent OFF date
            for(var closeDate in permanent_off_dates) {
                closeDate = new Date(permanent_off_dates[closeDate]);
                if(date >= closeDate) {
                    return false;
                }
            }
            return true;
        },
    }).on('changeDate', function(ev) {
        $('.datepicker').hide();
        $('#datedelivery').validationEngine('hide');
        var selectedDate = new Date(ev.date);
        var today = new Date('<?= $now ?>');
        // if (today.valueOf() == selectedDate.valueOf()) {
        //     $('.messageShow').html('<?= $message ?>').parent().addClass('has-warning');
        //     $('.btn-next').hide();
        // } else {
            $('.messageShow').html('').parent().removeClass('has-warning');
            $('.btn-next').show();
        // }
    });
});
</script>
