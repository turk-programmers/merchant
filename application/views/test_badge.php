<div class="test_badge <?=@$this->settings['testmode'] ? "testmode" : ""?>">    
	<div class="topleft">
		<span>
			<div><a target="_blank" href="<?=$url?>/print_session">Session</a></div>
		</span>
		<span>|</span>
		<span>
			<div><a target="_blank" href="<?=$url?>/debug">Debug</a></div>
		</span>
		<span>|</span>
		<span>
			<div><a href="#" onclick="propulateForm(1); return false;">Populate</a></div>
			<div><a href="#" onclick="propulateForm(2); return false;">Populate2</a></div>
		</span>
	</div>
	<div class="topright">
		<span>
			<div><a href="<?=$url?>/clear_session">Clear Session</a></div>
			<div><a target="_blank" href="<?=$url?>/clear_cache">Clear Cache</a></div>
		</span>
	</div>
	<div class="bottomleft"><?=ucfirst(@$user_session['pkgtype'])?></div>
	<div class="bottomright">
		{elapsed_time} sec.
	</div>
	<?php
	if(@$this->settings['testmode']) {
		?>
		<span class="test-mode-text">Test Mode</span>
		<?php
	} elseif(DEVSTATE) {
		?>
		<span class="test-mode-text">Dev Tool</span>
		<?php
	}
	?>
	
	<style>
	@keyframes blink {
		0% { opacity: 1; }
		50% { opacity: 1; }
		50.01% { opacity: 0; }
		100% { opacity: 0; }
	}
		
	.test_badge {
	    background-color: #BF0000;
	    border: 5px solid #BF0000;
	    color: #FFFFFF;
	    font-family: verdana;
	    font-size: 35px;
	    font-weight: bold;
	    padding: 35px 30px 25px;
	    position: fixed;
	    right: 0;
	    z-index: 9999999;
	}
	.test_badge > div {
	    font-size: 10px;
	    position: absolute;
	}
	.test_badge > div.topleft {
	    left: 0;
	    top: 0;
	}
	.test_badge > div.topright {
	    right: 0;
	    top: 0;
	}
	.test_badge > div.bottomleft {
	    left: 0;
	    bottom: 0;
	}
	.test_badge > div.bottomright {
	    right: 0;
	    bottom: 0;
	}
	.test_badge > div > span {
	    float: left;
	    margin: 0 1px;
	    min-height: 30px;
	}
	.test_badge > div.topleft > span > div{
	    text-align: left;
	}
	.test_badge > div.topright > span > div{
	    text-align: right;
	}
	.test_badge > div  a:link,
	.test_badge > div  a:visited {
	    color: inherit;
	    text-decoration: none;
	}
	.test_badge > div  a:hover,
	.test_badge > div  a:active {
	    text-decoration: underline;
	}
	.test_badge.testmode > span.test-mode-text {
		animation: blink .75s linear infinite;
	}
	pre {
	    background-color: #EEEEEE;
	    border: 1px solid #CCCCCC;
	    border-radius: 5px;
	    font-size: 13px;
	    margin: 10px;
	    padding: 10px;
	    white-space: pre-wrap;
	    word-wrap: break-word;
	}
	@media (max-width: <?=@$breakpoint['sm-max']?>px){
		.test_badge{
			display:none;
		}
	}
	@media print{
		.test_badge{
			display:none;
		}
	}
	</style>
	<script>
	$(function(){
		$('body').prepend($('.test_badge'));
	});
	</script>
	<script src="<?=ASSETPATH?>/js/test_badge.js<?=TAILSTRING?>"></script>
</div>
<script>var is_dev = 1;</script>
<script src="https://secure.funeralnet.com/js/populateform.js"></script>