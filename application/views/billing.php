<script>
$(function(){
    <?php
	if(@$prev_form){
		?>
		$.mc.prev_form = '<?=$prev_form?>';
		<?php
	}
    ?>
    $.mc.next_form = '<?=$next_form?>';
});
</script>

<div id="merchandise_wrapper">

    <?php $this->view('static-views/breadcrumb', array('shops' => $shops)); ?>
    <?php $this->view('static-views/temporary_off_message'); ?>
    <?php $this->view('billing/page-paragraph'); ?>
    <?php $this->view('billing/page-title'); ?>

    <div class='container-billing'>
        <form role="form" action="" method="post" id='mainform'>
            <input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
            <input type="hidden" name="next_form" 		id="next_form"		value="<?=$next_form?>" />
            <div class='row'>
                <?php $this->view('billing/form-wrapper'); ?>
                <?php $this->load->view('cart/summary_wrapper'); ?>
            </div>
        </form>
        <div class='contain-step'>
            <div class='pull-right'>
                <a class='merchant-btn btn-back' href="#">Return to Cart</a>
                <a class='merchant-btn-revert btn-next' href="#">Continue</a>
            </div>
        </div>
    </div>
</div>
<script src="<?=ASSETPATH?>/js/billing.js<?=TAILSTRING?>"></script>
<script>
$(function(){
    <?php if (!empty($user_session['billing'])) { ?>
    $("#mainform").jsonToElement({
        json: <?=json_encode($user_session['billing'])?>
    });
    <?php } ?>
});
</script>
