<span class=''>You have <?php echo $this->cart->total_items() ?> item(s)</span>
<div class='row'>
    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'></div>
    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>Price</div>
    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>Total</div>
</div>
<hr>
<?php
foreach((array)$this->cart->contents() as $id=>$prod){
    $choice = "";
    ?>
    <div class='row'>
        <?php
        if(@$prod['options']['advance_price']) {
            $choice = " (".@$prod['options']['advance_price']['prices'][@$prod['options']['selected_price']]['label'].")";
        } elseif(@$prod['options']['price2'] > 0 or @$prod['options']['price3'] > 0) {
            $choice = " (".@$settings['shop_config']['priceLabel'][$prod['selected_price']].")";
        }
        ?>
        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
            <?php
            echo $prod['qty'].' x '.$prod['name'] . @$choice;
            if(count($prod['selected_options'])) {
                foreach((array)$prod['selected_options'] as $option) {
                    echo "<br>".@$option['label'].": ".@$option['value'];
                }
            }
            ?>
        </div>
        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>$<?php echo number_format($prod['price'],2);?></div>
        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'><i>$<?php echo number_format($prod['subtotal'],2);?></i></div>
    </div>
    <?php
}
?>
<div class='row'>
    <div class='col-lg-9 col-md-9 col-sm-9 col-xs-9'>Subtotal </div>
    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'><b><i><u>$<?php echo number_format($this->cart->total(), 2) ?></u></i></b></div>
</div>