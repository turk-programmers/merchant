<link rel="stylesheet" media="screen" type="text/css" href="<?php echo DEFAULT_FRONT_FOLDER;?>css/pure-min.css" />
<style>
.row {
    zoom: 1;
    margin-left: 20px;
    margin-top: 5px;
}
.input-group-addon{

    text-align: right;
}
</style>
<script>
$(function(){
    <?php
	if(@$prev_form){
		?>
		$.mc.prev_form = '<?=$prev_form?>';
		<?php
	}
    ?>
    $.mc.next_form = '<?=$next_form?>';    
});    
</script>
<?php eval('$default_flower_package_name = '.$shops['package_name'].';'); ?>
<div id="merchandise_wrapper">
    <div id="breadcrumb">
            <a href="<?=$settings['base_url']?>">Home</a> / <a href="<?=$settings['base_url']?>/cart">Cart</a> / Billing Information
    </div>
    <div class='row'>
       <div class='col-lg-12'>
            <p>Substitutions of an arrangement and container of similar value may be made in the unlikely event that your selection is unavailable. </p>

            <p>Please allow 24 hours for delivery. Orders placed after 4pm on Fri. will not be available for delivery until the following Monday after 11 am.</p>
        </div> 
    </div>
            
    <div class='container'>
        <form role="form" action="" method="post" id='mainform'>
        <input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
        <input type="hidden" name="next_form" 		id="next_form"		value="<?=$next_form?>" />
        <div class='row'>
            
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-10">
                
                    <legend>Billing Information</legend>
                    <div class='form-group'>
                        <label for='firstname'>First Name</label>
                        <input type='text' name='firstname' id='firstname' class='form-control validate[required]' placeholder='First Name'/>
                    </div>
                    
                    <div class='form-group'>
                        <label for='lastname'>Last Name</label>
                        <input type='text' name='lastname' id='lastname' class='form-control validate[required]' placeholder='Last Name'/>
                    </div>
                    
                    <div class='form-group'>
                        <label for='email'>E-mail</label>
                        <input type='text' class='form-control validate[required,custom[email]]' name='email' id='email' placeholder='E-mail' data-value='kapom@frazerconsultants.com' />
                    </div>
                    
                    <div class='form-group'>
                        <label for='streetaddress'>Street Address</label>
                        <input type='text' class='form-control validate[required]' name='streetaddress' id='streetaddress' placeholder='Street Address'/>
                    </div>
                    
                    <div class='form-group'>
                        <label for='city'>City</label>
                        <input type='text' class='form-control validate[required]' name='city' id='city' placeholder='City'/>
                    </div>
                    
                    <div class='form-group'>
                        <label for='state'>State</label>                        
                        <select class='form-control validate[required]' name='state'>
                            <?php foreach($cfg['states'] as $key=>$group ): ?>
                            <option value='<?php echo $group['code'] ?>'><?php echo $group['name'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    
                    <div class='form-group'>
                        <label for='zip'>Zip</label>
                        <input type='text' class='form-control validate[required,custom[onlyLetterNumber]]' name='zip' id='zip' placeholder='Zip'/>
                    </div>                    
                                                            
                    <div class='form-group'>
                        <label for='daytimephone'>Daytime Phone</label>
                        <input type='text' class='form-control validate[required]' name='daytimephone' id='daytimephone' placeholder='Daytime Phone'/>
                    </div>
                    <?php if($shops['show_decedent']==1) { ?>
                    <div class='form-group'>
                        <label for='decedent'>Decedent</label>
                        <input type='text' class='form-control ' name='decedent' id='decedent' placeholder='Decedent'/>
                    </div>
                    <?php } ?>
                    
                    <?php if($shops['show_card']==1) { ?>
                    <div class='form-group'>
                        <label for='message'>Message To Appear On Card</label>                        
                        <textarea id='message' name='message' class='form-control' placeholder='Enter Message To Appear On Card'></textarea>
                    </div>                   
                    <?php } ?>
                
            </div>
            
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-10">
                <legend>Order Summary</legend>
                <span class=''>You have <?php echo $this->cart->total_items() ?> item(s)</span>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'></div>
                    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>price</div>
                    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>total</div>
                </div>
                <hr>
                <?php
                foreach($this->session->userdata('cart') as $id=>$prod){
                    ?>
                    <div class='row'>
                        <?php 
                            if(strrpos($prod['product_row'],'_')){ // has underscore it item is group
                                $choice = '('.$default_flower_package_name[$prod['selected_price']].')';
                            } else {
                                $choice = "";
                            }
                        ?>
                        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'><?php echo $prod['qty'].' x '.$prod['name'] .' '.$choice;?>
                        </div>
                        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>$<?php echo number_format($prod['price']);?></div>
                        <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'><i>$<?php echo number_format($prod['subtotal']);?></i></div>
                    </div>
                    <?php
                }
                ?>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>Total </div>
                    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'></div>
                    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>$<?php echo number_format($this->cart->total()) ?></div>
                </div>
            </div>
        </div>
        
        
        
        </form>
    </div>
    <div class='row col-lg-4 pull-right'>            
        <a class='btn btn-default btn-back' href="#">Return to Cart</a>                        
        <a class='btn btn-success btn-next' href="#">Continue</a>
    </div> 
</div>

<script>
$(function(){

    $("#mainform").validationEngine('detach').validationEngine({promptPosition : "topRight",scrollOffset:100});
    
    $('.btn-back').click(function(e){
        e.preventDefault();
        $.mc.goBack();
    });
    $('.btn-next').click(function(e){
        e.preventDefault();
        $.mc.goNext();
    });
    $('.btn-print').click(function(e){
        e.preventDefault();        
        $(this).hide();
        window.print();
        $(this).show();
    });
    
    <?php if (!empty($user_session['billing'])) { ?>
    $("#mainform").jsonToElement({        
        json: <?=json_encode($user_session['billing'])?>        
    });
    <?php } ?>
});
</script>
    