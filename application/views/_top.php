
<?php

function get_wrapper_cache($wrapper_link) {
    $wrapper = '';

    // Get wrapper
    $cache_filename = 'application/cache/template.html';
    $cache_time = 24 * 3600; // Time in seconds to keep a page cached
    // Check Cache
    $cache_created = (file_exists($cache_filename)) ? filemtime($cache_filename) : 0;
    // If cache valid, get it
    if ((time() - $cache_created) < $cache_time) {
        //readfile($cache_filename);
        $wrapper = file_get_contents($cache_filename);
    }
    // If no cache, get wrapper and cache it
    if (empty($wrapper) or is_dev()) {
        $wrapper = file_get_contents($wrapper_link);

        file_put_contents($cache_filename, $wrapper);        
    }
    return $wrapper;
}

$template_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.@$settings['template_path'];
if(preg_match("/^(http|https):\/\//i", @$settings['template_path'])) {
    $template_url = @$settings['template_path'];
}
$wrapper = get_wrapper_cache($template_url);

$content_pos = strpos($wrapper, '<!--merchant-->');
$wrapper_top = substr($wrapper, 0, $content_pos);
define('WRAPPER_BOTTOM', substr($wrapper, $content_pos + 15));

echo $wrapper_top;

?>