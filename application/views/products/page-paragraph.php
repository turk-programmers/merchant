<p>
	Substitutions of an arrangement and container of similar value may be made in the unlikely event that your selection is unavailable.<br>
	<br>
	Please allow 24 hours for delivery. Orders placed after 4pm on Fri. will not be available for delivery until the following Monday after 11 am.
</p>