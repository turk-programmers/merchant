<div class='form-group'>
    <label for='ccmonth'>Expiry date MM/YY</label>
    <br>
    <select class='form-control validate[required]' name='ccmonth' style="display: inline-block; width: auto;">
        <option value='01' <?php echo date('m') == '01' ? 'selected' : '' ?>>01</option>
        <option value='02' <?php echo date('m') == '02' ? 'selected' : '' ?>>02</option>
        <option value='03' <?php echo date('m') == '03' ? 'selected' : '' ?>>03</option>
        <option value='04' <?php echo date('m') == '04' ? 'selected' : '' ?>>04</option>
        <option value='05' <?php echo date('m') == '05' ? 'selected' : '' ?>>05</option>
        <option value='06' <?php echo date('m') == '06' ? 'selected' : '' ?>>06</option>
        <option value='07' <?php echo date('m') == '07' ? 'selected' : '' ?>>07</option>
        <option value='08' <?php echo date('m') == '08' ? 'selected' : '' ?>>08</option>
        <option value='09' <?php echo date('m') == '09' ? 'selected' : '' ?>>09</option>
        <option value='10' <?php echo date('m') == '10' ? 'selected' : '' ?>>10</option>
        <option value='11' <?php echo date('m') == '11' ? 'selected' : '' ?>>11</option>
        <option value='12' <?php echo date('m') == '12' ? 'selected' : '' ?>>12</option>
    </select>
    <select class='form-control validate[required]' name='ccyear' style="display: inline-block; width: auto;">
        <?php for ($i = date("Y"); $i <= (date("Y") + 10); $i++) { ?>
            <option value="<?php echo substr($i,2,2) ?>"><?php echo $i ?></option>
        <?php } ?>
    </select>
    <div class="clearfix"></div>
</div>