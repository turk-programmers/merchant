<div class="col-lg-6 col-md-6 col-sm-6 col-xs-11 page-wrapper">
    <?php
    if($shops['payment_gateway']=='Paypal') {
        ?>
        <img src="<?=ASSETPATH?>/graphics/paypal.png" class='img-responsive btn-next'>
        <?php
    } else {
        ?>
        <legend>Credit Card Information</legend>
        <?php $this->load->view('payment/form-fields'); ?>
        <?php
    }
    ?>
</div>