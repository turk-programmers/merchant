<div class="creditcard-logo">
    <img src="<?=ASSETPATH?>/graphics/ccicon-visa.png" data-type="visa" data-gray="<?=ASSETPATH?>/graphics/ccicon-visa-gray.png">
    <img src="<?=ASSETPATH?>/graphics/ccicon-mastercard.png" data-type="mastercard" data-gray="<?=ASSETPATH?>/graphics/ccicon-mastercard-gray.png">
    <img src="<?=ASSETPATH?>/graphics/ccicon-discover.png" data-type="discover" data-gray="<?=ASSETPATH?>/graphics/ccicon-discover-gray.png">
   <img src="<?=ASSETPATH?>/graphics/ccicon-amex.png" data-type="amex" data-gray="<?=ASSETPATH?>/graphics/ccicon-amex-gray.png">
</div>
<div class='form-group'>
    <label for='ccname'>Name on card</label>
    <input type='text' name='ccname' id='ccname' data-value='FuneralNet' class='form-control validate[required]' placeholder='Name on card'/>
</div>

<div class='form-group'>
    <label for='ccnumber'>Card Number</label>
    <input type='text' name='ccnumber' id='ccnumber' data-value='4111111111111111' class='creditcard form-control validate[required]' placeholder='Card Number'/>
</div>

<?php $this->load->view('payment/field_expire_date'); ?> 

<div class='form-group'>
    <label for='cccode'>CVV</label>
    <input type='text' class='ccv form-control validate[required]' name='cccode' id='cccode' placeholder='Code' data-value='111' />
</div>