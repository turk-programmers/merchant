<center>
	<div id="content-flowers">

		<p align="left"><b>Sandbox Funeral Home</b> has developed an exclusive arrangement with a local,
			well established florist to simplify ordering floral tributes for services at our funeral homes.
			Immediate family members, relatives and friends can order floral tributes for a private viewing,
			wake, funeral, visitation or memorial service right online.</p>

		<p align="left">By placing the order through our Web site, you no longer have to worry about
			giving the florist the information about the service and delivery times.</p>

		<p align="left">We will handle all the details and insure that your arrangement arrives fresh,
			on time and looking beautiful. And the cost is exactly the same as if you ordered
			the flowers in person directly from the florist.</p>

		<br><br>

		<h3 style="text-align: left;">&nbsp;Please make a selection from the following categories:</h3>

		<table border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td valign="top"><br><br><img src="<?=DEFAULT_FRONT_IMAGE?>summary.gif" alt="" width="240" height="350" border="0"></td>
					<td align="left" valign="top" style="line-height: 1.5em;"><br><br><br><br>
					<?php
					foreach ( $subcategories as $category_name ) {
						print '<h4>&raquo; <a href="'.$category_name['link'].'">'.$category_name['label'].'</a></h4>';
					}
					?>
					</td>
				</tr>
			</tbody>
		</table>

	</div>

</center>