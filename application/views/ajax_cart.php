
<?php
$index = 1;
foreach ($this->cart->contents() as $pid => $prod_arr) {

    $index ++;
}
?>
<div id="merchandise_wrapper"><div class="heading">
        <a><span id="cart-total"><?= $show_quantity = ($this->cart->total_items() == 0) ? '0' : $this->cart->total_items(); ?> item(s) - <?php echo "$" . number_format($this->cart->total(), 2) ?></span><i></i></a>
    </div><div class="content">
        <div class="cart-wrapper">
            <div class="mini-cart-info">
                <table>
                    <tbody>
                        <?php foreach ($this->cart->contents() as $pid => $prod_arr) { ?>
                        <?php 
                            $prod['name'] = $prod_arr['name'];
                            $prod['price'] = $prod_arr['price'];
                            $prod['quantity'] = $prod_arr['qty'];
                            $prod['totalprice'] = number_format($prod_arr['price'] * $prod_arr['qty'], 2);

                            $choice = '(' . $settings['shop_config']['priceLabel'][$prod_arr['options']['selected_price']] . ')';
                        ?>
                        <tr>
                            <td class="image" width="50px;">            
                                <img class='img-responsive' style='width: 50px;height:50px;' src="https://fnetclients.s3.amazonaws.com/merchant/clients/<?=$shops['client_id']?>/pictures/<?=$prod_arr['options']['thumb']?>" alt="<?php echo $prod_arr['options']['product_name']; ?>">   
                            </td>
                            <td class="name hidden-xs">
                                <div class="product_name"><?php echo $prod['name'] . $choice; ?></div>
                            </td>
                            <td class="quantity">x<?php echo $prod['quantity'] ?>&nbsp;</td>
                            <td class="total"><?php echo $prod['totalprice']; ?></td>
                            <td class="remove"><i onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&amp;remove=272::' : $('#cart').load('index.php?route=module/cart&amp;remove=272::' + ' #cart &gt; *');"></i></td>
                        </tr>
                        <?php } ?>
                        
                    </tbody></table>
            </div>
            <div class="mini-cart-total">
                <table>
                    <tbody>
                        <tr>                            
                            <td class=""><b>Summary Total:</b></td>
                            <td class="">$<?php echo number_format($this->cart->total(), 2) ?></td>
                        </tr>                       
                    </tbody></table>
            </div>
            <div class="checkout">
                
                <form action='/<?php echo $shops['name']?>/cart' method='post'>
                    <a href='/<?php echo $shops['name']?>/cart' class='merchant-btn <?php echo $this->cart->total_items() == 0? 'noitems' : '' ?>'>View Cart</a>                         
                    <button class='merchant-btn-revert <?php echo $this->cart->total_items() == 0? 'noitems' : '' ?>'>Checkout</button>
                    <input type='hidden' name='action' value='check-out'/>
                </form>
            </div>
        </div>
    </div></div>
<script>
    $('.noitems').click(function(){
       alert("It is required to select at least one product");
       return false;
    });
</script>