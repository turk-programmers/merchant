
<style>
.delete_button {
    background-image: url(<?php echo DEFAULT_FRONT_IMAGE; ?>remove.png);
}
</style>

<div id="merchandise_wrapper" class='cart-container'>

    <?php $this->view('static-views/breadcrumb', array('shops' => $shops)); ?>
    <?php $this->view('static-views/temporary_off_message'); ?>
    <?php $this->view('cart/page-title'); ?>

    <div id="cart_header">
        <span class='cart_qty pull-right'><?php echo '<p>' . $this->cart->total_items() . ' item(s) to buy now</p>' ?></span>
    </div>
    <table class='table table-striped table-bordered'>
        <thead>
            <tr>
                <td class='hidden-xs'>#</td>
                <td>Name</td>

                <td>Quantity</td>
                <td class='hidden-xs'>Unit Price</td>
                <td>Total</td>

            </tr>
        </thead>

        <?php $this->view('cart/lists') ?>

        <tfoot>
            <tr class="hidden-xs">
                <td colspan='4'>Summary Total</td>
                <td align='right'>$<span class='sumtotal'><?php echo number_format($this->cart->total(), 2) ?></span></td>
            </tr>
            <tr class="visible-xs">
                <td colspan='2'>Summary Total</td>
                <td align='right'>$<span class='sumtotal'><?php echo number_format($this->cart->total(), 2) ?></span></td>
            </tr>
            <tr>
                <td colspan='5' align='right'>

                    <form action='' method='post'>
                        <!-- <a class='merchant-btn previous' href="<?php echo $_SERVER["HTTP_REFERER"] ?>">Continue Shopping</a> -->
                        <a class='merchant-btn previous' href="/<?=SHOPNAME?>/products">Continue Shopping</a>
                        <button class='merchant-btn-revert'>Checkout</button>
                        <input type='hidden' name='action' value='check-out'/>
                    </form>
                </td>
            </tr>
        </tfoot>

    </table>
</div>

<script>
    var shopname = "<?php echo $shops['name'] ?>";
</script>
<script src="<?=ASSETPATH?>/js/cart.js<?=TAILSTRING?>"></script>