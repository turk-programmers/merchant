<link rel="stylesheet" href="<?=ASSETPATH?>/css/products.css" />
<style>

    #merchandise_wrapper #breadcrumb #cart span.left {
        background-image: url(<?php echo DEFAULT_FRONT_IMAGE; ?>qty-left.jpg);
    }
    #merchandise_wrapper #breadcrumb #cart span.right {
        background-image: url(<?php echo DEFAULT_FRONT_IMAGE; ?>qty-right.jpg);
    }

</style>
<div id="alertmsg1" class="alert alert-success flyover flyover-top"><h4>Success</h4><p> You have added product to your shopping cart!</p></div>
<div id="merchandise_wrapper">

    <?php $this->load->view('breadcrumb', array('shops' => $shops)); ?> 

    <p>Substitutions of an arrangement and container of similar value may be made in the unlikely event that your selection is unavailable. <br><br>Please allow 24 hours for delivery. Orders placed after 4pm on Fri. will not be available for delivery until the following Monday after 11 am.</p>

    <h3 style="margin-bottom:25px; text-transform:capitalize;">&nbsp;<?php echo $req_sub_category; ?></h3>
    <?php
        $section = array();
    ?>
    <div class='b-bread'>
        <div class="b-bread__btn">
            <?php            
            foreach ($subcategories['subcategory_list'] as $arr) {
                if(!in_array($arr['section'],$section)){
                    array_push($section,$arr['section']);
                }
                $subcatbttclass = ($arr['name'] == $req_sub_category) ? 'b-bread__btn_active' : 'b-bread__btn_inactive';
                ?>
                <div class="b-bread__sub_level_1" data-tab='<?php echo $arr['section']; ?>' data-level='1' data-section='<?php echo $arr['section']; ?>'>
                    <div class="b-bread__btn <?php echo $subcatbttclass; ?>">
                        <div class="b-bread__label <?php echo $subcatbttclass; ?>"><?php echo $arr['label'] . " (" . $arr['num_products'] . " items)"; ?></div>
                    </div>
                </div>
            <?php } ?>
            
        </div>                
    </div>
    <div class='pull-right'>
    <button style='margin-right: 5px; font-size: 25px' class='btn btn-default btn-grid glyphicon glyphicon-th'></button>
    <button style='margin-right: 5px; font-size: 25px' class='btn btn-default btn-list glyphicon glyphicon-th-list'></button>
    </div>
    <div class='clearfix'></div>
    <?php foreach($section as $item) { ?>
        <div class='b-bread'>
            <div class="b-bread__btn">
                <div class='b-bread__sub b-bread__sub_level_2 <?php echo $item ?>' data-section='<?php echo $item ?>'>
                    <?php foreach ($subcategories['subcategory_list_lv2'] as $arr) { 
                            if($item == $arr['section']){?>
                                <div class="b-bread__btn" data-section='<?php echo $arr['name'] ?>' >
                                    <div class="b-bread__label"><?php echo $arr['label'] ?></div>
                                </div>
                            <?php } ?>
                        
                    <?php } ?>
                    
                </div>
            </div> 
        </div>
    <?php } ?>
    <div class='clearfix'></div>
    <div id='listproduct'>
        <div id="grid">
            
        <?php foreach ($subcategories['product_list'] as $index => $arr) {?>       
            <div class='item product grid' data-groups='["<?php echo $arr['category_name'] ?>" , "<?php echo $arr['parent'] ?>"]'>
                <div class='row'> 
                    <div class='col-lg-12 col-xs-12 form-group'>                                   
                        <a class="mfp-image image-link" href="https://fnetclients.s3.amazonaws.com/merchant/clients/<?=$shops['client_id']?>/pictures/<?=$arr['thumb']?>" data-mfp-src="https://fnetclients.s3.amazonaws.com/merchant/clients/<?=$shops['client_id']?>/pictures/<?=$arr['image']?>">
                            <img class='img-responsive' src="https://fnetclients.s3.amazonaws.com/merchant/clients/<?=$shops['client_id']?>/pictures/<?=$arr['image']?>" alt="<?php echo $arr['product_name']; ?>">   
                        </a>                       
                    </div>                          


                    <div class='col-lg-12 col-xs-12 form-group'>
                        <?php if (isset($arr['shortdesc'])) echo "<h5>{$arr['shortdesc']}</h5>" ?>
                        <?php if (isset($arr['product_name'])) echo "<p class='name'>{$arr['product_name']}</p>" ?>
                        <?php if (isset($arr['longdesc'])) echo "<p class='description'>{$arr['longdesc']}</p>" ?>	
                        <?php //eval('$default_flower_package_name = ' . $settings['shop_config']['priceLabel'] . ';'); ?>

                        <?php if (($arr['price'] != '0.00') && ($arr['price2'] != '0.00') && ($arr['price3'] != '0.00')) { ?>
                            <form action="" method="post">

                                <div class='product_name'>
                                    <?php if ($shops['show_price'] == '1') { ?>                                    
                                        <select name='product_price'>
                                            <option value='price'><?php echo $settings['shop_config']['priceLabel']['price']. ' $' . $arr['price'];  ?></option>
                                            <option value='price2'><?php echo $settings['shop_config']['priceLabel']['price2']. ' $' . $arr['price2'];  ?></option>
                                            <option value='price3'><?php echo $settings['shop_config']['priceLabel']['price3']. ' $' . $arr['price3'];  ?></option>
                                        </select>
                                    <?php }?>

                                    <?php if ($shops['ecommerce'] == 1) { ?>                                                                                                            
                                        <input type="hidden" name="product_id" value="<?php echo $arr['product_id']; ?>">                                    
                                    <?php } ?>

                                    <?php if ($shops['ecommerce'] == 1) { ?>

                                        <button type="submit" value='Add to Cart' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-shopping-cart"></i> Add
                                        </button>

                                    <?php } ?>
                                </div> 

                            </form>                        

                        <?php } elseif (($arr['price'] != '0.00') && ($arr['price2'] != '0.00')) { ?>
                            <form action="" method="post">
                                <div class='product_name'>
                                    <?php if ($shops['show_price'] == '1') { ?>                                    
                                        <select name='product_price'>
                                            <option value='price'><?php echo $settings['shop_config']['priceLabel']['price']. ' $' . $arr['price'];  ?></option>
                                            <option value='price2'><?php echo $settings['shop_config']['priceLabel']['price2']. ' $' . $arr['price2'];  ?></option>                                        
                                        </select>
                                    <?php }?>

                                    <?php if ($shops['ecommerce'] == 1) { ?>                                                                                                            
                                        <input type="hidden" name="product_id" value="<?php echo $arr['product_id']; ?>">
                                    <?php } ?>

                                    <?php if ($shops['ecommerce'] == 1) { ?>

                                        <button type="submit" value='Add to Cart' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-shopping-cart"></i> Add
                                        </button>

                                    <?php } ?>
                                </div> 

                            </form>                        

                        <?php } elseif (($arr['price'] != '0.00') && ($arr['price2'] = '0.00') && ($arr['price3'] = '0.00')) { ?>
                            <form action="" method="post">
                                <div class='product_name'>
                                    <?php if ($shops['show_price'] == '1') { ?>                                    
                                        <select name='product_price'>
                                            <option value='price'><?php echo $settings['shop_config']['priceLabel']['price']. ' $' . $arr['price'];  ?></option>                                                                           
                                        </select>
                                    <?php }?>

                                    <?php if ($shops['ecommerce'] == 1) { ?>                                                                                                            
                                        <input type="hidden" name="product_id" value="<?php echo $arr['product_id']; ?>">
                                    <?php } ?>

                                    <?php if ($shops['ecommerce'] == 1) { ?>

                                        <button type="submit" value='Add to Cart' name="product_submit" id="cart_btt<?php echo $arr['product_id']; ?>" class="btn btn-success btn-sm btn-add">
                                            <i class="glyphicon glyphicon-shopping-cart"></i> Add
                                        </button>

                                    <?php } ?>
                                </div> 

                            </form>   

                        <?php } ?>                                                                                                                        
                    </div>
                </div>
            </div>
            <div class='clearfix'></div>
        <?php } ?>                    
        </div>
    </div>
    <?php if ($shops['ecommerce'] == 1) { ?>
        <div class='contain-step'>
            <div class='pull-right'>            
                <a class='btn btn-default previous' href="/<?php echo $shops['name'] ?>/products">Continue Shopping</a>                        
                <a class='btn btn-success next' href="/<?php echo $shops['name'] ?>/cart">Go to Cart</a>            
            </div> 
        </div>
    <?php } ?>
    <div id="divtmpflw"></div>
</div>
<script>

    var tmp = $.fn.tooltip.Constructor.prototype.show;
    $.fn.tooltip.Constructor.prototype.show = function () {
      tmp.call(this);
      if (this.options.callback) {
        this.options.callback();
      }
    }
    $(document).ready(function() {
        /* initialize shuffle plugin */
	var $grid = $('#grid');
        $grid.shuffle({
            itemSelector: '.item' // the selector for the items in the grid
        });
        
        $('#cart').popover({
            trigger: 'click',
            placement : 'bottom',            
        });
        var ajaxLoadcart;
        $('#cart').click(function(e){
            e.preventDefault();
            var ele = $(this);
            
            ajaxLoadcart = $.ajax({
                type: 'POST',               
                url: '/<?php echo $shops['name'] ?>/ajax_load_cart',
                dataType: "html",
                beforeSend: function() {
                    try {
                        ajaxLoadcart.abort();
                    } catch (Exception) {
                        //$('button[name=product_submit]').button('reset');

                    }
                },
                success: function(xhr){
                    console.log(xhr);
                    //document.getElementsByClassName("popover-content")[0].innerHTML = "SDFSDFSDFSDFSDFSDFSDFSDFSDF";
                    $('.popover-content').html(xhr);
                    //ele.popover({"html":true,content: });//.popover('show');
                    
                }
            });
        });

        $('.image-link').magnificPopup({type: 'image'});
               
       
       var ajaxAddToCart;
       $('button[name=product_submit]').click(function(e) {
           e.preventDefault();
           var form = $(this).parent().parent();
           var btn = $(this);
           $('.next , .previous').button('loading');
           //$('button[name=product_submit]').button('loading');
           ajaxAddToCart = $.ajax({
               type: 'POST',
               data: form.serialize(),
               url: '/<?php echo $shops['name'] ?>/ajax_add_to_cart/',
               beforeSend: function() {
                   try {
                       ajaxAddToCart.abort();
                   } catch (Exception) {
                       //$('button[name=product_submit]').button('reset');
                       $('.next , .previous').button('reset');
                   }
               },
               success: function(xhr) {
                   //$('button[name=product_submit]').button('reset');
                   $('.next , .previous').button('reset');
                   //btn.html('<i class="glyphicon glyphicon-refresh"></i> Update');
                   var jsonData = $.parseJSON(xhr);
                   if (!$('#alertmsg1').is('.in')) {
                       $('#alertmsg1').addClass('in');
                       setTimeout(function() {
                           $('#alertmsg1').removeClass('in');
                       }, 3200);
                   }
                   $('.cart_qty').html(jsonData['total_items']);
               }
           });
           return false;
       });

        $('.b-bread__sub_level_2 ').addClass('b-bread__btn_inactive');
        $('.b-bread__btn').click(function(event) {
            event.stopPropagation();

            // Inactive all children
            //$('.b-bread__btn', $(this)).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
            // Inactive all siblings
            $('.b-bread__btn', $(this).parent().parent()).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');

            // remove color
            $('.b-bread__label', $(this).parent().parent()).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');

            //focus
            $(this).removeClass('b-bread__btn_inactive').addClass('b-bread__btn_active');
            $('.b-bread__label', $(this)).removeClass('b-bread__btn_inactive').addClass('b-bread__btn_active');

            var ele = $(this);
            

            //Only chosed level 1
            if(ele.parent().data('level')=='1'){
                var groupName =ele.parent().data('section');
                $grid.shuffle('shuffle', groupName );
                $('.b-bread__sub_level_2').removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
                $('.b-bread__btn',$('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
                $('.b-bread__label',$('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
                $('.'+ele.parent().data('tab')).removeClass('b-bread__btn_inactive').addClass('b-bread__btn_active');
            } else {                
                var groupName =ele.data('section');
                $grid.shuffle({
                    itemSelector: '.item', // the selector for the items in the grid
                    
                });
                $grid.shuffle('shuffle', groupName );
            }
            
        });
        $('.btn-grid').click(function(){
            $('.list').removeClass('list').addClass('grid');                                    
            
            $('.b-bread__sub_level_2').removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
            $('.b-bread__btn',$('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
            $('.b-bread__label',$('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
            
            $('.grid').children('div').children('div:nth-child(1)')
                .attr('class','col-lg-12 col-xs-12 from-group');
        
            $('.grid').children('div').children('div:nth-child(2)')
                .attr('class','col-lg-12 col-xs-12 from-group');
            
            var groupName = $('.b-bread__sub_level_1').has(".b-bread__btn_active").data('section');
            $grid.shuffle('update', groupName );
        });

        $('.btn-list').click(function(){
            $('.grid').removeClass('grid').addClass('list');
            
            
            $('.b-bread__sub_level_2').removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
            $('.b-bread__btn',$('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
            $('.b-bread__label',$('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
            
            $('.list').children('div').children('div:nth-child(1)')
                .attr('class','col-lg-4 col-xs-4 form-group');

            $('.list').children('div').children('div:nth-child(2)')
                .attr('class','col-lg-8 col-xs-8 form-group').children('p');
                        
            var groupName = $('.b-bread__sub_level_1').has(".b-bread__btn_active").data('section');
            $grid.shuffle('update', groupName );
        });
    });

</script>