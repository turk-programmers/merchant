<?php

$cart_txt['itemtobuy'] = ($this->cart->total_items() > 0)? '<p>'.$this->cart->total_items().' items to buy now</p>' : '';

?>
<?php eval('$default_flower_package_name = '.$shops['package_name'].';'); ?>
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo DEFAULT_FRONT_FOLDER;?>css/pure-min.css" />
<style>
#cart_header h3 {}
#cart_header p {
	float: right;
	font-size: 16px;
	color: green;
}
.product_pic {
	max-width: 110px;
	max-height: 130px;
	border: solid 1px #AC8A5A;
}

.product_row {
	padding: 10px 0;
}
</style>

<div id="merchandise_wrapper">

	<?php $this->load->view('breadcrumb',array('shops'=>$shops)); ?> 


	<div id="cart_header">
		<?php echo $cart_txt['itemtobuy'] ;?>
		<h3>Shopping Cart</h3>
	</div>
	<?php

	if ( $this->cart->total_items() >0 ) {

	?>
        <table class='table table-striped table-bordered'>
            <thead>
                <tr>
                    <td class='hidden-xs'>#</td>
                    <td>Name</td>                    
                    <td class='hidden-xs'>Price</td>
                    <td>Quantity</td>
                    <td>Total Price</td>
                    <td>delete</td>
                </tr>
            </thead>
        
	<?php
                $index = 1;
		foreach ( $this->cart->contents() as $pid=>$prod_arr ) {

			$prod['name'] 		= $prod_arr['name'];
			$prod['price'] 		= $prod_arr['price'];
			$prod['quantity'] 	= $prod_arr['qty'];
			$prod['totalprice'] = number_format($prod_arr['price'] * $prod_arr['qty'], 2);
                        
                        
                        if(strrpos($prod_arr['product_row'],'_')){ // has underscore it item is group
                            $choice = '('.$default_flower_package_name[$prod_arr['options']['selected_price']].')';
                        } else {
                            $choice = "";
                        }
                        


	?>
            
            <tbody>
                <tr>
                    <td class='hidden-xs'><?=$index?></td>
                    <td>
                        
                        <a class='hidden-xs' class="mfp-image image-link" href="https://www.maxsass.com/flowers/graphics/CL13.jpg" data-mfp-src="https://www.maxsass.com/flowers/graphics/CL13.jpg">                                
                            <img class="product_pic" src="https://www.maxsass.com/flowers/graphics/CL13.jpg" alt="<?php echo $prod['name'];?>">
                        </a>
                        <div class="product_name"><?php echo $prod['name'] . $choice;?></div>
                    </td>
                    <td class='hidden-xs'><?php echo $prod['price'];?></td>
                    <td>
                        <form action='' method='post'>
                        <select name='qty' onchange="this.form.submit()">
                            <?php 
                            for($i=1;$i<=10;$i++){                                
                                ?>
                                    <option value='<?php echo $i ?>' <?php echo $prod['quantity']==$i ? 'selected':''  ?>><?php echo $i ?></option>
                                <?php
                            }
                            ?>
                        </select>
                            <input type='hidden' name='rowid' value='<?=$prod_arr['rowid']?>'/>
                            <input type='hidden' name='action' value='update'/>
                        </form>
                    </td>
                    <td><?php echo $prod['totalprice'];?></td>
                    <td>
                        <form action='' method='post'>
                            <button class='btn btn-danger center-block' name='remove' type='submit'><i class="glyphicon glyphicon-minus"></i> <span class='hidden-xs'>Remove</span></button>

                        <input type='hidden' name='rowid' value='<?=$prod_arr['rowid']?>'/>
                        <input type='hidden' name='action' value='remove'/>
                        </form>
                    </td>
                </tr>
            </tbody>
            
	<?php
                $index ++;
		}
	?>
            <tfoot>
                <tr>
                    <td colspan='5' class='hidden-xs'>Summary Total</td>
                    <td colspan='1'>$<?php echo number_format($this->cart->total(),2) ?></td>
                </tr>
                <tr>
                    <td colspan='6' align='right'>
                        <form action='' method='post'>
                            <a class='btn btn-default' href="/flowers/products">Return to Shopping</a>                        
                            <button class='btn btn-success'>Checkout</button>
                            <input type='hidden' name='action' value='check-out'/>
                        </form>
                    </td>
                </tr>
            </tfoot>
	
        </table>
	<?php	
	
	}
	else {
		print 'Your Shopping Cart is empty.';
	}


# SHOW CATEGORY LIST
#
#
#
#
#

?>
</div>

<script>
$(function(){
    $('button[name=remove]').click(function(){        
        if(confirm("Do you want to delete this flower ?")==false){
            return false;
        }        
    });
    $('.image-link').magnificPopup({type: 'image'});
}) ;   
</script>
