
<script>
$(function() {
    <?php
    if (@$prev_form) {
        ?>
        $.mc.prev_form = '<?= $prev_form ?>';
        <?php
    }
    ?>
    $.mc.next_form = '<?= $next_form ?>';
});
</script>
<div id="merchandise_wrapper">
    <?php $this->view('static-views/breadcrumb', array('shops' => $shops)); ?>
    <?php $this->view('static-views/temporary_off_message'); ?>
    <?php $this->view('payment/page-title'); ?>
    <?php
    if (@$error_message) {
        ?>
        <div class="errorMessage" style='color:red;'>
            <?php echo $error_message; ?>
        </div>
        <?php
    }
    ?>
    <div class='container-payment'>
        <div class='row'>
            <form role="form" action="" method="post" id='mainform'>
                <input type="hidden" name="scriptaction"    id="scriptaction"   value="validate" />
                <input type="hidden" name="next_form"       id="next_form"      value="<?= $next_form ?>" />
                <?php $this->load->view('payment/form-wrapper'); ?>
                <?php $this->load->view('cart/summary_wrapper'); ?>
                <?php $this->load->view('payment/summary'); ?>
            </form>
        </div>
    </div>
    <?php $this->load->view('payment/operate_buttons'); ?>
</div>
<?php $this->load->view('payment/assets_include'); ?>