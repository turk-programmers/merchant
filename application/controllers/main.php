<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

define('DEFAULT_FRONT_FOLDER', '//www.funeralnet.com/mfscommon/programs/merchant/assets/');
define('DEFAULT_FRONT_IMAGE', '//www.funeralnet.com/mfscommon/programs/merchant/assets/graphics/');

include_once('/home/funeralnet/public_html/mfscommon/php/dev_check.php');
$tailString = "?_".date("YmdH");
if(is_dev() or is_staff()) {
    $tailString = "?_".date("YmdHis");
}
define('TAILSTRING', $tailString);

if(is_dev()) {
    if(function_exists("opcache_reset")) {
        @opcache_reset();
    }
    error_reporting(E_ALL);
}

class Main extends CI_Controller {

    public $client_id = CLIENTID;
    public $shop_id = SHOPID;
    public $category;
    public $settings;
    public $locations;
    public $shops;
    public $cfg;
    public $shop_off_records;

    public $logs = array();
    private $logStep = 1;

    public function __construct() {
        parent::__construct();

        $this->load->add_package_path(realpath('./application'), TRUE);

        // session_id();
        $sessionPath = sys_get_temp_dir();
        session_name(CLIENTID . $this->config->config['sess_native_name']. "_" . SHOPNAME);
        session_save_path($sessionPath);
        session_start();

        // $path = session_save_path() . '/sess_' . session_id();
        // chmod($path, 0640);

        // define('DEVSTATE', (is_dev() or is_staff()));
        define('DEVSTATE', is_dev());

        // define asset path if not defined in index.php
        if(!defined('ASSETPATH')) {
            $asset_path = "//www.funeralnet.com/mfscommon/programs/merchant_static_asset/assets";
            if(file_exists(getcwd()."/assets/core/core-less-css.php")) {
                $asset_path = "/".SHOPNAME."/assets";
            }
            define('ASSETPATH', $asset_path);
        }

        # Load helper
        $helpers = array();
        $helpers = array(
            'merchant',
            'sendmail',
        );
        foreach($this->load->get_package_paths() as $path) {
            $path = $path.'helpers';
            $helpers = $this->get_helper_paths($path, $helpers);
        }
        // if(is_dev() and CLIENTID == "hollomon") {
        //     myprint($helpers);
        //     exit();
        // }
        $this->load->helper($helpers);

        $this->load->library(array(
            'session',
            'secureSession',
            'tableMessage',
            'encrytp_shop',
        ));
        $this->session->set_user('loaded-helpers', $helpers);

        $this->load->model(array('merchant_model','obit_model'));
        $this->merchant_model->init($this->client_id, $this->shop_id);
        $this->obit_model->init($this->client_id);
        //$this->sms_model->init($this->client_id);

        $this->settings = $this->merchant_model->get_config();
        // Set default settings
        if( ! @$this->settings['template_path']) {
            @$this->settings['template_path'] = 'flowers-template';
        }

        $this->locations = $this->merchant_model->get_location();
        $this->shop = $this->merchant_model->get_shops();
        $this->shop_name = $this->shop['name']; // Shortcut for shop name


        if (!empty($this->shop['timezone'])) {
            date_default_timezone_set($this->shop['timezone']);
        }



        # Load Payment
        // if($this->shop['payment_gateway']=='Paypal')
        // if(in_array($this->shop['payment_gateway'], array('Paypal','Paypal-Direct')))
        // {
        //     $paypal_details = array(
        //         // Paypal_ec defaults sandbox status to true
        //         // Change to false if you want to go live and
        //         // update the API credentials above
        //         'sandbox_status' => false,
        //         'API_username' => $this->shop['paypal_email'],
        //         'API_signature' => $this->shop['payment_key'],
        //         'API_password' => $this->shop['payment_account'],
        //     );
        //     $this->load->library('payment', array(
        //         'gateway' => 'paypal',
        //         'paypal_details' => $paypal_details,
        //         ));

        //     // $this->load->library('/payment/paypal_driver','','paypal');
        //     // $paypal_details = array(
        //     //     // Paypal_ec defaults sandbox status to true
        //     //     // Change to false if you want to go live and
        //     //     // update the API credentials above
        //     //     'sandbox_status' => false,
        //     //     'API_username' => $this->shop['paypal_email'],
        //     //     'API_signature' => $this->shop['payment_key'],
        //     //     'API_password' => $this->shop['payment_account'],
        //     // );

        //     // if($this->settings['testmode']!=1) {
        //     //     $paypal_details['sandbox_status'] = false;
        //     // }

        //     // $this->paypal->init($paypal_details);
        // }
        // else
        // {
        //     if($this->shop['use_portal']){
        //         //Use Portal does not call any gateway
        //     } else {
        //         $gateway = $this->shop['payment_gateway'];
        //         if(preg_match("/Firstdata/i",$gateway)){
        //             $newgateway = explode(' ',$gateway);
        //             $gateway = $newgateway[1];
        //         }

        //         $this->load->library('payment', array('gateway' => $gateway));
        //     }

        // }


        if($this->shop['use_portal'] or $this->shop['payment_gateway'] == 'Static-Email'){
            //Use Portal does not call any gateway
        } else {
            $gateway = $this->shop['payment_gateway'];
            if(preg_match("/Firstdata/i",$gateway)){
                $newgateway = explode(' ',$gateway);
                $gateway = $newgateway[1];
            }

            $params = array('gateway' => $gateway);

            if(in_array($this->shop['payment_gateway'], array('Paypal','Paypal-Direct'))) {
                $params = array(
                    'gateway' => 'Paypal',
                    'paypal_details' => array(
                        // Paypal_ec defaults sandbox status to true
                        // Change to false if you want to go live and
                        // update the API credentials above
                        'sandbox_status' => false,
                        'API_username' => $this->shop['paypal_email'],
                        'API_signature' => $this->shop['payment_key'],
                        'API_password' => $this->shop['payment_account'],
                    ),
                );
            }

            $this->load->library('payment', $params);
        }


        #load config
        $this->config->load('ecommerce');
        $this->cfg = $this->config->config;

        $this->shop_off_records = $this->merchant_model->get_shop_off_records();

        $this->load->vars('cfg', $this->cfg);
        $this->load->vars('settings', $this->settings);
        $this->load->vars('locations', $this->locations);
        $this->load->vars('shops', $this->shop);
        $this->load->vars('off_records', $this->shop_off_records);

        loadCustomDataDelivery();

    }

    //
    // Recursive function for getting helper paths dynamically.
    //
    private function get_helper_paths($path, $helpers = array()) {
        if ($handle = @opendir($path)) {
            while (false !== ($entry = readdir($handle))) {

                // Skip if entry equal to a current and parent folder.
                if($entry == '.' or $entry == '..') {
                    continue;
                }

                $entry = $path.'/'.$entry;
                if(is_dir($entry)) {
                    $helpers = $this->get_helper_paths($entry, $helpers);
                } else {
                    if(preg_match('/_helper\.php$/', $entry)) {
                        $entry = explode('/application/helpers/', $entry);
                        $entry = $entry[1];
                        $entry = str_replace('_helper.php', '', $entry);
                        if(!in_array($entry, $helpers)) {
                            $helpers[] = $entry;
                        }
                    }
                }
            }
        }
        return $helpers;
    }

    public function debug() {
        if(is_dev()) {
            // perform_sending_email();
            // devprint($this->session->get_user('sent_emails'));
            $this->merchant_model->orderList_debug();
        }
    }

    public function index($category = NULL, $sub_category = NULL) {

        $this->clear_session();
        redirect('products');
    }

    /**
     *
     * @param type $sub_category
     * This function is for list all item in this shop
     * $sub_category is a parameter for query the group in database  if null is show normal page
     */
    public function products($category = NULL) {

        // devprint("debugging");

        $this->check_session(1, 'normalPage');

        if (empty($category)) {
            // devprint("no cat");

            // get catagories for this shop_name
            $categories = $this->merchant_model->get_categories($this->shop_name);
            // devprint($categories);

            $sub_categories_lv2 = $this->merchant_model->get_subcategories_lv2($this->shop_name);
            // devprint($sub_categories_lv2);

            $default_tax_rate = $this->merchant_model->get_default_tax_rate($this->shop_name);
            // devprint($default_tax_rate);

            $this->cart->setTax($default_tax_rate);
            // devprint("tax was set to cart.");

            // devprint($this->cart->getTax());

            $this->template('index', array('category' => $this->shop_name, 'categories' => $categories));

            // // If cat has sub-cats load subcat view
            // if (!empty($categories)) {
            //     $this->template('index', array('category' => $this->shop_name, 'categories' => $categories));
            // }
        } else {

            $sub_categories = $this->merchant_model->get_products($this->shop_name, $category);
            if (count($sub_categories['subcategory_list']) <= 0 or $sub_categories['product_list'] == 'No Products') {
                redirect('/'); // No data Reditect to Category
            }
            $categories = $this->merchant_model->get_categories($this->shop_name);
            $this->session->set_user('last_category' , $category);
            $this->template('products', array('category' => $this->shop_name, 'req_sub_category' => $category, 'subcategories' => $sub_categories));
        }
    }

    public function print_session() {
        print '<pre>';
         // print '<b>$_SESSION</b> - ';
         // print_r(session_save_path());
        print 'cart: ';
        print_r($this->session->userdata);
        echo '<b>$settings</b> - ';
        print_r($this->settings);
        echo '<b>$locations</b> - ';
        print_r($this->locations);
        echo '<b>$shops</b> - ';
        print_r($this->shop);
        echo '<b>$shop_off_records</b> - ';
        print_r($this->shop_off_records);
        echo '<b>$cfg</b> - ';
        print_r($this->cfg);
        echo '<b>$CI Cart</b> - ';
        print_r($this->cart->contents());
        echo '<b>Message</b> - ';
        print_r($this->tablemessage->putMesTbl());
         echo '<b>User session</b> - ';
        print_r($this->session->get_user());
        print '</pre>';
    }

    public function debug_cart()
    {
        echo "============= CI Cart ==========<br/>";
        myprint($this->cart->contents());
        echo "============= owner Cart ==========<br/>";
        myprint($this->session->get_user('cart'));

    }

    public function clear_session() {
        $this->cart->destroy();
        $this->session->sess_destroy();
        $_SESSION = array();

        redirect_default();

        //redirect('products');
    }

    public function clear_cache(){
       unlink(getcwd().'/application/cache/template.html');
       redirect_default();
       //redirect('products');
    }
    /**
     * This function is for show item in the cart
     */
    public function cart() {
        $this->check_session(1, 'cart');
        if ($this->input->post()) {
            //remove
            if ($this->input->post('action', TRUE) == 'remove') {
                $data = array(
                    'rowid' => $this->input->post('rowid'),
                    'qty' => 0
                );
                $this->cart->update($data);
                $this->check_session(1, 'cart');
            } elseif ($this->input->post('action', TRUE) == 'update') {
                $data = array(
                    'rowid' => $this->input->post('rowid'),
                    'qty' => $this->input->post('qty')
                );
            //    $this->cart->contents();
                $this->cart->update($data);

                // devprint($this->session->get_user(), true);

                // $rowid = $this->input->post('rowid', true);
                // $qty = $this->input->post('qty', true);
                // $this->session->set_user("cart=>cart_contents=>$rowid=>qty", $qty);

            } elseif ($this->input->post('action', TRUE) == 'update-option') {

                // $rowid = $this->input->post('rowid');
                // $qty = $this->input->post('qty');
                // $this->session->set_user("cart=>cart_contents=>$rowid=>qty", $qty);

                $rowid = $this->input->post('rowid', true);
                $option_key = $this->input->post('option_key');
                $option_value = $this->input->post('option_value');
                $this->session->set_user("cart=>cart_contents=>$rowid=>selected_options=>$option_key=>value", $option_value);

            } elseif ($this->input->post('action', TRUE) == 'check-out') {
                $cart = array();

                foreach ($this->cart->contents() as $id => $group) {
                    $data = array();
                    foreach ($group as $key => $value) {
                        if ($key == 'options') {
                            foreach ($value as $option_key => $option_value) {
                                if (($option_key == 'selected_price') or ( $option_key == 'category_name') or ( $option_key == 'category_label') or ( $option_key == 'shipping') or ( $option_key == 'shortdesc') or ( $option_key == 'longdesc')) {
                                    $data[$option_key] = $option_value;
                                }
                            }
                        }
                        $data[$key] = $value;
                    }
                    $cart[$group['rowid']] = $data;
                }
                $this->session->set_userdata('cart', $cart);

                redirect('billing');
            }
        }
        // $this->cart->_save_cart();
        // $this->cart->contents();
        // $user_session = $this->session->get_user();
        $data['referencePage'] = null;
        if (isset($_SERVER["HTTP_REFERER"])) {
            $data['referencePage'] = $_SERVER["HTTP_REFERER"];
        } else if (isset($user_session['last_category'])) {
            $data['referencePage'] = $this->cfg['base_url'] . "products/" .$user_session['last_category'];
        }
        $this->template('cart' , $data);
    }

    /**
     *This function is show billing page
     */
    public function billing() {
        $this->check_session(1, 'cart'); // cart shouldn't zero
        $vars = array(
            'prev_form' => 'cart',
            'next_form' => 'delivery',
            'page_title' => 'Billing Information',
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        //$this->session->set_user('cart', $this->cart->contents());

        if (!$this->input->post('scriptaction')) {

        } else {
            $this->session->set_user('billing', $this->input->post());
            $autopopulate = array(
                'deliver_firstname' => $this->input->post('firstname'),
                'deliver_lastname' => $this->input->post('lastname'),
                'deliver_email' => $this->input->post('email'),
                'deliver_streetaddress' => $this->input->post('streetaddress'),
                'deliver_city' => $this->input->post('city'),
                'deliver_state' => $this->input->post('state'),
                'deliver_zip' => $this->input->post('zip'),
                'deliver_daytimephone' => $this->input->post('daytimephone'),
                'deliver_decedent' => $this->input->post('decedent'),
                'deliver_message' => $this->input->post('message'),
            );
            $this->session->set_user('billing=>autopopulate', $autopopulate);
            $this->go_to($this->input->post('next_form'));
        }
        $this->template('billing');
    }

    /**
     *This function is show delivery page
     */
    public function delivery() {
        $this->check_session(1, 'billing'); // billing should has some data
        $this->check_session(1, 'cart');
        $vars = array(
            'prev_form' => 'billing',
            'next_form' => 'payment',
            'page_title' => '',
        );
        $this->load->vars($vars);
        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        // Load data from constructor instead
        // loadCustomDataDelivery();

        if (!$this->input->post('scriptaction')) {

        } else {
            $this->session->set_user('delivery', $this->input->post());
            $user_session = $this->session->get_user();

            //convert date format
            $deliverydate = date('Y-m-d',strtotime(str_replace("-","/",$user_session['delivery']['datedelivery'])));
            if ($next_form == $this->input->post('next_form')){
                checkDelivery();
            }

            $this->session->set_user('delivery=>datedelivery', $deliverydate);

            $this->session->set_user('summary=>totalWithTax', $this->cart->total());
            //$this->order_archive();
            $this->go_to($this->input->post('next_form'));
        }
        $this->template('delivery');
    }



    /**
     *This function is show payment page
     */
    public function payment($param = null) {

        $this->check_session(1, 'payment');
        checkDelivery();

        $error_message = '';
        if ($param == 'error') {
            $error_message = $this->session->get_user('insurance=>result=>reason') . '<br>' . $this->session->get_user('insurance=>result=>message') . '<br>' . $this->session->get_user('insurance=>result=>moremessage');
        } elseif($param == 'bot') {
            $error_message = $this->session->get('error-bot-message');
        }
        $vars = array(
            'prev_form' => 'delivery',
            'next_form' => 'thank_you',
            'page_title' => 'CREDIT CARD INFORMATION',
            'error_message' => $error_message,
            'last_form' => true,
        );
        $this->load->vars($vars);

        if( $this->shop['payment_gateway']=='Stripe' ){
            $stripe_vars['publishable_key'] = $this->shop['payment_key'] ;
            $stripe_vars['secret_key'] = $this->shop['payment_account'] ;
            $this->load->vars($stripe_vars);
        }

        foreach ($vars as $i => $v) {
            $$i = $v;
        }

        $user_session = $this->session->get_user();
        $this->cart->contents();
        # get Taxsale from selected location
        # if location is choses to Other location , should use saletax from shop default
        $salestax_rate = $this->shop['default_tax_rate'];
        $delivery_rate = $this->shop['default_delivery'];
        $hasNotLocation = true;

        foreach ($this->locations as $key => $group) {
            if ($user_session['delivery']['deliverto'] == $group['id']) {
                $salestax_rate = $group['tax_rate'];
                #if delivery rate is null use the delivery fee of location
                if($group['delivery_rate']!=''){
                    $delivery_rate = $group['delivery_rate'];
                }
                $this->session->set_user('salestax_rate', $group);
                $hasNotLocation = false;
                break; // leave the foreach
            }
        }

        #user select Other location
        if ($hasNotLocation) {
            $this->session->set_user('salestax_rate', array(
                "salestax_rate" => $salestax_rate
            ));
        }

        #set taxable
        if($this->shop['delivery_taxable'] == '1'){
            $this->cart->setDeliveryTaxable(true);
            $this->session->set_user('summary=>taxAble ', true);
        } else {
            $this->session->set_user('summary=>taxAble ', false);
        }

        $this->cart->setDeliveryFee($delivery_rate);
        $this->cart->setTax($salestax_rate);



        if (!$this->input->post('scriptaction')) {

            $this->session->set_user('summary=>subTotal',$this->cart->total());
            $this->session->set_user('summary=>saleTax',$this->cart->getTax());
            $this->session->set_user('summary=>deliveryFee', $this->cart->getDeliveryFee());
            $this->session->set_user('summary=>taxAmount',$this->cart->calTax());
            $this->session->set_user('summary=>total',$this->cart->totalWithTax2());

        } else {

            if ($this->input->post('next_form') != 'thank_you') {
                $this->go_to($this->input->post('next_form'));
            }

            $this->session->set_user('insurance', $this->input->post());

            // if (!isset($user_session['uniq'])) {
            //     $this->session->set_user('uniq', $this->generate_uniq_id());
            // }
            get_uniq_id();

            $user_session = $this->session->get_user();

            //
            // Check if you are not a bot
            //
            if(!check_recaptcha()) {
                redirect('payment/bot');
            }

            //$this->cart->contents();
            // devprint("", true);
            if($this->shop['use_portal']){
                $paymentData = array(
                    'gatewayID' => $this->shop['gatewayid'],
                    'privateKey' => $this->shop['privatekey'],
                    'publicKey' => $this->shop['publickey'],
                    'cardNumber' => str_replace('-','',$this->input->post('ccnumber')),
                    'cardName' => $this->input->post('ccname'),
                    'clientID' => $this->shop['client_id'] ,
                    'description' => $this->shop['label']."[ID-".$this->shop['id']."]- ".$this->cart->totalWithTax(),
                    'amount' => $this->cart->totalWithTax(),
                    'exp_year' => $this->input->post('ccyear'),
                    'exp_month' => $this->input->post('ccmonth'),
                    'cvv'=>$this->input->post('cccode'),
                    'email'=>$user_session['billing']['email'],
                );
                $paymentData['userIP'] = $_SERVER['REMOTE_ADDR'];
                $paymentData['clientIP'] = $_SERVER['SERVER_ADDR'];
                $checksum = $paymentData['userIP'].$paymentData['clientIP'].$paymentData['amount'].$paymentData['cvv'].$paymentData['cardNumber'].$paymentData['cardName'].$paymentData['publicKey'].$paymentData['clientID'].$paymentData['gatewayID'].$paymentData['privateKey'];
                $paymentData['checksum'] = md5($checksum);

                // Collect gateway information
                $this->session->set_user('gateway-info', array(
                    'use_portal' => $this->shop['use_portal'],
                    'clientID' => $this->shop['client_id'] ,
                    'gatewayID' => $this->shop['gatewayid'],
                    'privateKey' => $this->shop['privatekey'],
                    'publicKey' => $this->shop['publickey'],
                    ));

                $curl_handle = curl_init();
                curl_setopt($curl_handle, CURLOPT_URL, 'https://gateway.funeralnet.com/pay');
                curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl_handle, CURLOPT_POST, 1);
                curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $paymentData);
            //
                $buffer = curl_exec($curl_handle);
                curl_close($curl_handle);

                $result = json_decode($buffer,true);

                #make Secure credit card
                if ($user_session['insurance']) {
                    $ccnum = $user_session['insurance']['ccnumber'];
                    $this->session->set_user('insurance=>ccnumber', '**** **** **** ' . substr($ccnum, -4));
                    $this->session->set_user('insurance=>result', $result);
                    $user_session = $this->session->set_user('insurance=>cccode', '***');
                }

                $this->createLogString($result);
                $this->writeLog($this->logs,$this->shop['client_id'],'portal');

                if ( $result['isApproved'] ) { // Payment Success
                    $this->order_archive('Accepted');
                    $this->go_to($this->input->post('next_form'));
                } elseif(is_debugger($user_session['billing']['email'])) {
                    $this->order_archive('Accepted');
                    $this->go_to($this->input->post('next_form'));
                } else {
                    //
                    // Send an email for declined
                    //
                    perform_sending_declined_email();

                    $this->order_archive();

                    redirect('payment/error');
                }
            } else {
                if($this->shop['payment_gateway']=='Paypal-Direct') {
                    $amount = $this->cart->calTax();

                    $to_buy = array(
                        'method' => 'direct',
                        'orderid' => get_uniq_id(),
                        'card_name' => $this->input->post('ccname'),
                        'last_name' => '',
                        'address' => '',
                        'city' => '',
                        'state' => '',
                        'zip' => '',
                        'ip_address' => $_SERVER['SERVER_ADDR'],
                        'type' => 'Sale',
                        'currency' => 'USD',
                        'country' => 'US',
                        'description' => get_uniq_id() . '-' . $this->shop['label'] . ' - $' . $this->cart->totalWithTax(),
                        'auth_login' => $this->shop['payment_account'],
                        'auth_transkey' => $this->shop['payment_key'],
                        'ccnumber' => str_replace('-','',$this->input->post('ccnumber')),
                        'expiration_of_month' => $this->input->post('ccmonth'),
                        'expiration_of_year' => '20'.$this->input->post('ccyear'),
                        'amount' => !is_debugger($user_session['billing']['email']) ? $this->cart->totalWithTax() :  rand(100, 119) / 100 ,
                        //'amount' => rand(100, 119) / 100,
                        'cccode' => $this->input->post('cccode'),

                    );

                    // Collect gateway information
                    $this->session->set_user('gateway-info', array(
                        'payment_gateway' => $this->shop['payment_gateway'],
                        'auth_login' => $this->shop['payment_account'],
                        'auth_transkey' => $this->shop['payment_key'],
                        ));

                    foreach($this->cart->contents() as $pid => $product) {
                        $temp_product = array(
                                'name' => $product['options']['product_name'],
                                'desc' => '(' .  $this->settings['shop_config']['priceLabel'][$product['options']['selected_price']] . ')',
                                'number' => $product['id'],
                                'quantity' => $product['qty'], // simple example -- fixed to 1
                                'amount' => $product['price']);

                        // add product to main $to_buy array
                        $to_buy['products'][] = $temp_product;
                    }

                    $to_buy['products'][] = array(
                        'name' => 'Delivery fee',
                        'amount'=> $this->cart->getDeliveryFee()
                    );

                    if (is_debugger($user_session['billing']['email']) and DEVSTATE ) {
                        $amount = rand(100, 119) / 100;

                        $to_buy['tax_amount'] = $amount;
                        $productCount = 0;
                        foreach($to_buy['products'] as $key=>$item){
                            $to_buy['products'][$key]['amount'] = $amount;
                            $productCount++;
                        }

                        $this->createLogString("Change Amount to : ".$amount*$productCount);
                    }

                     // echo '<pre />';
                     // print_r($to_buy);

                    $this->payment->process($to_buy);
                    $paypal_order = $this->payment->getOrder();
                    $this->session->set_user('cc_result', $paypal_order);

                    // if (isset($set_ec_return['ec_status']) && ($set_ec_return['ec_status'] === true)) {
                    if ($this->payment->isApproved() or is_debugger($user_session['billing']['email'])) {
                        // redirect to Paypal
                        // $this->order_archive();
                        // $this->merchant_model->save_token_paypal(get_uniq_id(),$set_ec_return['TOKEN'],'0');
                        // $this->load->view('thankyou');

                        // $this->paypal->redirect_to_paypal($set_ec_return['TOKEN']);
                        $this->order_archive('Accepted');
                        $this->go_to($this->input->post('next_form'));
                        // You could detect your visitor's browser and redirect to Paypal's mobile checkout
                        // if they are on a mobile device. Just add a true as the last parameter. It defaults
                        // to false
                        // $this->paypal_ec->redirect_to_paypal( $set_ec_return['TOKEN'], true);

                    } else {

                        $this->session->set_user('insurance=>result=>reason', "Error:".$set_ec_return['L_ERRORCODE0'] );
                        $this->session->set_user('insurance=>result=>message', $set_ec_return['L_LONGMESSAGE0'] );
                        $this->session->set_user('insurance=>result=>moremessage', $set_ec_return['L_LONGMESSAGE0'] );
                        redirect('payment/error');
                    }

                } 
                elseif($this->shop['payment_gateway']=='Paypal') {
                    $amount = $this->cart->calTax();

                    $to_buy = array(
                        'method' => 'express',
                        'orderid' => get_uniq_id(),
                        'desc' => 'Purchase from '.$this->client_id.' '.$this->shop_name,
                        'currency' => 'USD', // currency for the transaction
                        'type' => 'Sale',  // for PAYMENTREQUEST_0_PAYMENTACTION, it's either Sale, Order or Authorization
                        'return_URL' => site_url('paypal/success'),
                        // see below have a function for this -- function back()
                        // whatever you use, make sure the URL is live and can process
                        // the next steps
                        'cancel_URL' => site_url('paypal/cancel'), // this goes to this controllers index()
                        'tax_amount' => $this->cart->calTax(),

                    );
                    foreach($this->cart->contents() as $pid => $product) {
                        $temp_product = array(
                                'name' => $product['options']['product_name'],
                                'desc' => '(' .  $this->settings['shop_config']['priceLabel'][$product['options']['selected_price']] . ')',
                                'number' => $product['id'],
                                'quantity' => $product['qty'], // simple example -- fixed to 1
                                'amount' => $product['price']);

                        // add product to main $to_buy array
                        $to_buy['products'][] = $temp_product;
                    }

                    $to_buy['products'][] = array(
                        'name' => 'Delivery fee',
                        'amount'=> $this->cart->getDeliveryFee()
                    );

                    if (is_debugger($user_session['billing']['email']) and DEVSTATE ) {
                        $amount = rand(100, 119) / 100;

                        $to_buy['tax_amount'] = $amount;
                        $productCount = 0;
                        foreach($to_buy['products'] as $key=>$item){
                            $to_buy['products'][$key]['amount'] = $amount;
                            $productCount++;
                        }

                        $this->createLogString("Change Amount to : ".$amount*$productCount);
                    }

                    $this->payment->process($to_buy);
                    $paypal_order = $this->payment->getOrder();

                    // if (isset($set_ec_return['ec_status']) && ($set_ec_return['ec_status'] === true)) {
                    if ($this->payment->isApproved() or is_debugger($user_session['billing']['email'])) {
                        // redirect to Paypal
                        $this->order_archive();

                        $this->merchant_model->save_token_paypal(get_uniq_id(),$paypal_order['TOKEN'],'0');
                        // $this->paypal->redirect_to_paypal($set_ec_return['TOKEN']);
                        $this->payment->redirect_to_paypal();
                        // You could detect your visitor's browser and redirect to Paypal's mobile checkout
                        // if they are on a mobile device. Just add a true as the last parameter. It defaults
                        // to false
                        // $this->paypal_ec->redirect_to_paypal( $set_ec_return['TOKEN'], true);
                    } else {

                        $this->session->set_user('insurance=>result=>reason', "Error:".$set_ec_return['L_ERRORCODE0'] );
                        $this->session->set_user('insurance=>result=>message', $set_ec_return['L_LONGMESSAGE0'] );
                        $this->session->set_user('insurance=>result=>moremessage', $set_ec_return['L_LONGMESSAGE0'] );
                        redirect('payment/error');
                    }


                } 
                elseif( $this->shop['payment_gateway']=='Stripe' ){

                    include_once('/home/funeralnet/public_html/mfscommon/php/stripe/init.php');
                    \Stripe\Stripe::setApiKey($this->shop['payment_account']);


                    $passed = false;
                    try {

                        $token  = $_POST['stripeToken'];
                        $email = $_POST['stripeEmail'];
                        $customer = \Stripe\Customer::create(array(
                            'email' => $email,
                            'card'  => $token
                        ));

                        // Collect gateway information
                        $this->session->set_user('gateway-info', array(
                            'payment_gateway' => $this->shop['payment_gateway'],
                            'email' => $email,
                            'card' => $token,
                            ));

                        // $total_charge = $this->cart->total() ;
                        $total_charge = $this->cart->totalWithTax() ;
                        #Test mode Random amount 1-1.19
                        if (is_debugger($user_session['billing']['email']) and DEVSTATE ) {
                            $total_charge = rand(100, 119) / 100;
                            $this->createLogString("Change Amount to : ".$total_charge);
                        }

                        $charge = \Stripe\Charge::create(array(
                            'customer' => $customer->id,
                            'amount'   => number_format($total_charge, 2, '', ''),
                            'currency' => 'usd',
                        ));

                        $passed = $charge->paid;

                    } catch (Stripe\Error\Base $e) {
                        // Code to do something with the $e exception object when an error occurs
                        // devprint($e->getMessage(), true);
                    } catch (Exception $e) {
                        // Catch any other non-Stripe exceptions
                        // devprint($e->getMessage(), true);
                    }


                    if($passed == true or is_debugger($user_session['billing']['email'])){
                        // echo '<h1>Successfully charged</h1>';
                        $logging['email'] = $charge->source->name ;
                        $logging['last_4'] = $charge->source->last4 ;
                        $logging['exp_month'] = $charge->source->exp_month ;
                        $logging['exp_year'] = $charge->source->exp_year ;
                        $logging['amount'] = $charge->amount ;
                        //$this->session->set_user('payment=>log',$logging) ;

                        #make Secure credit card
                        if ($user_session['insurance']) {
                            $this->session->set_user('insurance=>ccnumber', $logging['last_4'] );
                            $this->session->set_user('insurance=>result=>result', "success" );
                            $user_session = $this->session->set_user('insurance=>cccode', '***');
                        }

                        $this->order_archive('Accepted');
                        $this->go_to($this->input->post('next_form'));

                    }
                    else{
                        $this->session->set_user('insurance=>result=>result', "error" );
                        $this->session->set_user('insurance=>result=>reason', "Error : Please use other credit card or contact Funeralhome." );

                        //
                        // Send an email for declined
                        //
                        perform_sending_declined_email();

                        $this->order_archive();
                        redirect('payment/error');
                    }

                } 
                elseif( $this->shop['payment_gateway']=='Static-Email' ) {
                    $this->order_archive('Accepted');
                    $this->go_to($this->input->post('next_form'));
                }
                else {
                    $gateway_collect = array();
                    $data = array(
                        'email' => $user_session['billing']['email'],
                        'card_name' => $this->input->post('ccname'),
                        'last_name' => '',
                        'address' => '',
                        'city' => '',
                        'state' => '',
                        'zip' => '',
                        'country' => 'US',
                        'description' => get_uniq_id() . '-' . $this->shop['label'] . ' - $' . $this->cart->totalWithTax(),
                        'auth_login' => $this->shop['payment_account'],
                        'auth_transkey' => $this->shop['payment_key'],
                        'ccnumber' => str_replace('-','',$this->input->post('ccnumber')),
                        'expiration_of_month' => $this->input->post('ccmonth'),
                        'expiration_of_year' => $this->input->post('ccyear'),
                        'amount' => $this->cart->totalWithTax(),
                        'cccode' => $this->input->post('cccode'),
                    );
                    // devprint($data, true);
                    $gateway_collect['auth_login'] = $this->shop['payment_account'];
                    $gateway_collect['auth_transkey'] = $this->shop['payment_key'];

                    $this->createLogString($data);
                    if (strtolower($this->shop['payment_gateway']) == 'firstdata turnkey') { // TurnKey
                        $data['hmackey'] = $this->shop['hmackey'];
                        $data['keyid'] = $this->shop['keyid'];
                        $data['customer_ref'] = '';
                        $data['reference_3'] = '';
                        $data['reference_no'] = ''; // unique id

                        $gateway_collect['hmackey'] = $this->shop['hmackey'];
                        $gateway_collect['keyid'] = $this->shop['keyid'];

                    }

                    if (strtolower($this->shop['payment_gateway']) == 'firstdata linkpoint') { // Link Point
                        $data['auth_login'] = $this->shop['store_number'];
                        $data['cert'] = FCPATH.$this->shop['cert'];
                        $data['result'] ='LIVE';
                        //$data['result']='GOOD';

                        $gateway_collect['auth_login'] = $this->shop['store_number'];
                        $gateway_collect['cert'] = FCPATH.$this->shop['cert'];
                        $gateway_collect['result'] = 'LIVE';
                    }

                    if (strtolower($this->shop['payment_gateway']) == 'firstdata payeezy') { // Firstdata Payeezy
                        $data['apiKey'] = $this->shop['payment_account'];
                        $data['apiSecret'] = $this->shop['payment_key'];
                        $data['merchantToken'] = $this->shop['merchant_token'];

                        $gateway_collect['apiKey'] = $this->shop['payment_account'];
                        $gateway_collect['apiSecret'] = $this->shop['payment_key'];
                        $gateway_collect['merchantToken'] = $this->shop['merchant_token'];
                    }

                    if (strtolower($this->shop['payment_gateway']) == 'usaepay') {
                        $data['invoice'] = get_uniq_id();
                        $gateway_collect['invoice'] = get_uniq_id();
                        if(is_debugger($user_session['billing']['email'])){
                            $data['sandbox'] = true;
                        } else {
                            $data['sandbox'] = false;
                        }
                        $gateway_collect['sandbox'] = $data['sandbox'];
                    }
                    #Test mode Random amount 1-1.19
                    if (is_debugger($user_session['billing']['email']) and DEVSTATE ) {

                        $data['amount'] = rand(100, 119) / 100;
                        $this->createLogString("Change Amount to : ".$data['amount']);
                    }
                    $this->createLogString($data);

                    #
                    $this->payment->process($data);

                    // Collect gateway information
                    $this->session->set_user('gateway-info', $gateway_collect);

                    /*
                    echo "<pre>";
                    print_r($this->payment->getOrder());
                    echo "</pre>";
                    exit();
                    */


                    #make Secure credit card
                    if ($user_session['insurance']) {
                        $ccnum = $user_session['insurance']['ccnumber'];
                        $this->session->set_user('insurance=>ccnumber', '**** **** **** ' . substr($ccnum, -4));
                        $this->session->set_user('insurance=>result', $this->payment->getOrder());
                        $user_session = $this->session->set_user('insurance=>cccode', '***');
                    }


                    $this->createLogString($this->payment->getOrder());
                    $this->writeLog($this->logs,$this->shop['client_id'],strtolower($this->shop['payment_gateway']) );

                    if ($this->payment->isApproved() or is_debugger($user_session['billing']['email'])) { // Payment Success
                        $this->order_archive('Accepted');
                        $this->go_to($this->input->post('next_form'));
                    } else {

                        //
                        // Send an email for declined
                        //
                        perform_sending_declined_email();

                        $this->order_archive();
                        redirect('payment/error');
                    }
                }
            }
        }

        if( $this->shop['payment_gateway']=='Stripe' ){
            $this->template('payment_stripe');
        } else {
            $this->template('payment');
        }
    }

    /**
     *
     * @param type $param
     * recive data from paypal callback
     * if the transection is correct archive this order to Accept and redirect to thankyou page
     * http://sandbox.funeralnet.com/flowers/paypal/success?token=EC-84R836410M044182E&PayerID=MYM4J7KTF7MMS
     */
    public function payment_stripe( $param = null ){
        if( $this->input->post() ){
            print_r( $this->input->post() ) ;
        }
    }
    public function paypal($param=null){

        if ($param == 'success') {

            $token = $_GET['token'];
            $payer_id = $_GET['PayerID'];
            $get_ec_return = $this->paypal->get_ec($token);

            if (isset($get_ec_return['ec_status']) && ($get_ec_return['ec_status'] === true)) {
                $tranToken = $this->merchant_model->checktoken($token);

                if($tranToken['status']){
                    //load old section
                    $session = $this->merchant_model->get_order_by_uniq($tranToken['uniq']);
                    $data = unserialize($session);
                    $this->session->set('user',$data);

                    $user_session = $this->session->get_user();

                    $this->order_archive('Accepted');
                    $this->merchant_model->save_token_paypal(get_uniq_id(),$token,'1');
                    redirect('thank_you');
                } else {
                    $this->session->set_user('insurance=>result=>reason', "Error:Token ID is not correct" );
                    redirect('payment/error');
                }

            } else {
                $this->session->set_user('insurance=>result=>reason', "Error:".$get_ec_return['L_ERRORCODE0'] );
                $this->session->set_user('insurance=>result=>message', $get_ec_return['L_LONGMESSAGE0'] );
                $this->session->set_user('insurance=>result=>moremessage', $get_ec_return['L_LONGMESSAGE0'] );
                redirect('payment/error');
            }


        } elseif($param == 'cancel') {
            redirect('payment');
        } else {
            redirect('payment');
        }
    }

    public function writeLog($data,$clientname='noparam',$gatewayName='noparam',$gatewayID='gateway'){
    	$y = date('Y');
    	$m = date('m');
    	$d = date('d');

    	$directory = 'application/cache/log/payment/'.$clientname.'/'.$gatewayName.'/'.$y.'/'.$m.'/'.$d;
    	if(!is_dir($directory)){
    		mkdir($directory, 0777, true);
    	}

    	$f = fopen($directory.'/'.$gatewayName.'_'.$gatewayID.'.txt', "a+");
    	fwrite($f,'--------------------------------------------------------------------' . "\n ");
    	fwrite($f, date('Y-m-d H:i:s') . '    ' . (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . "\n ");
            $results = print_r($data, true);
            fwrite($f,$results);
    	fclose($f);
        // echo $directory;
    }

    private function createLogString($data){
        if(is_array($data)){
            foreach($data as $key=>$value){
                $this->logs[$this->logStep][$key] = $value;
            }
        } else {
            $this->logs[$this->logStep]['Message'] = $data;
        }
        $this->logs[$this->logStep]['timestamp'] = date('Y-m-d H:i:s');
        $this->logStep++;
    }

    public function thank_you() {
        $vars = array(
            'page_title' => '',
            'last_form' => true,
        );
        $this->template('thankyou');
    }

     /**
     *
     * @param type $obitid
     * Billing with Obit program
     */
    public function obit($obitid = null){
        $obit = $this->input->get('obit', TRUE);
        if(isset($obit)){
            $obitid = $obit;
        }
        if(is_null($obitid) or $obitid ==''){
            echo "redirect Back";
        } else {
            $obit = $this->obit_model->ObitDecedent($obitid);

            $this->session->set_user('billing=>decedent',$obit['fname'].' '.$obit['mname'].' '.$obit['lname']);
            $this->session->set_user('delivery=>deliver_decedent',$obit['fname'].' '.$obit['mname'].' '.$obit['lname']);

            $ObitAddress = $this->obit_model->ObitService($obitid);

            /*
             * Array (
             * [defid] => 752919
             * [clientid] => funeralnet
             * [branchid] => 0
             * [top] => 0
             * [name] => Memphis Funeral Home
             * [address] => 2200 East Matthews
             * [city] => Jonesboro
             * [state] => AR
             * [country] =>
             * [zip] => 72401
             * [phone] => (999)999-9789
             * [email] => mscott@continentalcomputers.com
             * [url] => www.continentalcomputers.com
             * [list] => 0
             * [type] => Funeral Home
             * [photo] =>
             * [stamp] => 2013-09-22 20:31:46 )
             */
            $this->session->set_user('delivery=>deliverto','other');
            $this->session->set_user('delivery=>deliver_firstname','Memorial Service');
            $this->session->set_user('delivery=>deliver_lastname',$ObitAddress['name']);
            $this->session->set_user('delivery=>deliver_email',$ObitAddress['email']);
            $this->session->set_user('delivery=>deliver_streetaddress',$ObitAddress['address']);
            $this->session->set_user('delivery=>deliver_zip',$ObitAddress['zip']);
            $this->session->set_user('delivery=>deliver_city',$ObitAddress['city']);
            $this->session->set_user('delivery=>deliver_state',$ObitAddress['state']);
            $this->session->set_user('delivery=>deliver_daytimephone',$ObitAddress['phone']);
            $ObitServiceDate = $this->obit_model->ObitServiceDate($obitid);
            /*
             * Array (
             * [sdate] => 2013-09-28
             * )
             */
            if($ObitServiceDate){
                $this->session->set_user('delivery=>datedelivery',date("m/d/Y", strtotime($ObitServiceDate['sdate'])));
            }

            redirect('products');
        }
    }

    ####################################################################################################
    #											Ajax											   #
    ####################################################################################################

    /*
     * Use for add item into the cart
     */
    public function ajax_add_to_cart() {
        $product_id = $this->input->post('product_id', TRUE);
        $product_price = $this->input->post('product_price', TRUE);

        $qty = $this->input->post('qty', TRUE);
        // $product_rec = $this->merchant_model->get_product_by_id($product_id);
        #price = field , eg. price price2 price3
        $product = array(
            'id' => $product_id,
            'price' => $product_price,
            'qty' => $qty,
            'selected_options' => array(),
        );
        if($this->input->post('selected_options')) {
            $product['selected_options'] = $this->input->post('selected_options');
        }
        $rt['post'] = $this->input->post();
        $this->update_cart($product);
        if ($this->cart->getError()) {
            $rt['status'] = 'fail';
            $rt['erroe'] = $this->cart->getError();
        } else {
            $rt['status'] = 'complete';
        }
        $rt['total_items'] = $this->cart->total_items();
        echo json_encode($rt);
    }

    /*
     * Use for remove item in the cart with rowid
     */
    public function ajax_remove_to_cart() {
        $data = array(
            'rowid' => $this->input->post('rowid'),
            'qty' => 0
        );
       // $this->cart->contents();

        $this->cart->update($data);

        $rt['status'] = 'complete';
        $rt['total_items'] = $this->cart->total_items();
        $rt['sum'] = $this->cart->total();
        echo json_encode($rt);
    }

    public function ajax_load_cart() {
        $this->load->view('ajax_cart');
    }

    ####################################################################################################
    #											FUNCTIONS											   #
    ####################################################################################################

    private function generate_uniq_id() {
        // $digit = 6; # Number of digit for order id (Include year number for 2 first digit)

        // $space = "";
        // for ($i = 1; $i <= $digit; $i++) {
        //     $space.="0";
        // }

        // $uniqid = date("y") . substr($space . "1", (($digit - 2) * -1));

        // $last_uniq = $this->merchant_model->get_last_uniq_id();
        // //echo $last_uniq;
        // //echo "<br>";
        // if ($last_uniq) {
        //     $uniqid = substr($last_uniq, (($digit - 2) * -1)) + 1;
        //     $uniqid = date("y") . substr($space . $uniqid, (($digit - 2) * -1));
        // }

        // $prefix = 'E' . substr($this->session->get_user('billing=>state'), 0, 1) . 'SC';

        // //echo  $prefix.$uniqid;
        // return $prefix . $uniqid;

        return gen_uniq_id();
    }

    private function go_to($next_form) {
        # Build an absolute URL
        //$url = $this->proper_url(array('page' => $next_form, 'secure' => $this->config->item('secure')));
        # Load the next page.
        //header('Location: ' . $url);
        redirect($next_form);
        exit;
    }

    private function template($view, $data = array()) {
        template($view, $data);
    }

    private function email_template($file, $arr = array()) {
        // $html = '';

        // $template = fopen($file, 'r') or die("Template : Couldn't open for reading \"$file\"\n");
        // while (!feof($template)) {
        //     $html .= fgets($template, 1024);
        // }
        // fclose($template);

        // $html = @preg_replace('/\$(\w+)\$/e', '$arr["$1"]', $html);

        // return $html;




        //
        // Call to a helper function in sendmail_helper.php instead.
        //
        return email_template($file, $arr);
    }

    private function get_product_by_id($product_id, $field = "") {

        return $this->merchant_model->get_product_by_id($product_id, $field);
    }

    /**
     *
     * @param type $need_redirect
     * @param type $step
     * Check Session for each step
     */
    private function check_session($need_redirect = 0, $step = 'cart') {
        $user_session = $this->session->get_user();
        $denide = false;

        switch ($step) {
            case 'normalPage' :
                if ($this->order_completed()) { // cart shouldn't zero
                    $denide = true;
                }
                break;
            case 'cart' :
                if ($this->order_completed() or $this->cart->total_items() <= 0) { // cart shouldn't zero
                    $denide = true;
                }
                break;
            case 'delivery' :
                if ($this->order_completed() or ! isset($user_session['delivery'])) { // delivery should has some data
                    $denide = true;
                }
                break;
            case 'billing' :
                if ($this->order_completed() or ! isset($user_session['billing'])) { // billing should has some data
                    $denide = true;
                }
                break;
            case 'payment' :
                if ($this->order_completed() or ! isset($user_session['billing']) or ! isset($user_session['delivery']) or ( $this->cart->total_items() <= 0 )) {
                    $denide = true;
                }
                break;
        }

        if ($denide) {
            if ($need_redirect) {
                // echo $step;
                // exit();
                //$redirect_to = $this->settings['base_url'].$this->shop['name'];
                $redirect_to = base_url(); //$this->settings['base_url'];
                header("Location: $redirect_to");
            }
        }
    }

    /**
     *
     * @param type $product
     * update cart session
     * @return boolean
     */
    private function update_cart($product = "") {

        if ($product == "")
            return false;

        //if ( (! is_numeric($product['qty'])) OR ($product['qty'] < 0) ) return false;

        if ($product['price'] != "price" AND
                $product['price'] != "price2" AND
                $product['price'] != "price3")
            return false;

        $arr = $this->get_product_by_id($product['id']);

        if (!$arr)
            return false;

        switch ($product['price']) {
            case 'price': $grade  = 'Good'; break;
            case 'price2': $grade = 'Better'; break;
            case 'price3': $grade = 'Best';  break;
            default: $grade = 'Unknow'; break;
        }

        $makeCart = array(
            'id' => $product['id'],
            'qty' => 1,
            'price' => $arr[$product['price']],
            'name' =>  cleanCharacter($arr['product_name']) ,
            'second_name' =>  cleanCharacter($arr['second_name']) ,
            'grade' => $grade ,
            'selected_options' => @$product['selected_options'],
            'options' => $arr,
        );

        //
        // Prepare selected options
        //
        if(count($makeCart['selected_options'])) {
            foreach((array)$makeCart['selected_options'] as $key=>$option) {
                $makeCart['selected_options'][$key] = array(
                    'label' => @$arr['additional_options'][$key]['label'],
                    'value' => $option,
                    );
            }
        }


        //Check cart
        foreach ($this->cart->contents() as $pid => $prod_arr) {
            if ($prod_arr['id'] == $makeCart['id'] and $makeCart['price'] == $prod_arr['price']) {
                $makeCart['qty'] = $prod_arr['qty'] + 1;
            }
        }

        $makeCart['options']['selected_price'] = $product['price'];

        $this->cart->insert($makeCart);
        return true;
    }

    /**
     *
     * @return boolean
     * check Order already complated
     */
    private function order_completed() {
        if ($this->session->get_user('order_archive_id')) {
            $order = $this->merchant_model->get_order_by_id($this->session->get_user('order_archive_id'));
            if (strtolower($order['status']) == 'accepted') {

                return true;
            }
        }
        return false;
    }

    /*
     * insert or update session to database
     */

    private function order_archive($status = "Declined") {
        # If the order was completed the record shouldn't be updating anymore.
        if ($this->order_completed())
            return;

        # Purchase date
        if (!$this->session->get_user('purchasedateint')) {
            $this->session->set_user('purchasedateint', time());
        }

        // Try to generate a uniq id if not exist
        get_uniq_id();

        $times = $this->session->get_user('purchasedateint');

        if ($status == 'Accepted') {

            //Teleflora service
            if ( $this->shop['is_teleflora'] )
            {
                send_teleflora();
            }


            // $this->load->library('provider');

            // $emailpovider = $this->provider->getDetailProvider($user_session['delivery']['deliverto']);
            // $this->session->set_user('provider', $emailpovider);

            //
            // Perform sending out notification emails.
            //
            perform_sending_email();




            // //Send to customer
            // $message_customer = $this->email_template('assets/templates/ecommerce_customer.tmpl.html', $user_session);
            // send_mail($this->shop['email_from'], $user_session['billing']['email'], 'Thank You for Your '.$this->shop['label'].' Order', $message_customer);

            // if (is_debugger($user_session['billing']['email']) and DEVSTATE ) {
            //     //send to debug email
            //     $message_deliver = $this->email_template('assets/templates/ecommerce_deliver.tmpl.html', $user_session);
            //     send_mail($this->shop['email_from'], $user_session['billing']['email'],'Thank You for Your '. $this->shop['label'].' Order for '.$user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname'], $message_deliver);
            // } else {
            //      //Send to deliver
            //     if(@$user_session['delivery']['deliver_email']){
            //         $message_deliver = $this->email_template('assets/templates/ecommerce_deliver.tmpl.html', $user_session);
            //         send_mail($this->shop['email_from'], @$user_session['delivery']['deliver_email'],'Thank You for Your '. $this->shop['label'].' Order for '.$user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname'], $message_deliver);
            //     }
            // }

            // if (is_debugger($user_session['billing']['email']) and DEVSTATE ) {
            //     $message_shop = $this->email_template('assets/templates/ecommerce_shop.tmpl.html', $user_session);
            //     send_mail($this->shop['email_from'], $user_session['billing']['email'], 'Thank You for Your '.$this->shop['label'].' Order for '.$user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname'], $message_shop);
            // } else {
            //     //Send to shop
            //     $message_shop = $this->email_template('assets/templates/ecommerce_shop.tmpl.html', $user_session);
            //     eval('$email_to = ' . $this->shop['email_to'] . ';');
            //     if(is_array($email_to)) {
            //         foreach($email_to as $i=>$mail) {
            //             send_mail($this->shop['email_from'], $mail, 'New '.$this->shop['label'].' Order for '.$user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname'], $message_shop);
            //         }
            //     } else {
            //        // send_mail($this->shop['email_from'], $this->shop['email_to'], 'Thank You for Your '.$this->shop['label'].' Order for '.$user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname'], $message_shop);
            //     }
            // }

            // if (is_debugger($user_session['billing']['email']) and DEVSTATE ) {
            //     $message_shop = $this->email_template('assets/templates/ecommerce_provider.tmpl.html', $user_session);
            //     send_mail($this->shop['email_from'], $user_session['billing']['email'],  'Thank You for Your '.$this->shop['label'].' Order for '.$user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname'], $message_shop);
            // } else {
            //     //Send to provider
            //     $message_shop = $this->email_template('assets/templates/ecommerce_provider.tmpl.html', $user_session);
            //     send_mail($this->shop['email_from'], $emailpovider['email'],  'Thank You for Your '.$this->shop['label'].' Order for '.$user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname'], $message_shop);
            // }

            //Send sms to Shop
            if(@$this->settings['sms_to']){
                send_sms("+15039678008", $this->settings['sms_to'],'You have a new order from '.$this->client_id.' '.$this->shop['name'].' program', $this->shop['label']);
            }

            //Send sms to Provider
            if(@$user_session['provider']['phone']){
                send_sms("+15039678008", $user_session['provider']['phone'],'You have a new order from '.$this->client_id.' '.$this->shop['name'].' program', $this->shop['label']);
            }

        }

        # Preparing data to save to archive record
        $user_session = $this->session->get_user();
        $user_session['cart'] = $this->cart->contents();
        $user_session['delivery']['hoursdelivery'] = date("H");
        $user_session['delivery']['minutesdelivery'] = date("i");

        # Wrapping CC number
        $user_session['insurance']['ccnumber'] = "**".substr($user_session['insurance']['ccnumber'], -4);
        $user_session['insurance']['cccode'] = "***";

        # This session will be saved in archive record
        $session_save = serialize($user_session);

        $transaction = isset($user_session['insurance']['result']['orderNumber']) ? $user_session['insurance']['result']['orderNumber'] : null;
        # Payment method
        $data = array(
            "shop_id" => $this->shop['id'],
            "first_name" => $user_session['billing']['firstname'],
            "last_name" => $user_session['billing']['lastname'],
            "email" => $user_session['billing']['email'],
            "amount" => str_replace(',', '', $user_session['summary']['total']),
            "method" => @$this->shop['payment_gateway'] ? $this->shop['payment_gateway'] : '',
            "session" => $session_save,
            "delivery_date" => @$user_session['delivery']['datedelivery'],
            "status" => $status,
            "uniq" => get_uniq_id(),
            "created" =>date('Y-m-d H:i:s'),
            "transaction_id" => $transaction,
            "decedent" => @$user_session['delivery']['deliver_decedent'],
        );

        if ($this->session->get_user('order_archive_id')) {
            $this->merchant_model->update_order_archive($data, $this->session->get_user('order_archive_id'));
        } else {
            $order_archive_id = $this->merchant_model->add_order_archive($data);
            $this->session->set_user('order_archive_id', $order_archive_id);

            //Update session
            $user_session = $this->session->get_user();
            $user_session['cart'] = $this->cart->contents();
            $user_session['delivery']['hoursdelivery'] = date("H");
            $user_session['delivery']['minutesdelivery'] = date("i");

            # Wrapping CC number
            $user_session['insurance']['ccnumber'] = "**".substr($user_session['insurance']['ccnumber'], -4);
            $user_session['insurance']['cccode'] = "***";

            $session_save = serialize($user_session);
            $updateData = array('session' => $session_save );
            $this->merchant_model->update_order_archive($updateData, $this->session->get_user('order_archive_id'));
        }
    }

    public function debugMessage(){
        $user_session = $this->session->get_user();

            $this->load->library('provider');

            $emailpovider = $this->provider->getDetailProvider($user_session['delivery']['deliverto']);
            $this->session->set_user('provider', $emailpovider);

            /**
             * Purchase Information
             */
            $this->tablemessage->putMesTbl('[clear]');
            $this->tablemessage->putMesTbl();
            $this->tablemessage->putMesTbl('Item in your Shopping Cart');
            foreach ($this->cart->contents() as $pid => $product) {
                /* insert image */
                $img = "<img class='img-responsive ' src='https://fnetclients.s3.amazonaws.com/merchant/clients/".$this->shop['client_id']."/pictures/".$product['options']['thumb']."' alt='".$product['name']."'>";
                $this->tablemessage->putMesTbl($img);
                $this->tablemessage->putMesTbl($product['qty'] . 'x ' . $product['name'], '$' . number_format($product['price'] * $product['qty'], 2));
                if(count($product['selected_options'])) {
                    foreach((array)$product['selected_options'] as $option) {
                        $this->tablemessage->putMesTbl('[indent]', @$option['label'].": ".@$option['value']);
                    }
                }
            }
            $this->tablemessage->putMesTbl();
            $this->tablemessage->putMesTbl('Sub total', number_format($user_session['summary']['totalWithTax'], 2));
            $this->tablemessage->putMesTbl('(Tax rate '.$user_session['salestax_rate']['tax_rate'].'%)');
            $this->tablemessage->putMesTbl('Tax amount', number_format($user_session['summary']['taxAmount'], 2));
            $this->tablemessage->putMesTbl('Delivery fee', number_format($user_session['summary']['deliveryFee'], 2));
            $this->tablemessage->putMesTbl('Total', number_format($user_session['summary']['total'], 2));
            //$this->session->set_user('arrangementtable', $this->tablemessage->putMesTbl());
            $this->session->set_user('purchaseinfo', $this->tablemessage->putMesTbl());

            echo "<pre>";
            print_r($this->session->get_user());
            echo "</pre>";
    }

    public function debugUniq(){
        //$uniq = (int)"10000" + 1;


        $uniq = $this->merchant_model->getNewUniq();
        echo str_pad($uniq, 4, '0', STR_PAD_LEFT);
    }

    public function debug_mode() {

        $condition = json_decode($this->shop['condition_same_day'] , true);
        echo '<pre>';
        print_r($condition);
        echo '</pre>';
        //for($i=0; $i<7; $i++) {
            echo date('w' , strtotime(date('Y-m-d H:i:s')) );
            echo date('l'  ,strtotime(date('Y-m-d H:i:s')) );
            echo '<br/>';
            echo "Today : " . date('Y-m-d H:i:s') ."<br/>";
        //}

        $condition = json_decode($this->shop['condition_same_day'] , true);
        $selectDay = '2016-05-30 14:51:00';
        $timeSelect = strtotime(date('H:i' , strtotime($selectDay)));

        $daynumber = date('w' , strtotime(date('Y-m-d H:i:s')) );
        $dayCondition = isset($condition[$daynumber]) ? $condition[$daynumber] : null;
        if (!empty($dayCondition) and sizeof($dayCondition) > 0){
            if (strtotime($dayCondition['start']) <= $timeSelect and strtotime($dayCondition['end']) >=  $timeSelect  ) {
                echo "true";
            } else {
                echo 'false';
                echo strtotime($dayCondition['start']) .">=" . $timeSelect . "<br/>";
                echo strtotime($dayCondition['end']) ."<=" . $timeSelect;
            }
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
