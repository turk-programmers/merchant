$(function() {
    var ajaxRemove;
    $('.delete_button').click(function(e) {
      //e.preventDefult();
      if (confirm("Do you want to delete this item?") == false) {
        return false;
      }
      var ele = $(this);
      var rowid = ele.data('rowid');
      ajaxRemove = $.ajax({
        type:'POST',
        data:{rowid : rowid},
        url:'/'+ shopname +'/ajax_remove_to_cart/',
        beforeSend : function(){
          try{
            ajaxRemove.abort();
          } catch(Exception){

          }
        },
        success : function(xhr){
          var jsonData = $.parseJSON(xhr);
          ele.parent().parent().hide('slow');
          $('.cart_qty').html('<p>' + jsonData['total_items'] +' items to buy now</p>');
          $('.sumtotal').html(jsonData['sum']);
        }
      });
    });
    $('.image-link').magnificPopup({type: 'image'});
});