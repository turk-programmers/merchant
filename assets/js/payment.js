$(function(){
    var $mainForm = $("#mainform");
    $mainForm.validationEngine('detach').validationEngine({promptPosition : "topRight",scrollOffset:100});
    $('.creditcard').mask("?9999999999999999");
    $('.ccv').mask("?9999");
    $('.btn-back').click(function(e){
        e.preventDefault();
        $.mc.goBack();
    });
    $('.btn-next').click(function(e){
        var $this = $(this);
        if( !$mainForm.validationEngine('validate') ) return false;

        if ( $this.hasClass('disabled')) return false;
        else $this.html('Process..').addClass('disabled');

        e.preventDefault();
        $.mc.goNext();
    });
    $('.btn-print').click(function(e){
        e.preventDefault();
        $(this).hide();
        window.print();
        $(this).show();
    });

    $('.creditcard-logo > img').each(function(){
        $(this).data('active-src', $(this).attr('src'));

    });

    $('input[name=ccnumber]').validateCreditCard(function(result){
            $('#cctype').val('');
            ccAccepted();
            changeLogoTo();
            if (result.card_type != null) {
                    //console.log(result.card_type.name);
                    changeLogoTo(result.card_type.name);
                    $('#cctype').val(result.card_type.name);
                    if(result.length_valid && result.luhn_valid){
                            ccAccepted(true);
                    }
            }
            //console.log('input[name=ccnum].validateCreditCard');
            //updateSelected();
    }, { accept: ['visa', 'mastercard', 'discover', 'amex']});


});
    function ccAccepted(accepted){
        if(accepted){
            $('input[name="ccnumber"]').data('format','correct').addClass('tick').validationEngine('hide');
        }else{
            $('input[name="ccnumber"]').data('format','wrong').removeClass('tick');
        }
    }

    function changeLogoTo(type){
        if(type){
            $('.creditcard-logo > img').each(function(){
                if($(this).data('type') == type){
                        $(this).attr('src',$(this).data('active-src'));
                }else{
                        $(this).attr('src',$(this).data('gray'));
                }
            });
        }else{
            $('.creditcard-logo > img').each(function(){
                $(this).attr('src',$(this).data('active-src'));
            });
        }
    }