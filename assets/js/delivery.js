
    var coreDelivery = (function(){

        function toSeconds(t) {
            var bits = t.split(':');
            return bits[0]*3600 + bits[1]*60 + bits[2]*1;
        }


        return {
            checkSameDay : function( thisDate ) {
                var dateSelect = new Date(thisDate);
                var conditionAllow = shops.sameDay_con;
                var today = new Date( shops.today );
                var onlyDateSelect = new Date(dateSelect.getMonth()+1 + "-"+ dateSelect.getDate() + "-" +dateSelect.getFullYear())
                if (today < onlyDateSelect ) { return true; }
                if (today > onlyDateSelect) {  return false; }
                if (shops.allow_same_day == 0 & today.sameDay(onlyDateSelect) ){ return false; }

                if ( conditionAllow != 0 )
                {
                    var cStart = conditionAllow[dateSelect.getDay()].start + ":00";
                    var cEnd = conditionAllow[dateSelect.getDay()].end + ":00";

                    var selectTime = dateSelect.getHours() + ":" + dateSelect.getMinutes() + ":" + "00";

                    //if ( cStart <= selectTime & cEnd >= selectTime ) return true;
                    if (toSeconds(cStart) <= toSeconds(selectTime) & toSeconds(cEnd) >= toSeconds(selectTime)  ) return true;
                    else return false;
                }
                else 
                {
                    if ( today.sameDay(onlyDateSelect) ) return true;
                   return false;
                }

            }
        }
   })();

   Date.prototype.sameDay = function(d) {
      return this.getFullYear() === d.getFullYear()
        && this.getDate() === d.getDate()
        && this.getMonth() === d.getMonth();
    }

    $(function() {

        $("#mainform").validationEngine('detach').validationEngine({promptPosition: "topRight", scrollOffset: 100});

        $('.btn-back').click(function(e) {
            e.preventDefault();
            $.mc.goBack();
        });
        $('.btn-next').click(function(e) {
            e.preventDefault();
            $.mc.goNext();
        });
        $('.btn-print').click(function(e) {
            e.preventDefault();
            $(this).hide();
            window.print();
            $(this).show();
        });


        $('.deliverto').change(function() {

            var o = $('.deliverto option:selected');
            var street = o.data('street');
            var city = o.data('city');
            var state = o.data('state');
            var zip = o.data('zip');
            var salestax = o.data('salestax');
            var default_salestax = o.data('default-salestax');
            var name = o.data('name');
            console.log(o.val() );
            if (o.val() != '' && o.val() != 'other') {

                $('.billaddress').parent().addClass('hidden');
                var jsons = {
                    deliver_firstname: name,
                    deliver_lastname: '',
                    deliver_streetaddress: street,
                    deliver_city: city,
                    deliver_state: state,
                    deliver_zip: zip,
                    deliver_email:'',
                    deliver_daytimephone:'',
                };

                $("#mainform").jsonToElement({
                    json: jsons,
                    hide: true
                });
            } else {
                if (o.val() == 'other') {
                    $('.billaddress').parent().removeClass('hidden');
                } else {
                    $('.billaddress').parent().addClass('hidden');
                }
                $('.tmpdata').each(function(){
                    tmpdata[$(this).attr('id')] = '';

                });
                tmpdata.deliverto = o.val() ;
                var json = tmpdata;
                $("#mainform").jsonToElement({
                    json: json,
                    show: true
                });
            }
        });

        //$('.deliverto')[0].selectedIndex=0;
        //$('.deliverto').val(0).change();
    });
