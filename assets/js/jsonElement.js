$(function() {
    /*
     * bind data to Element (input ,select , textarea) with json format after back to page again
     *$('#FormTarger').jsonToElement({json:{}});
     */
    $.fn.jsonToElement = function(options) {
        var opts = $.extend({
            json: '{}',
            hide:false,
            show:false
        }, options);
        
        var obj = opts.json;

        return this.each(function() {
            $(this).find('input').each(function() {
                var o = $(this);
                var t = o.attr('type');
                var n = o.attr('name');
                var v = o.attr('value');

                $.each(obj, function(key, value) {
                    //	        
                    if (t == 'text' || t == 'hidden') {
                        if (key == n && n != 'selected') {
                            o.val(value);
                            delete obj[key];
                            
                            if(opts.hide){
                                o.parent().hide();                                
                            }
                            if(opts.show){
                                o.parent().show();                                
                            }
                        }
                    } else if (t == 'checkbox' || t == 'radio') {
                        if (key == n) {
                            if (v.toLowerCase() == value.toLowerCase()) {

                                o.click();
                                //o.parent().addClass('checked'); // only ie 8 for ny
                                delete obj[key];
                                
                                if(opts.hide){
                                    o.parent().hide();                                    
                                }
                                if(opts.show){
                                    o.parent().show();                                
                                }
                            }
                        }

                    }
                    //return false;
                });
            });

            // select
            $(this).find('select').each(function() {
                var o = $(this);
                var n = o.attr('name');
                $.each(obj, function(key, value) {
                    if (key == n) {
                        o.find('option').removeAttr("selected");
                        o.find('option').each(function() {
                            if ($(this).val() == value) {

                                o.val(value);
                                //o.change();
                                $(this).prop('selected', true).attr("selected", "selected");
                                delete obj[key];
                                if(opts.hide){ // hide control
                                    o.parent().hide();
                                }
                                if(opts.show){
                                    o.parent().show();                                
                                }
                            }
                        });
                    }
                    //return false;
                });
            });

            //textarea
            $(this).find('textarea').each(function() {
                var o = $(this);
                var n = o.attr('name');
                $.each(obj, function(key, value) {
                    if (key == n) {
                        o.text(value);
                        delete obj[key];
                        if(opts.hide){ // hide control
                            o.parent().hide();
                        }
                        if(opts.show){
                            o.parent().show();                                
                        }
                    }
                });
            });
            return false;
        });
    };

    $.fn.clearForm = function (options){
        var opts = $.extend( {
            callback : function(){}
        },
            options
        );
        return this.each(function(){
          $(this).find("input[type=text], textarea").val("");
          //$(this).find('select').val(0);
          
          opts.callback.call(this);
          
        });
    };
});