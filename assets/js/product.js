var tmp = $.fn.tooltip.Constructor.prototype.show;
$.fn.tooltip.Constructor.prototype.show = function() {
    tmp.call(this);
    if (this.options.callback) {
        this.options.callback();
    }
}
$(document).ready(function() {
    /* initialize shuffle plugin */
    var $grid = $('#grid');
    var sPageURL = window.location['hash'].substring(0,(window.location['hash'].length)-5);
    $('.view-details-popup').magnificPopup({
        type:'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });



    $grid.shuffle({
        itemSelector: '.item' // the selector for the items in the grid
    });



    $grid.on('layout.shuffle' , function(evt, $collection, shuffle){
        // console.log("update layout")
        adjust_all_items();
        
    });

    // window.onload = function() {
    //     $grid.shuffle('layout');
    // }

  
    

    // $('#cart').popover({
    //     trigger: 'click',
    //     placement: 'left' ,

    // });



    var ajaxLoadcart;
    $('#cart').click(function(e) {
        e.preventDefault();
        // var ele = $(this);
        // if ( $('.popover').size() > 0 ) loadCart();
        location.href= "/"+SHOPNAME+"/cart";
    });

    function loadCart( callback ) {
        var $popover = $('.popover');
        var oposition = $popover.position();
        var owidth = $popover.width();
        var oheight = $popover.height();

        $.ajax({
            type: 'POST',
            url: '/' + shopname + '/ajax_load_cart',
            dataType: "html",
            success: function(xhr) {
                $('.popover-content').html(xhr);
                var left = oposition.left - ($popover.width() - owidth );
                var top  = oposition.top - (($popover.height() - oheight ) / 2 ) ;

                $popover.css({
                    'left' : left ,
                    'top' : top
                });

                if ( typeof callback === 'function') callback();
            }
        });
    }

    window.addEventListener("orientationchange", function() {
       $('#cart').popover('hide')
    } , false);

    $('.image-link').magnificPopup({type: 'image'});


    var ajaxAddToCart;
    $('button[name=product_submit]').click(function(e) {
        e.preventDefault();
        var form = $(this).parent().parent();
        var btn = $(this);
        //$('.next , .previous').button('loading');
        $nextText = $('.next').html();
        $previousText = $('.previous').html();
        $tempText = $(this).html();
        $(this).html("Loading...").attr('disabled');
        //$('button[name=product_submit]').button('loading');
        ajaxAddToCart = $.ajax({
            type: 'POST',
            data: form.serialize(),
            url: '/' + shopname + '/ajax_add_to_cart/',
            beforeSend: function() {
                try {
                    ajaxAddToCart.abort();
                } catch (Exception) {
                    //$('button[name=product_submit]').button('reset');
                    $('.next').html($nextText);
                    $('.previous').html($previousText);
                }
            },
            success: function(xhr) {
                $('.noitems').removeClass('noitems').unbind('click');
                //$('button[name=product_submit]').button('reset');
                //$('.next , .previous').button('reset');
                $('.next').html($nextText);
                $('.previous').html($previousText);
                $('button[name=product_submit]').html($tempText).removeAttr('disabled');
                //btn.html('<i class="glyphicon glyphicon-refresh"></i> Update');
                var jsonData = $.parseJSON(xhr);
                if (!$('#alertmsg1').is('.in')) {

                    if (typeof alertSuccessText !== 'undefined'  ) {
                        var message = alertSuccessText.replace(/{img}/gi , btn.data('img')).replace(/{name}/gi , btn.data('name'));
                        $('#alertmsg1').html(message);
                    }
                    else {
                        $('#alertmsg1').html('<h4>Success</h4><p><img src="'+btn.data('img')+'" style="width:60px"/> You have added <b>'+ btn.data('name') +'</b> to your shopping cart!<br><a class="notify-gotocart-link" href="/'+shopname+'/cart">Go To Cart</a></p>');
                    }




                    $('#alertmsg1').addClass('in');
                    setTimeout(function() {
                        $('#alertmsg1').removeClass('in');
                    }, 4200);
                }


                $('.cart_qty').html(jsonData['total_items']);

                if ( $('.popover').find('#merchandise_wrapper').size() > 0 ) {
                    loadCart();
                }
            }
        });
        return false;
    });

    $('.b-bread__sub_level_2 ').addClass('b-bread__btn_inactive');
    $('.b-bread__btn').click(function(event) {
        event.stopPropagation();

        // Inactive all children
        //$('.b-bread__btn', $(this)).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
        // Inactive all siblings
        $('.b-bread__btn', $(this).parent().parent()).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');

        // remove color
        $('.b-bread__label', $(this).parent().parent()).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');

        //focus
        $(this).removeClass('b-bread__btn_inactive').addClass('b-bread__btn_active');
        $('.b-bread__label', $(this)).removeClass('b-bread__btn_inactive').addClass('b-bread__btn_active');

        var ele = $(this);


        //Only chosed level 1
        if (ele.parent().data('level') == '1') {
            var groupName = ele.parent().data('section');
            $grid.shuffle('shuffle', groupName);
            $('.b-bread__sub_level_2').removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
            $('.b-bread__btn', $('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
            $('.b-bread__label', $('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
            $('.' + ele.parent().data('tab')).removeClass('b-bread__btn_inactive').addClass('b-bread__btn_active');

            window.history.pushState('', '', ele.parent().data('link'));


        } else {
            var groupName = ele.data('section');
            $grid.shuffle({
                itemSelector: '.item', // the selector for the items in the grid

            });
            $grid.shuffle('shuffle', groupName);
        }

    });
    $('.btn-grid').click(function() {
        // 
        // Perform adjusting item
        // 
        // adjust_all_items();

        $(this).removeClass('merchant-btn').addClass('merchant-btn-revert');
        $('.btn-list').removeClass('merchant-btn-revert').addClass('merchant-btn');
        $('.list').removeClass('list').addClass('grid');

      

        $('.b-bread__sub_level_2').removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
        $('.b-bread__btn', $('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
        $('.b-bread__label', $('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');


        $('.grid').children('div').children('div:nth-child(1)')
                .removeClass('col-lg-4 col-sm-4')
                .addClass('col-lg-12 col-xs-12 height-img');

        $('.grid').children('div').children('div:nth-child(2)')
                .removeClass('col-lg-8 col-sm-8')
                .addClass('col-lg-12 col-xs-12 from-group');

        var groupName = $('.b-bread__sub_level_1').has(".b-bread__btn_active").data('section');
       // $grid.shuffle('layout', groupName);
        adjust_all_items();
            $grid.shuffle('layout', groupName);
        // 
        // setTimeout(function() {
        //     $grid.shuffle('update', groupName);
        // }, 500);
        // 
        // Perform adjusting item
        // 
       //adjust_all_items();
    });

    $('.btn-list').click(function() {
        // 
        // Perform adjusting item
        // 
        // adjust_all_items();
        
        $('.grid').removeClass('grid').addClass('list');
        $(this).removeClass('merchant-btn').addClass('merchant-btn-revert');
        $('.btn-grid').removeClass('merchant-btn-revert').addClass('merchant-btn');


        $('.b-bread__sub_level_2').removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
        $('.b-bread__btn', $('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');
        $('.b-bread__label', $('.b-bread__sub_level_2')).removeClass('b-bread__btn_active').addClass('b-bread__btn_inactive');

        $('.list').children('div').children('div:nth-child(1)')
                .attr('class', 'col-lg-4 col-sm-4 form-group');

        $('.list').children('div').children('div:nth-child(2)')
                .attr('class', 'col-lg-8 col-sm-8 list-warpper').children('p');

        var groupName = $('.b-bread__sub_level_1').has(".b-bread__btn_active").data('section');
        $grid.shuffle('update', groupName);
        // setTimeout(function() {
        //     $grid.shuffle('update', groupName);
        // }, 500);
        // 
        // Perform adjusting item
        // 
        adjust_all_items();
    });

    $(sPageURL).click();

    $('.noitems').click(function(){
       alert("It is required to select at least one product");
       return false;
    });
});