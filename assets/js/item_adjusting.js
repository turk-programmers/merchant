/**
 * Adjust all merchandise items to show their properties be balancing height.
 *
 * @returns {void}
 */
function adjust_all_items() {
    // Get all items in row set
    var rows = get_all_row_items();
    // Addjust all items in each row.
    for (var g in rows) {
        for (var r in rows[g]) {
            var items = rows[g][r];
            var data = adjust_row_items(items);
            // console.log("##### " + g + "######")
            // // console.log(items);
            // console.log(data)
        }
    }

}
/**
 * Get all visible item with row their belong to.
 *
 * @returns {Object} Object contain all visible item in row set
 */
function get_all_row_items() {
    var rows = new Object();
    var index = 0;
    $('#listproduct').each(function () {
        var parent = $(this);
        //rows.push(get_row_items_by_type(parent));
        rows[index] = get_row_items_by_type(parent);
        index++;
    });
    return rows;
}
function get_row_items_by_type(type_wrapper) {
    var parent = type_wrapper.find('#grid');
    var activeTab = $(".b-bread__btn.b-bread__btn_active").attr("id");
    var col_number = get_column_number(parent);
    var count = 0;
    var row_number = 1;
    var rows = new Object();



    $('.item:visible').each(function () {
        var item = $(this);
        var thisGroups = item.data("groups");

       
        if ( $.inArray(activeTab , thisGroups) >= 0) {
        //if(item.css('visibility') != 'hidden') {
            if (++count > col_number) {
                count = 1;
                row_number++;
            }
            item.data('row', row_number).attr('data-row', row_number);
            if (typeof rows[row_number] == 'undefined') {
                rows[row_number] = new Object();
            }
            rows[row_number][item.data('value')] = item;
        }
    })

    return rows;
}
/**
 * Get current column number that showing on now.
 *
 * @param {object} parent The big parent object to get a <b>screen width</b>.
 * @param {object} object_item The one cell object of column to get a <b>column width</b>.
 * @returns {inteter} Number of column that showing on now.
 */
function get_column_number(parent) {
    var item = parent.find('.item').first();
    var col_number = parseInt(parseInt(parent.width()) / parseInt(item.width()-5));
    //console.log('col_number:' + col_number);
    return col_number;
}
/**
 * Adjust all item in the row to have balance height of each property.
 *
 * @param {Array} items Array contain all items in the same row.
 * @returns {void}
 */
function adjust_row_items(items) {
    var pieces = [{
            selector: '.row .height-img',
            max_height: 0
        }, {
            selector: 'form .additional_options',
            max_height: 0
        }, {
            selector: '.row .name',
            max_height: 0
        // }, {
        //     selector: '.description',
        //     max_height: 0
        // }, {
        //     selector: '.options',
        //     max_height: 0
        // }, {
        //     selector: '.buttons',
        //     max_height: 0
        }];
    // Clear height of item properties in the row
    for (var i in items) {
        var item = items[i];
        for (var index in pieces) {
            var piece = pieces[index];
            /*
             * Clear height of piece.
             */
            item.find(piece['selector']).css('height', 'auto');
        }
    }

    // Return if no need to adjust
    if ($('.panel.panel_merchandise').hasClass('view_list_style_list')) {
        return;
    }

    // Get max height of item properties in the row
    for (var i in items) {
        var item = items[i];


        for (var index in pieces) {
            var piece = pieces[index];
            /*
             * Get max height of piece.
             */
            if (item.find(piece['selector']).height() > piece['max_height']) {
                piece['max_height'] = item.find(piece['selector']).height();
            }
        }
    }


    // Apply max height of all item properties in the row
    for (var i in items) {
        var item = items[i];
        for (var index in pieces) {
            var piece = pieces[index];
            /*
             * Apply max height of piece.
             */
            item.find(piece['selector']).css('height', piece['max_height'] + 'px');
        }
    }
    return pieces;
    //console.log(pieces);

}
function adjust_row_items_by_type(type_wrapper) {
    // Get all items in row set
    var rows = get_row_items_by_type(type_wrapper);
    // Addjust all items in each row.
    for (var r in rows) {
        var items = rows[r];
        adjust_row_items(items);
    }


}


$(function() {
    //adjust_all_items();
    $('.merchant-btn-revert').first().click();
});
$(window).load(function() {
   // adjust_all_items();
    $('.merchant-btn-revert').first().click(); 
});