$(function(){

    $("#mainform").validationEngine('detach').validationEngine({promptPosition : "topRight",scrollOffset:100});
    
    $('.btn-back').click(function(e){
        e.preventDefault();
        $.mc.goBack();
    });
    $('.btn-next').click(function(e){
        e.preventDefault();
        $.mc.goNext();
    });
    $('.btn-print').click(function(e){
        e.preventDefault();        
        $(this).hide();
        window.print();
        $(this).show();
    });        
});