/*
 * fnetmerchant library v0.0.1 - 2014-07-15
 * Turk Enterprises Inc.
 */
(function($, document, window){
    $.fn['mc'] = $['mc'] = function (options){
        $.mc.getMainForm = function(){
            return $('#mainform');
        };
        
        $.mc.goNext = function(){
            var form = $.mc.getMainForm();
            $('#next_form', form).val($.mc['next_form']);
            form.submit();
        };
        
        $.mc.goBack = function(){
            var form = $.mc.getMainForm();
            form.validationEngine('detach');
            $('#next_form', form).val($.mc['prev_form']);
            form.submit();
        };
        
        $.mc.goTo = function(target){
            var form = $.mc.getMainForm();
            form.validationEngine('detach');
            $('#next_form', form).val(target);
            form.submit();
        };
    };
    /*
    * Initial the plugin
    */
    $.mc();
                
        
}(jQuery, document, window));
