function propulateForm(valuenumber){
	valuenumber = valuenumber || 1;
	$('input:visible').each(function(){
	    if($(this).prop('type') == "text"){
	    	if(!$(this).val() && !$(this).attr('disabled')){
	    		if(valuenumber == 2){
			        if($(this).attr('data-value2')){
			            $(this).val($(this).attr('data-value2'));
			        }else{
			            valuenumber = 1;
			        }
	    		}
	    		if(valuenumber == 1){
	    			if($(this).attr('data-value')){
			            $(this).val($(this).attr('data-value'));
			        }else{
			            $(this).val($(this).attr('name'));
			        }
	    		}
	    	}
	    }else if($(this).prop('type') == "checkbox" || $(this).prop('type') == "radio"){
	        //$(this).attr('checked',true);
                $(this).click();
	    }
	});
	$('select:visible').each(function(){
		if(!$(this).val() && !$(this).attr('disabled')){
		    var sel = $(this);
		    if(valuenumber == 2){
		        if($(this).attr('data-value2')){
		            $(this).val($(this).attr('data-value2'));
		        }else{
				    valuenumber = 1;
		        }
    		}
    		if(valuenumber == 1){
    			if($(this).attr('data-value')){
		            $(this).val($(this).attr('data-value'));
		        }else{
				    $('option', $(this)).each(function(){
				        if($(this).val()!=''){
				            sel.val($(this).val());
				            return false;
				        }
				    });
		        }
    		}
		}
	});
	$('textarea:visible').each(function(){
		if(!$(this).val() && !$(this).attr('disabled')){
			if(valuenumber == 2){
		        if($(this).attr('data-value2')){
		            $(this).val($(this).attr('data-value2'));
		        }else{
		            valuenumber = 1;
		        }
    		}
    		if(valuenumber == 1){
    			if($(this).attr('data-value')){
		            $(this).val($(this).attr('data-value'));
		        }else{
		            $(this).val($(this).attr('name'));
		        }
    		}
		}
	});
	$('.image-radio').each(function(){
		if($(this).hasClass('locked')) return;
		$(this).click().find('label').click();
	});
	$('.image-checkbox').each(function(){
		if($(this).hasClass('locked')) return;
	    $(this).click().find('label').click();
	});
	if($.uniform){
	    $.uniform.update();
	}

	$("#mainform").validationEngine('hide');
}