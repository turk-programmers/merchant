<?php
require_once('/home/funeralnet/public_html/mfscommon/programs/merchant/application/libraries/lessc.php');
require_once('/home/funeralnet/public_html/mfscommon/php/dev_check.php');

$less = new lessc;
// $less->setVariables(array(
// 	'main_color' => '#092949',
// 	));
$request_uri = $_SERVER['REQUEST_URI'];

$file_name = trim(preg_replace('/(.*)(.*\/assets)(.*)(\.[a-z]*)(.*)/i', '$3.css', $request_uri), '/');
if(is_readable($file_name)) {
    css_output(@file_get_contents($file_name));
} else {
    $file_name = trim(preg_replace('/(.*)(.*\/assets)(.*)(\.[a-z]*)(.*)/i', '$3.less', $request_uri), '/');
    if(!is_readable($file_name)) {
        $file_name = "core/".trim(preg_replace('/(.*)(.*\/assets)(.*)(\.[a-z]*)(.*)/i', '$3.css', $request_uri), '/');
        if(is_readable($file_name)){
            css_output(@file_get_contents($file_name));
        } else {
            $file_name = "core/".trim(preg_replace('/(.*)(.*\/assets)(.*)(\.[a-z]*)(.*)/i', '$3.less', $request_uri), '/');
            if(!is_readable($file_name)){
                file_not_found();
            }
        }
    }
}

$theme_file_name = "css/global.less";
if(!is_readable($theme_file_name)){
	// echo "Not Readable.\n";
	$theme_file_name = 'core/'.$theme_file_name;
}

$less_text = "";
$constants = get_defined_constants(true);
if(count(@$constants['user'])) {
    foreach((array)$constants['user'] as $name=>$value) {
        $less_text .= "@".$name.": '".$value."';\n";
    }
    $less_text .= "\n";
}

echo "/*".$theme_file_name . "*/\n";
$less_text .= @file_get_contents($theme_file_name);

echo "/*".$file_name . "*/\n";
$less_text .= @file_get_contents($file_name);

try {
    $less->setFormatter("compressed");
    $css_text = $less->compile($less_text);
    css_output($css_text);
} catch (Exception $ex) {
    file_not_found();
}



function file_not_found() {
    header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
    header("Status: 404 Not Found");
    echo $ex->getMessage();
    exit;
}
function css_output($css_text = "") {
    header("Content-Type: text/css");
    if(!is_dev()) {
        $seconds_to_cache = 3600;
        $ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
        header("Expires: $ts");
        header("Pragma: cache");
        header("Cache-Control: max-age=$seconds_to_cache");
    }
    echo $css_text;
}